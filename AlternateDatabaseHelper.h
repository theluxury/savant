//
//  AlternateDatabaseHelper.h
//  Savant
//
//  Created by Mark Wang on 11/23/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AlternateDatabaseHelper : NSObject
extern NSString * const YOUR_DATABASE;

+ (NSMutableDictionary *) writeToUserDefaults:(NSString *)name url:(NSURL *)url;

+ (void) addMenuItem:(NSMenu *)menu dictionary:(NSDictionary *)dictionary;

+ (void) mainDatabaseItemClicked:(NSMenuItem *)menuItem;

+ (void) addAllDbs;

+ (NSArray *) fetchSortedFromEveryDb:(NSString *)string;

+ (NSPersistentStore *)getMainStore;

@end
