//
//  AlternateDatabaseHelper.m
//  Savant
//
//  Created by Mark Wang on 11/23/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "AlternateDatabaseHelper.h"
#import "BlacklistHelper.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "NSURL+SavantLocations.h"
#import "PredicateHelper.h"
#import "NSURL+SavantLocations.h"
#import "NSString+TrimUtil.h"
#import "NSString+Contains.h"

@implementation AlternateDatabaseHelper

NSString * const YOUR_DATABASE = @"Your Database";

+(NSMutableDictionary *)writeToUserDefaults:(NSString *)name url:(NSURL *)url {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:name forKey:@"name"];
    [dictionary setObject:[url absoluteString] forKey:@"url"];
    
    // Do the array thing since theoretically you have a grip of them.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dblistArray = [NSMutableArray array];
    if ([defaults objectForKey:ALT_DATABASE_KEY])
        dblistArray = [[defaults objectForKey:ALT_DATABASE_KEY] mutableCopy];
    
    // Does a check to see if the name already exists.
    for (NSDictionary *dict in dblistArray) {
        if ([[dict objectForKey:@"name"] isEqualToString:name])
            return nil;
    }
    
    
    [dblistArray addObject:dictionary];
    [BlacklistHelper setUserDefaults:dblistArray forKey:ALT_DATABASE_KEY];
    
    return dictionary;
}

+ (void)addMenuItem:(NSMenu *)menu dictionary:(NSDictionary *)dictionary {
    NSMenuItem *newDbMenuItem = [[NSMenuItem alloc] initWithTitle:[dictionary objectForKey:@"name"] action:@selector(altDatabaseItemClicked:) keyEquivalent:@""];
    [newDbMenuItem setTarget:[AlternateDatabaseHelper class]];
    [menu insertItem:newDbMenuItem atIndex:0];
}

+ (void)altDatabaseItemClicked:(NSMenuItem *)menuItem {
    NSString *altDbName = [menuItem title];
    NSString *altDbUrl;
    NSDictionary *correspondingDictionary;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dblistArray = [NSMutableArray array];
    if ([defaults objectForKey:ALT_DATABASE_KEY])
        dblistArray = [[defaults objectForKey:ALT_DATABASE_KEY] mutableCopy];
    
    for (NSDictionary *dict in dblistArray) {
        if ([altDbName isEqualToString:[dict objectForKey:@"name"]]) {
            altDbUrl = [dict objectForKey:@"url"];
            correspondingDictionary = dict;
        }
    }
    
    
    NSString *prompt = [NSString stringWithFormat:@"You have chosen database %@ at %@. Would you like to remove it?", altDbName, altDbUrl];
    NSAlert *alert = [NSAlert alertWithMessageText: prompt
                                     defaultButton:@"Cancel"
                                   alternateButton:@"Remove"
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    NSInteger button = [alert runModal];
    if (button == NSAlertAlternateReturn) {
        // Shouldn't happen but meh.
        if (!correspondingDictionary) {
            return;
        }
        [dblistArray removeObject:correspondingDictionary];
        // Order here is important. Don't remove from menu before calling this.
        [BlacklistHelper setUserDefaults:dblistArray forKey:ALT_DATABASE_KEY];
        [[menuItem menu] removeItem:menuItem];

    }
}


+ (void)mainDatabaseItemClicked:(NSMenuItem *)menuItem {
    // Eventually want to let them rename this I guess.
    return;
}

+ (void) addAllDbs {

    NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
    
    NSError *error;
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSReadOnlyPersistentStoreOption, nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dblistArray = [NSMutableArray array];
    if ([defaults objectForKey:ALT_DATABASE_KEY])
        dblistArray = [[defaults objectForKey:ALT_DATABASE_KEY] mutableCopy];
    
    
    for (NSDictionary *dict in dblistArray) {
        NSURL *url = [NSURL URLWithString:[dict objectForKey:@"url"]];
        [psc addPersistentStoreWithType:NSSQLiteStoreType
                          configuration:nil
                                    URL:[url URLByAppendingPathComponent:@"SavantData.storedata"]
                                options:options
                                  error:&error];
    }
    
    
    [[[DataManager sharedInstance] imageCache] removeAllObjects];
    return;
    
}

+ (NSArray *)fetchSortedFromEveryDb:(NSString *)string {
    
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSPredicate *textPredicate = [SharedPredicateHelper searchTextNodes:string entity:@"moment"];
    
    // So first, have to get list of the DB's
    NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
    for (NSPersistentStore *store in [psc persistentStores]) {
        
        // if it doesn't exist, you probably don't care about it right?
        Moment *earliestMoment;
        NSArray *momentArray;
        if (string && [[string trimmedString] length] != 0)
            momentArray = [PredicateHelper fetchScreenshots:textPredicate storeArray:@[store] withLimit:1 ascending:EARLIEST];
        else
            // actually misnomer here since should ee latest moment. oh well...
            momentArray = [PredicateHelper fetchScreenshots:nil storeArray:@[store] withLimit:1 ascending:MOST_RECENT];
        
        // If the store doesn't have the phrase, should just skip it.
        if (!momentArray || [momentArray count] == 0)
            continue;
        earliestMoment = momentArray[0];
        // So here, should set the moment's timestamp as the key, and the object for it, set another dictionary with the name, url, and store? That seems good.
        NSMutableDictionary *storeDictionary = [NSMutableDictionary dictionary];
        [storeDictionary setObject:store forKey:@"store"];
        [storeDictionary setObject:[[store URL] absoluteString] forKey:@"url"];
        [storeDictionary setObject:[self getDbNameFromUrl:[store URL]] forKey:@"name"];
        [storeDictionary setObject:earliestMoment forKey:@"moment"];
        
        [dictionary setObject:storeDictionary forKey:earliestMoment.timestamp];
    }
    
    // Then, sort it out using the moment's timestamp.
    NSArray *sortedKeys = [[dictionary allKeys] sortedArrayUsingSelector: @selector(compare:)];
    NSMutableArray *sortedValues = [NSMutableArray array];
    for (NSString *key in sortedKeys)
        [sortedValues addObject: [dictionary objectForKey: key]];
    
    return sortedValues;
    
}


+ (NSString *)getDbNameFromUrl:(NSURL *)url {
    // So this method should return the name of the database from the URL
    if ([[url absoluteString] contains:[[NSURL SVDataDirectory] absoluteString]])
        return YOUR_DATABASE;
    
    // So if it gets here, means not the main DB
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dblistArray = [NSMutableArray array];
    if ([defaults objectForKey:ALT_DATABASE_KEY])
        dblistArray = [[defaults objectForKey:ALT_DATABASE_KEY] mutableCopy];

    for (NSDictionary *dict in dblistArray) {
        NSURL *defaultUrl = [NSURL URLWithString:[dict objectForKey:@"url"]];
        // So the reason for the contains is because url contains the "SavantData.storeddata"
        if ([[url absoluteString] contains:[defaultUrl absoluteString]])
            return [dict objectForKey:@"name"];
    }
    
    // If it got here, didn't get anything. That'd be odd, but meh.
    return @"Unamed Db";
    
}

+ (NSPersistentStore *)getStoreFromName:(NSString *)name {
    // Note: This method only works after you have added the stores to the persistent store coordinator.
    NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
    
    if ([name isEqualToString:YOUR_DATABASE])
        return [psc persistentStoreForURL:[NSURL SVPersistentStoreUrl]];
    
    // If is here, means not the default database.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dblistArray = [NSMutableArray array];
    if ([defaults objectForKey:ALT_DATABASE_KEY])
        dblistArray = [[defaults objectForKey:ALT_DATABASE_KEY] mutableCopy];
    
    for (NSDictionary *dict in dblistArray) {
        if ([name isEqualToString:[dict objectForKey:@"name"]]) {
            NSURL *url = [NSURL URLWithString:[dict objectForKey:@"url"]];
            url = [url URLByAppendingPathComponent:@"SavantData.storedata"];
            return [psc persistentStoreForURL:url];
        }
    }
    
    return nil;
    
}

// TODO: Put some sort of auto-update mechanism
+ (NSDictionary *) fetchStoresWithNames {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dblistArray = [NSMutableArray array];
    if ([defaults objectForKey:ALT_DATABASE_KEY])
        dblistArray = [defaults objectForKey:ALT_DATABASE_KEY];
    
    for (NSDictionary *storeDict in dblistArray) {
        NSString *name = [storeDict objectForKey:@"name"];
        NSPersistentStore *store = [self getStoreFromName:name];
        if (store)
            [dictionary setObject:store forKey:name];
        else {
            [self noDbAlert:name];
        }
    }
    
    NSPersistentStore *yourStore = [self getStoreFromName:YOUR_DATABASE];
    if (yourStore)
        [dictionary setObject:yourStore forKey:YOUR_DATABASE];
    else {
        // Note: This would be really bad.
        [self noDbAlert:YOUR_DATABASE];
    }
    
    return dictionary;
}

+ (void)noDbAlert:(NSString *)dbName {
    NSString *prompt = [NSString stringWithFormat:@"Could not find the database with the name %@. Perhaps you should delete it.", dbName];
    NSAlert *alert = [NSAlert alertWithMessageText:@"No Database Found" defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@"%@", prompt];
    [alert runModal];
}

+ (NSPersistentStore *)getMainStore {
    return [self getStoreFromName:YOUR_DATABASE];
}


@end
