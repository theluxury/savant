//
//  AXApplicationMirror.h
//  Savant
//
//  Created by Paul Musgrave on 2014-09-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextNode.h"
#import "AXWrapper.h"


@protocol AXApplicationMirrorDelegate <NSObject>

- (void)textNodeAdded:(TextNode *)textNode;
- (void)textNodeRemoved:(TextNode *)textNode;

@end


@interface AXApplicationMirror : NSObject

@property (nonatomic, weak) id<AXApplicationMirrorDelegate> delegate;

+ (instancetype)mirrorForPid:(pid_t)pid;
- (void)beginMirroringTextForWindow:(AXWrapper *)windowWrapper;
- (void)stopMirroring;

// Due to unreliable notifications, the current mechanism is to refresh everything
// in the part of the screen that changed
- (void)refreshContentInRect:(CGRect)changeRect;

@end
