//
//  AXApplicationMirror.m
//  Savant
//
//  Created by Paul Musgrave on 2014-09-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "AXApplicationMirror.h"
#import "DataManager.h"

@interface AXApplicationMirror ()

@property (nonatomic) AXObserverRef observerRef;
@property (nonatomic, strong) AXWrapper *appWrapper;

@property (nonatomic, strong) NSMutableSet *mirroredElements;
@property (nonatomic, strong) NSMutableSet *freshElements;
@property (nonatomic, strong) NSMapTable *textNodesByElement;

// ## :P
@property (nonatomic, strong) AXWrapper *focusedWindow;

@property (nonatomic) BOOL shouldUpdateWindowPosition;

@end


@implementation AXApplicationMirror


+ (instancetype)mirrorForPid:(pid_t)pid
{
    return [[self alloc] initWithPID:pid];
}

- (instancetype)initWithPID:(pid_t)pid
{
    if(self = [super init]){
//        AXObserverCreate(pid, observerCallback, &_observerRef);
//        CFRunLoopAddSource( [[NSRunLoop mainRunLoop] getCFRunLoop],
//                            AXObserverGetRunLoopSource(_observerRef),
//                            kCFRunLoopDefaultMode );
        
        _appWrapper = [AXWrapper wrapperForPID:pid];
        _mirroredElements = [NSMutableSet set];
        _freshElements = [NSMutableSet set];
        _textNodesByElement = [NSMapTable strongToStrongObjectsMapTable];
    }
    return self;
}

//- (void)dealloc
//{
//    // Since the AXObserverRef schedules its run loop source on the main thread, and the
//    // callback keeps an unsafe (void *)self, we need to do this
//    if([NSThread isMainThread]){
//        CFRelease(_observerRef);
//    } else {
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            CFRelease(_observerRef);
//        });
//    }
//}

// This was is dealloc, but trying it as a separate call to see if that fixes the non-deletion issue
// (it shouldn't, but something strange must be going on)
- (void)stopMirroring
{
    for (AXWrapper *wr in self.textNodesByElement) {
        [self.delegate textNodeRemoved:[self.textNodesByElement objectForKey:wr]];
    }
}


//static void observerCallback(AXObserverRef observer, AXUIElementRef element, CFStringRef notification, void *self)
//{
//    [(__bridge id)self handleNotification:(__bridge NSString *)(notification) forElement:[AXWrapper wrapperForElement:element]];
//}
//
//- (void)handleNotification:(NSString *)notification forElement:(AXWrapper *)el
//{
//    // Have to handle this separately, because it's not safe to call AX methods
//    // on a deleted element
////    if ([notification isEqualToString:NSAccessibilityUIElementDestroyedNotification]){
////
//////        NSLog(@"Destroyed %@",el.axElementRef); // DEBUG
////        [self unmirror:el];
////        return;
////    }
////    NSLog(@"%@ :: %@",notification, el); // DEBUG
//    
//    if ([[el getAttribute:NSAccessibilityWindowAttribute] isEqual:self.focusedWindow]){
//        if([notification isEqualToString:NSAccessibilityValueChangedNotification]){
//            if ([el isOfTextType]){
//                // TODO: handle changes in a more fine grained way
//                if (!CGRectIsEmpty(el.visibleRect)){
////                    [self removeTextNodeForElement:el];
//                    [self createTextNodeForElement:el];
//                }
//            }
//        } else { // Resized or moved
//            // TODO: this causes a global position update, based on the assumption that it only
//            // gets triggered for window movement. Should probably handle this better
//            self.shouldUpdateWindowPosition = YES;
////            [el updateVisibleRectWithParentRect:el.parent.visibleRect];
//        }
//    }
//}
//
//- (void)listenForAXNotification:(NSString *)notification fromElement:(AXWrapper *)el
//{
//    AXObserverAddNotification(self.observerRef, el.axElementRef, (__bridge CFStringRef)(notification), (__bridge void *)(self));
//}

// ## hack
- (void)beginMirroringTextForWindow:(AXWrapper *)windowWrapper
{
    self.focusedWindow = windowWrapper;
    [self mirror:windowWrapper parent:nil];
    
//    // Listening on the application element means that we get notified for all elements from that application
//    // We don't listen to created notifications because they're not always called, so we have to crawl anyway
//    [self listenForAXNotification:NSAccessibilityValueChangedNotification fromElement:self.appWrapper];
//    [self listenForAXNotification:NSAccessibilityResizedNotification fromElement:self.appWrapper];
//    [self listenForAXNotification:NSAccessibilityMovedNotification fromElement:self.appWrapper];
//    // Deletion notifications appear to be somewhat temperamental as well, and we are only bothering
//    // to track visible ones, so trying to use delta here
////    [self listenForAXNotification:NSAccessibilityUIElementDestroyedNotification fromElement:self.appWrapper];
}

- (void)mirror:(AXWrapper *)el parent:(AXWrapper *)parent
{
    // This is definitely necessary because the accessibility hierarchy isn't always actually a tree
    // It seems like parents might be swapped out over children as well (at least in chrome), though not completely sure
    if([self.mirroredElements containsObject:el]) return;

    CGRect visibleRect = [el updateVisibleRectWithParentRect: parent ? parent.visibleRect : [[NSScreen mainScreen] frame]];

    if(!CGRectIsEmpty(visibleRect)){
        [self.mirroredElements addObject:el];
        if([el isOfTextType]){
            [self createTextNodeForElement:el];
        }
        for (AXWrapper *child in el.visibleChildren){
            [self mirror:child parent:el];
        }
    }
}

- (void)unmirror:(AXWrapper *)el
{
    [self removeTextNodeForElement:el];
    [self.mirroredElements removeObject:el];
    for (AXWrapper *child in el.prevVisibleChildren) {
        [self unmirror:child];
    }
}

// ## hmm; don't want to have managed objects for transient elements that don't end up corresponding to
// any screenshot.
- (void)createTextNodeForElement:(AXWrapper *)el
{
    // ## we were still getting some text node leaks; this should prevent that,
    // though if they're still sticking around longer than they should be then
    // it's probably a good idea to remove this and put back the other removes,
    // and figure out why the leaked ones aren't already removed at the proper time
    [self removeTextNodeForElement:el];
//    if([self.textNodesByElement objectForKey:el]){
//        NSLog(@"!!!!!!!!!!!!!!!!! Overwriting text node !!!!!!!!!!!!!!!!!!!");
//    }
    NSString *visibleText = el.visibleText;
    if(visibleText && ![visibleText isEqualToString:@""]){
        TextNode *node = [TextNode insertInManagedObjectContext:[DataManager sharedInstance].recordingMOC];
        node.text = visibleText;
        node.rect = NSStringFromRect(el.visibleRect);
        
        [self.textNodesByElement setObject:node forKey:el];
        [self.delegate textNodeAdded:node];
    }

}
- (void)removeTextNodeForElement:(AXWrapper *)el
{
    TextNode *node = [self.textNodesByElement objectForKey:el];
    [self.textNodesByElement removeObjectForKey:el];

    if(node){
        [self.delegate textNodeRemoved:node];
    }
}


// Now that we're pruning to the changed rect, try a full refresh for now to improve reliability
- (void)refreshContentInRect:(CGRect)changeRect
{
    [self refreshContentOfElement:self.focusedWindow inRect:changeRect parentRect:[[NSScreen mainScreen] frame]];
    self.freshElements = [NSMutableSet set];
}

- (void)refreshContentOfElement:(AXWrapper *)el inRect:(CGRect)changeRect parentRect:(CGRect)parentRect
{
    // This is a precaution against deleting elements that move between parents
    // (not entirely sure whether this happens or not)
    [self.freshElements addObject:el];
    
    CGRect prevRect = el.visibleRect;
    CGRect visibleRect = [el updateVisibleRectWithParentRect:parentRect];
    
    if(CGRectIsEmpty(visibleRect)){
        [self unmirror:el];
        return;
    }
    
    if([el isOfTextType]){
        if(!CGRectEqualToRect(prevRect, visibleRect)){
            [self removeTextNodeForElement:el];
            if(!CGRectIsEmpty(visibleRect)){
                [self createTextNodeForElement:el];
            }
        } else if(![el.lastVisibleText isEqualToString:el.visibleText]){
            [self removeTextNodeForElement:el];
            [self createTextNodeForElement:el];
        }
    }
    
    CGRect elChangeRect = CGRectIntersection(changeRect, visibleRect);
    if(!CGRectIsEmpty(elChangeRect)){
        NSMutableSet *prevChildSet = [NSMutableSet setWithArray:el.prevVisibleChildren];
        NSSet *curChildSet = [NSSet setWithArray:el.visibleChildren];
        for (AXWrapper *child in curChildSet) {
            // This could potentially do multiple refreshed on an element contained by multiple parents,
            // but shouldn't be a big problem (if that even happens anymore with the visibleChidren change).
            // Could easily avoid it with freshElements.
            if([self.mirroredElements containsObject:child]){
                [self refreshContentOfElement:child inRect:elChangeRect parentRect:visibleRect];
            } else {
                [self mirror:child parent:el];
            }
        }
        [prevChildSet minusSet:curChildSet];
        [prevChildSet minusSet:self.freshElements];
        for (AXWrapper *deletedChild in prevChildSet) {
            [self unmirror:deletedChild];
        }
    }
}


@end
