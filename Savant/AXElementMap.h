//
//  AXElementMap.h
//  Savant
//
//  Created by Paul Musgrave on 2014-10-03.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXWrapper.h"

// Singleton to maintain canonical AXWrappers for accessibility elements.
// This allows the caching in AXWrapper to work properly (which we do because
// the accessibility functions are expensive en masse).
@interface AXElementMap : NSObject

+ (instancetype)sharedInstance;

- (AXWrapper *)getWrapper:(AXUIElementRef)elRef;
- (void)setWrapper:(AXWrapper *)wr forElementRef:(AXUIElementRef)elRef;
- (void)removeElement:(AXUIElementRef)elRef;

@end
