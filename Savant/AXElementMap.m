//
//  AXElementMap.m
//  Savant
//
//  Created by Paul Musgrave on 2014-10-03.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "AXElementMap.h"


@interface AXElementMap (){
    CFMutableDictionaryRef _wrapperDictionary;
}
@end


@implementation AXElementMap

+ (instancetype)sharedInstance
{
    static AXElementMap *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    if(self = [super init]){
        // Create a dictionary which uses CFEqual and CFHash and does not retain keys or values
        CFDictionaryKeyCallBacks keyCallbacks = {
            0,
            NULL,
            NULL,
            NULL,
            CFEqual,
            CFHash
        };
        CFDictionaryValueCallBacks valueCallbacks = {
            0,
            NULL,
            NULL,
            NULL,
            CFEqual
        };
        _wrapperDictionary = CFDictionaryCreateMutable(NULL, 0, &keyCallbacks, &valueCallbacks);
    }
    return self;
}
- (void)dealloc
{
    CFRelease(_wrapperDictionary);
}

- (AXWrapper *)getWrapper:(AXUIElementRef)elRef
{
    return CFDictionaryGetValue(_wrapperDictionary, elRef);
}
- (void)setWrapper:(AXWrapper *)wr forElementRef:(AXUIElementRef)elRef
{
    CFDictionarySetValue(_wrapperDictionary, elRef, (__bridge const void *)(wr));
}
- (void)removeElement:(AXUIElementRef)elRef
{
    CFDictionaryRemoveValue(_wrapperDictionary, elRef);
}


@end
