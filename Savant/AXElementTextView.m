//
//  AXElementTextView.m
//  Savant
//
//  Created by Mark Wang on 8/31/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "AXElementTextView.h"

@implementation AXElementTextView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    NSRect newRect = NSMakeRect(dirtyRect.origin.x, dirtyRect.origin.y, dirtyRect.size.width, dirtyRect.size.height);
    
    NSBezierPath *textViewSurround = [NSBezierPath bezierPathWithRoundedRect:newRect xRadius:2 yRadius:2];
    [textViewSurround setLineWidth:1.5];
    [[NSColor blueColor] set];
    [textViewSurround stroke];
    [self setAlphaValue:0.5];
    
    self.backgroundColor = [NSColor colorWithRed:225.0/255 green:230.0/255 blue:237.0/255 alpha:1.0];
}

@end
