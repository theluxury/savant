//
//  AXWrapper.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-26.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/NSAccessibility.h>
#import "Moment.h"

@interface AXWrapper : NSObject

@property (nonatomic) AXUIElementRef axElementRef;

@property (nonatomic, strong, readonly) NSString *role;
@property (nonatomic, readonly) CGRect visibleRect; // This is empty until updated

@property (nonatomic, strong) NSArray *prevVisibleChildren;
@property (nonatomic, strong) NSString *lastVisibleText;

// This returns a canonical wrapper: if it is called
// with two AXUIElementRefs that are CFEqual, it will return the same object
+ (instancetype)wrapperForElement:(AXUIElementRef)elementRef;

// This does not return a canonical wrapper
+ (instancetype)wrapperForPID:(pid_t)pid;


- (NSArray *)listAttributes;
- (id)getAttribute:(NSString *)attribute;
- (id)getAttribute:(NSString *)attribute withParameter:(id)parameter;
- (void)setValue:(id)value forAttribute:(NSString *)attribute;
- (NSInteger)countForAttribute:(NSString *)attribute;

- (NSString *)title;
- (NSString *)value;

- (NSArray *)children;
- (AXWrapper *)parent;

- (AXWrapper *)childAtPath:(NSArray *)path;

- (BOOL)isOfTextType;
- (BOOL)isScrollArea;
- (BOOL)isOfParentType;
- (BOOL)isDestroyed;

- (NSArray *)visibleChildren;
- (NSString *)visibleText;

- (CGRect)getRect;
- (CGRect)updateVisibleRectWithParentRect:(CGRect)parentRect;

- (pid_t)pid;


@end
