//
//  AXWrapper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-26.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "AXWrapper.h"
#import "AXElementMap.h"


@implementation AXWrapper

@synthesize role = _role;

+ (instancetype)wrapperForElement:(AXUIElementRef)axElementRef
{
    AXElementMap *elementMap = [AXElementMap sharedInstance];
    AXWrapper *wrapper = [elementMap getWrapper:axElementRef];
    if(!wrapper){
        CFRetain(axElementRef);
        wrapper = [[self alloc] init];
        wrapper.axElementRef = axElementRef;
        
        [elementMap setWrapper:wrapper forElementRef:axElementRef];
    }
    return wrapper;
}

+ (instancetype)wrapperForPID:(pid_t)pid
{
    AXWrapper *wrapper = [[self alloc] init];
    wrapper.axElementRef = AXUIElementCreateApplication(pid);
    return wrapper;
}

- (void)dealloc
{
    [[AXElementMap sharedInstance] removeElement:_axElementRef];
    CFRelease(_axElementRef);
}

- (BOOL)isEqual:(id)object
{
    if(self == object) return YES;
    if(![object isKindOfClass:[self class]]) return NO;
    return [self isEqualToAXWrapper:object];
}
- (BOOL)isEqualToAXWrapper:(AXWrapper *)other
{
    return CFEqual(self.axElementRef, other.axElementRef);
}
- (NSUInteger)hash
{
    return CFHash(self.axElementRef);
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ : %@", [super description], [self role]];
}

- (void)printAttributes
{
    NSArray *attributes = [self listAttributes];
    NSLog(@"%@ ::", [self description]);
    for (NSString *attribute in attributes) {
        NSLog(@"  %@:%@", attribute, [self getAttribute:attribute]);
    }
}

- (void)printTree
{
    [self printTreeWithIndent:@""];
}
- (void)printTreeWithIndent:(NSString *)indent
{
    NSLog(@"%@%@ (", indent, [self role]);
    NSString *newIndent = [indent stringByAppendingString:@"  "];
    for (AXWrapper *w in self.children){
        [w printTreeWithIndent:newIndent];
    }
    NSLog(@"%@)",indent);
}


- (NSArray *)listAttributes
{
    NSArray *attributes;
    AXUIElementCopyAttributeNames(self.axElementRef, (void *)&attributes);
    return attributes;
}

- (id)getAttribute:(NSString *)attribute
{
    id value;
    if(!AXUIElementCopyAttributeValue(self.axElementRef, (__bridge CFStringRef)attribute, (void *)&value)){
        // Map any AXUIElementRefs to AXWrappers
        if (CFGetTypeID((CFTypeRef)value) == AXUIElementGetTypeID()) {
            value = [AXWrapper wrapperForElement:(AXUIElementRef)value];
        } else if ([value isKindOfClass:[NSArray class]]) {
            NSArray *arrayValues = (NSArray *)value;
            if ([arrayValues count] > 0 && CFGetTypeID((CFTypeRef)[arrayValues objectAtIndex:0]) == AXUIElementGetTypeID()) {
                value = [NSMutableArray array];
                for (id element in arrayValues) {
                    [value addObject:[AXWrapper wrapperForElement:(AXUIElementRef)element]];
                }
            }
        }
    } else {
        return nil;
    }
    
    return value;
}

- (id)getAttribute:(NSString *)attribute withParameter:(id)parameter
{
    // ## map wrappers?
    id value;
    AXUIElementCopyParameterizedAttributeValue(self.axElementRef, (__bridge CFStringRef)attribute, (__bridge CFStringRef)parameter, (void *)&value);
    return value;
}

- (NSInteger)countForAttribute:(NSString *)attribute
{
    CFIndex count;
    AXUIElementGetAttributeValueCount(self.axElementRef, (__bridge CFStringRef)attribute, &count);
    return count;
}

- (void)setValue:(id)value forAttribute:(NSString *)attribute
{
    //## may need modification to support some types
    AXUIElementSetAttributeValue(self.axElementRef, (__bridge CFStringRef)attribute, (__bridge CFTypeRef)(value));
}


- (NSString *)role
{
    if(_role) return _role;
    return _role = [self getAttribute:NSAccessibilityRoleAttribute];
}
- (AXWrapper *)parent
{
    return [self getAttribute:NSAccessibilityParentAttribute];
}

- (NSString *)title
{
    return [self getAttribute:NSAccessibilityTitleAttribute];
}

- (NSString *)value
{
    return [self getAttribute:NSAccessibilityValueAttribute];
}

- (NSArray *)visibleChildren
{
    NSArray *visibleChildren;
    // Possibly should duck type NSAccessibilityVisibleChildrenAttribute; some roles sometimes have it
    // (like AXList, AXRow (possibly AXOutlineRow subtype)), and some others might even always, though I don't know if we care (e.g. AXMenuBar)
    if([self.role isEqualToString:NSAccessibilityTableRole] ||
       [self.role isEqualToString:NSAccessibilityOutlineRole]){
        
        NSMutableArray *cells = [NSMutableArray array];
        NSArray *rows = [self getAttribute:NSAccessibilityVisibleRowsAttribute];
        NSUInteger numColumns = [self countForAttribute:NSAccessibilityVisibleColumnsAttribute];
        CFArrayRef rowChildren;
        for (AXWrapper *row in rows){
            AXUIElementCopyAttributeValues(row.axElementRef, kAXChildrenAttribute, 0, numColumns, &rowChildren);
            if(rowChildren){
                for (int i=0; i<CFArrayGetCount(rowChildren); i++) {
                    [cells addObject:[AXWrapper wrapperForElement: CFArrayGetValueAtIndex(rowChildren, i)]];
                }                
            }
        }
        visibleChildren = cells;
    } else if([self.role isEqualToString:NSAccessibilityBrowserRole]) {
        // This is slightly suspect, since the skipped direct child element is potentially
        // relevant (its a scroll view), but the columns are also supposed to be scroll views,
        // so they'll be polled anyway
        visibleChildren = [self getAttribute:NSAccessibilityVisibleColumnsAttribute];
    } else if([self.role isEqualToString:NSAccessibilityGridRole]) {
        // Not that this is documented anywhere (NSAccessibilityGridRole isn't at all),
        // but empirically seems to be right (e.g. Evernote, iTunes)
        visibleChildren = [self getAttribute:NSAccessibilityVisibleChildrenAttribute];
    } else {
        visibleChildren = self.children;
    }
    return self.prevVisibleChildren = visibleChildren;
}


// Warning: the accessibility hierarchy is not strictly a tree - there may be
// shared grandchildren (in particular, table-like elements have both columns and
// rows as children, and both contain the corresponding child cells)
- (NSArray *)children
{
    return [self getAttribute:NSAccessibilityChildrenAttribute];
}


// Path components are AXRoles. Might eventually need to support additional index/first/last specifiers,
// as either an array or a string component (e.g. #i)
- (AXWrapper *)childAtPath:(NSArray *)path
{
    AXWrapper *el = self;
    for (id component in path) {
        NSArray *componentSegments = [component componentsSeparatedByString:@"::"];
        NSString *role = componentSegments[0];
        NSInteger idx = componentSegments.count > 1 ? [componentSegments[1] integerValue] : 0;
        NSInteger seen = 0;
        BOOL found = NO;
        for (AXWrapper *child in el.children) {
            if([child.role isEqualToString:role]){
                if(seen == idx){
                    el = child;
                    found = YES;
                    break;
                } else {
                    seen++;
                }
            }
        }
        if(!found) return nil;
    }
    return el;
}


- (CGPoint)point
{
    CGPoint point;
    AXValueRef pv = CFBridgingRetain([self getAttribute:NSAccessibilityPositionAttribute]);
    AXValueGetValue(pv, kAXValueCGPointType, &point);
    CFBridgingRelease(pv);
    return point;
}
- (CGSize)size
{
    CGSize size;
    AXValueRef sv = CFBridgingRetain([self getAttribute:NSAccessibilitySizeAttribute]);
    AXValueGetValue(sv, kAXValueCGSizeType, &size);
    CFBridgingRelease(sv);
    return size;
}
- (CGRect)getRect
{
    CGPoint point = [self point];
    CGSize size = [self size];
    return CGRectMake(point.x, point.y, size.width, size.height);
}

- (CGRect)updateVisibleRectWithParentRect:(CGRect)parentRect;
{
    return _visibleRect = CGRectIntersection([self getRect], parentRect);
}


- (NSString *)visibleText
{
    NSString *role = [self role];
    NSString *visibleText;
    if([role isEqualToString:NSAccessibilityTextAreaRole]){
        visibleText = [self getAttribute: NSAccessibilityStringForRangeParameterizedAttribute
                    withParameter:[self getAttribute:NSAccessibilityVisibleCharacterRangeAttribute]];
    } else if ([role isEqualToString:NSAccessibilityStaticTextRole]){
        // Don't know how to do exact visibility checking here; could include visibleRect check,
        // and could maybe guess based on text size or something
        visibleText = [self getAttribute:NSAccessibilityValueAttribute];
    } else if ([role isEqualToString:NSAccessibilityTextFieldRole]) {
        // This is for URL bar. 
        visibleText = [self getAttribute:NSAccessibilityValueAttribute];
    }
    //? AXPopUpButton (value)
    //? AXMenuBarItem (title)
    //? AXButton (title)
    
    // Apparently this is sometimes not a string (in particular, an NSCFNumber). This should be safe either way.
    return self.lastVisibleText = (visibleText ? [NSString stringWithFormat:@"%@", visibleText] : nil);
}

- (BOOL)isOfTextType
{
    NSString *role = [self role];
    return [role isEqualToString: NSAccessibilityTextAreaRole] || [role isEqualToString:NSAccessibilityStaticTextRole] || [role isEqualToString:NSAccessibilityTextFieldRole];
}
- (BOOL)isScrollArea
{
    return [self.role isEqualToString:NSAccessibilityScrollAreaRole];
}
- (BOOL)isOfParentType
{
    // Doesn't filter out all the childless element types, just some of the more common ones
    NSString *role = [self role];
    return !([role isEqualToString: NSAccessibilityButtonRole]
             || [role isEqualToString:NSAccessibilityStaticTextRole]);
}

- (BOOL)isDestroyed
{
    pid_t pid;
    return AXUIElementGetPid(self.axElementRef, &pid) == kAXErrorInvalidUIElement;
}

- (pid_t)pid
{
    pid_t pid;
    AXUIElementGetPid(self.axElementRef, &pid);
    return pid;
}


@end
