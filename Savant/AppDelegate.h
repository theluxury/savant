//
//  AppDelegate.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ScreenRecorder.h"
#import "SVBrowsingWindowController.h"
#import "VideoWindow.h"
#import "VideoWindowController.h"
#import "MiniWindowController.h"
#import <Sparkle/Sparkle.h>
#import "StatusItemView.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, SUUpdaterDelegate>

extern NSString * const blacklist;
extern NSString * const FILTER_BLACKLIST_SORT;
extern NSString * const FILTER_BLACKLIST;
extern NSString * const FILTER_WHITELIST;
extern NSString * const URL_BLACKLIST;
extern NSString * const DATA_LOCATION_KEY;
extern NSString * const MAXIMUM_SPACE_KEY;
extern NSString * const APP_WHITELIST;
extern NSString * const URL_WHITELIST;
extern NSString * const ALT_DATABASE_KEY;

@property (nonatomic, strong) VideoWindowController *videoController;
@property (nonatomic, strong) SVBrowsingWindowController *browsingController;
@property (nonatomic, strong) MiniWindowController *miniWindowController;
// can probably get rid of filter string and just check for the status of the menu item. 
@property (nonatomic, strong) NSString *filterString;
@property (nonatomic, strong) ScreenRecorder *screenRecorder;
@property (nonatomic) BOOL shouldRecordAccessibilityText;

@property (weak) IBOutlet NSMenu *statusMenu;
@property (nonatomic, strong) NSStatusItem *statusItem;
@property (nonatomic, strong) StatusItemView *statusItemView;
@property (weak) IBOutlet NSMenuItem *visibilityItem;
@property (weak) IBOutlet NSMenuItem *windowVisibilityItem;
@property (weak) IBOutlet NSMenuItem *recordingItem;
@property (weak) IBOutlet NSMenuItem *launchItem;
@property (weak) IBOutlet NSMenuItem *filterItem;
@property (weak) IBOutlet NSMenuItem *chooseBlacklistItem;
@property (weak) IBOutlet NSMenu *databaseMenu;


@property (weak) IBOutlet NSMenu *appBlacklistMenu;
@property (weak) IBOutlet NSMenu *urlBlacklistMenu;
@property (weak) IBOutlet NSMenu *appWhitelistMenu;
@property (weak) IBOutlet NSMenu *urlWhitelistMenu;

@property (weak) IBOutlet NSMenuItem *appBlacklistMenuItem;
@property (weak) IBOutlet NSMenuItem *urlBlacklistMenuItem;
@property (weak) IBOutlet NSMenuItem *appWhitelistMenuItem;
@property (weak) IBOutlet NSMenuItem *urlWhitelistMenuItem;



@property (nonatomic, strong) IBOutlet SUUpdater *sparkleUpdater;

- (IBAction)toggleRecording:(NSMenuItem *)sender;
- (IBAction)toggleLaunchOnStartup:(id)sender;
- (IBAction)toggleFilterCondition:(NSMenuItem *)sender;
- (IBAction)toggleBlacklistCondition:(NSMenuItem *)sender;
- (IBAction)addToAppBlacklist:(NSMenuItem *)sender;
- (IBAction)addToUrlBlacklist:(NSMenuItem *)sender;
- (IBAction)addToAppWhitelist:(NSMenuItem *)sender;
- (IBAction)addToUrlWhitelist:(NSMenuItem *)sender;
- (IBAction)toggleVisibility:(NSMenuItem *)sender;
- (IBAction)toggleWindowVisiblity:(NSMenuItem *)sender;
- (IBAction)exitSavant:(NSMenuItem *)sender;

- (IBAction)switchDb:(NSMenuItem *)sender;




+ (instancetype)sharedInstance;

@end

