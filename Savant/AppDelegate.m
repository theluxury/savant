       //
//  AppDelegate.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "AppDelegate.h"
#import "NSURL+SavantLocations.h"
#import "TextNode.h"
#import "LaunchAtStartupHelper.h"
#import <HockeySDK/HockeySDK.h>
#import "DataManager.h"
#import "GAHelper.h"
#import "NSArray+RemoveObject.h"
#import "BlacklistHelper.h"
#import "AlternateDatabaseHelper.h"
#import "NSApplication+Restart.h"
#import "System Preferences.h"
#import "TaggingViewController.h"

#import <Carbon/Carbon.h>

@interface AppDelegate ()
@property (nonatomic, strong) NSTimer *applicationCheckTimer;

@end

@implementation AppDelegate

NSString * const filterSort = @"filterSort";
NSString * const mostUsed = @"mostUsed";
NSString * const mostRecent = @"mostRecent";
NSString * const FILTER_BLACKLIST_SORT = @"blacklistFilterSort";
NSString * const FILTER_BLACKLIST = @"filterBlacklist";
NSString * const FILTER_WHITELIST = @"filterWhitelist";
NSString * const blacklist = @"blacklist";
NSString * const URL_BLACKLIST = @"urlBlacklist";
NSString * const APP_WHITELIST = @"appWhitelist";
NSString * const URL_WHITELIST = @"urlWhitelist";
NSString * const DATA_LOCATION_KEY = @"dataLocation";
NSString * const MAXIMUM_SPACE_KEY = @"maximumSpace"; // In GB
NSString * const ALT_DATABASE_KEY = @"altDatabases";

// TODO: Use keycodes directly from HIToolbox instead?
#define justEsc 53
#define KEY_K 40
#define KEY_P 35
#define KEY_LEFT_ARROW 123
#define KEY_RIGHT_ARROW 124

+ (instancetype)sharedInstance
{
    return [NSApplication sharedApplication].delegate;
}

- (id)init
{
    if (self = [super init]) {
        _filterString = @"count";
        [self setupHotkeyListener];
    }
    return self;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    #ifndef DEBUG
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"42c8f39d7e0c58319959db66fc968c2b"];
        [[BITHockeyManager sharedHockeyManager].crashManager setAutoSubmitCrashReport:YES];
        [[BITHockeyManager sharedHockeyManager] startManager];
        _sparkleUpdater.sendsSystemProfile = YES;
    #endif
    
    // I guess we can let them use it without this feature (and display some notice by the search), though kinda pointless since is captures the screen anyway
    if(!AXIsProcessTrusted()){
        NSLog(@"Accessibility not enabled!");
        
        NSAlert *alert = [NSAlert alertWithMessageText:@"Accessibility not enabled" defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@"To enable text search and copy/pasting from screenshots, click 'Click the lock to make changes', enter your user password, and click on Savant. Afterwards, please restart Savant."];
        SystemPreferencesApplication *application = (SystemPreferencesApplication *)[SBApplication applicationWithBundleIdentifier:@"com.apple.systempreferences"];
        [application activate];
        
        SystemPreferencesPane *pane = [application.panes objectWithID:@"com.apple.preference.security"];
        SystemPreferencesAnchor *anchor = [pane.anchors objectWithName:@"Privacy_Accessibility"];
        [anchor reveal];
        [alert runModal];
        
        // This makes it exit to force them to turn on accessibility.
        [NSApp performSelector:@selector(terminate:) withObject:nil afterDelay:0.0];
    } else {
        // TODO: update if changed?
        self.shouldRecordAccessibilityText = YES;
    }
    
    
    [self initializeDefaults];
    [self initializeSavantFiles];
    [self setupMenuBar];
    [self fileNotifications];
    [[GAHelper sharedInstance] startRecording];
    
    
    self.screenRecorder = [[ScreenRecorder alloc] init];
    [self.screenRecorder startCapture];
    
    self.videoController = [[VideoWindowController alloc] init];
    self.browsingController = [[SVBrowsingWindowController alloc] init];
    self.miniWindowController = [[MiniWindowController alloc] init];
    // Need this because otherwise the window is considered visible for some reason. This probably isn't the best way to do this, but fine for now.
    [self.miniWindowController.window orderOut:self];
    
    // This makes it add the other people's dbs to be searchable. 
    [AlternateDatabaseHelper addAllDbs];
    
    [NSApp setServicesProvider:self];
}

- (void)findInSavant:(NSPasteboard *)pboard
         userData:(NSString *)userData error:(NSString **)error {
    // Test for strings on the pasteboard.
    NSArray *classes = [NSArray arrayWithObject:[NSString class]];
    NSDictionary *options = [NSDictionary dictionary];
    
    if (![pboard canReadObjectForClasses:classes options:options]) {
        *error = NSLocalizedString(@"Error: couldn't encrypt text.",
                                   @"pboard couldn't give string.");
        return;
    }
    
    NSString *pboardString = [pboard stringForType:NSPasteboardTypeString];
    [self.browsingController toggleVisibility:pboardString];
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    [[DataManager sharedInstance] save];
    [[GAHelper sharedInstance] endRecording];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag
{
    if(![self.browsingController isActive]){
        [self toggleVisibility:nil];
    }
    return NO;
}

#pragma mark Launch Setup

- (void)initializeSavantFiles
{
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtURL:[NSURL SVDataDirectory]  withIntermediateDirectories:YES attributes:nil error:nil];
    [manager createDirectoryAtURL:[NSURL SVRecordingDirectory]  withIntermediateDirectories:YES attributes:nil error:nil];
}

- (void)initializeDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    [defaults registerDefaults:@{
                                 DATA_LOCATION_KEY: [self defaultDataLocation],
                                 MAXIMUM_SPACE_KEY: @10
                                 }];
    
    if (![defaults objectForKey:firstRun]) {
        [defaults setObject:[NSDate date] forKey:firstRun];
    }
    
    if (![defaults objectForKey:filterSort]) {
        [BlacklistHelper setUserDefaults:mostUsed forKey:filterSort];
    }
    
    if (![defaults objectForKey:FILTER_BLACKLIST_SORT]) {
        [BlacklistHelper setUserDefaults:FILTER_BLACKLIST_SORT forKey:FILTER_BLACKLIST];
    }
}
- (NSString *)defaultDataLocation
{
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL path];
}

- (void)setupMenuBar{
    // Create an NSStatusItem.
    
    self.statusItemView = [[StatusItemView alloc] init];
    self.statusItemView.activeImage = [NSImage imageNamed:@"menuBarIcon"];
    self.statusItemView.inactiveImage = [NSImage imageNamed:@"menuBarIconInactive"];
    self.statusItemView.menu = self.statusMenu;
    
    float width = 19.0;
    self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:width];
    
    self.statusItem.view = self.statusItemView;
    self.statusItemView.statusItem = self.statusItem;
    
    
    if ([LaunchAtStartupHelper isLaunchAtStartup])
        [_launchItem setState:NSOnState];
    else
        [_launchItem setState:NSOffState];
    
    // TODO: tidy (move to initializeDefaults -> registerDefaults)
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[defaults objectForKey:filterSort] isEqualTo:mostRecent]) {
        [_filterItem setTitle:@"Sort buttons by: most recent"];
        _filterString = @"mostRecent";
    }
    
    if ([[defaults objectForKey:FILTER_BLACKLIST_SORT] isEqualToString:FILTER_WHITELIST]) {
        [_chooseBlacklistItem setTitle:@"Filter by: Whitelist"];
    }
    
    
    [self setupBlacklistMenu];
    [self setupDatabaseMenu];
    [_recordingItem setState:NSOnState];
    self.statusItemView.isActive = YES;
}

- (void)setupBlacklistMenu {
    
    NSMutableDictionary *blacklistDictionary = [NSMutableDictionary dictionary];
    [blacklistDictionary setObject:_appBlacklistMenu forKey:blacklist];
    [blacklistDictionary setObject:_urlBlacklistMenu forKey:URL_BLACKLIST];
    [blacklistDictionary setObject:_appWhitelistMenu forKey:APP_WHITELIST];
    [blacklistDictionary setObject:_urlWhitelistMenu forKey:URL_WHITELIST];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    for (id key in blacklistDictionary) {
        if ([defaults objectForKey:key] && [[defaults objectForKey:key] count] != 0) {
            for (NSString *string in [defaults objectForKey:key]) {
                [BlacklistHelper addBlacklistMenuItem:string menu:[blacklistDictionary objectForKey:key]];
            }
        }
    }
    
    [self hideUnshownList];
}

- (void)setupDatabaseMenu {
    NSMenuItem *personalDatabaseItem = [[NSMenuItem alloc] init];
    [personalDatabaseItem setTitle:YOUR_DATABASE];
    [personalDatabaseItem setTarget:[AlternateDatabaseHelper class]];
    [personalDatabaseItem setAction:@selector(mainDatabaseItemClicked:)];
    [_databaseMenu insertItem:personalDatabaseItem atIndex:0];
     
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:ALT_DATABASE_KEY]) {
        for (NSDictionary *dict in [defaults objectForKey:ALT_DATABASE_KEY]) {
            [AlternateDatabaseHelper addMenuItem:_databaseMenu dictionary:dict];
        }
    }
    
}

- (void)hideUnshownList {
    if ([_chooseBlacklistItem.title isEqualToString:@"Filter by: Blacklist"]) {
        [_appBlacklistMenuItem setHidden:NO];
        [_urlBlacklistMenuItem setHidden:NO];
        [_appWhitelistMenuItem setHidden:YES];
        [_urlWhitelistMenuItem setHidden:YES];
    } else {
        [_appBlacklistMenuItem setHidden:YES];
        [_urlBlacklistMenuItem setHidden:YES];
        [_appWhitelistMenuItem setHidden:NO];
        [_urlWhitelistMenuItem setHidden:NO];
    }
    
}

- (IBAction)changeSaveLocation:(id)sender {
    NSAlert *alert = [[NSAlert alloc] init];
    alert.messageText = @"Where would you like Savant to save your data?";
    [alert addButtonWithTitle:@"Ok"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Choose Directory"];
    
    NSTextField *textField = [[NSTextField alloc] initWithFrame:NSMakeRect(0, 0, 400, 24)];
    [textField setStringValue:[[NSUserDefaults standardUserDefaults] stringForKey:DATA_LOCATION_KEY]];
    [textField selectText:self];
    [alert setAccessoryView:textField];
    
    NSModalResponse response = [alert runModal];
    if(response == NSAlertFirstButtonReturn){
        [self setSaveLocation:[textField stringValue]];
    } else if (response == NSAlertThirdButtonReturn) {
        NSOpenPanel* openDlg = [NSOpenPanel openPanel];
        [openDlg setCanChooseFiles:NO];
        [openDlg setCanChooseDirectories:YES];
        [openDlg setPrompt:@"Select"];
        NSModalResponse dlgResponse = [openDlg runModal];
        if (dlgResponse == NSAlertDefaultReturn) {
            [self setSaveLocation: [[openDlg URL] absoluteString]];
        }
    }
}
- (void)setSaveLocation:(NSString *)newLocation
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *curLocation = [defaults stringForKey:DATA_LOCATION_KEY];
    if(![curLocation isEqualToString:newLocation]){
        if (_screenRecorder.isRecording){
            [_screenRecorder stopCapture];
        }
        
        // If the save location is blank, reset it to the default
        if(!newLocation || [newLocation isEqualToString:@""]){
            newLocation = [self defaultDataLocation];
        }
        
        NSURL *curDataDir = [NSURL SVDataDirectory];
        [defaults setObject:newLocation forKey:DATA_LOCATION_KEY];
        [defaults synchronize];
        NSURL *newDataDir = [NSURL SVDataDirectory];
        [self initializeSavantFiles];
        
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager moveItemAtURL:curDataDir toURL:newDataDir error:nil];
        
        [NSApplication restart];
    }
}

- (IBAction)setMaximumSpace:(id)sender {
    NSAlert *alert = [[NSAlert alloc] init];
    alert.messageText = @"Set the maximum amount of storage space to be used by Savant (GB):";
    [alert addButtonWithTitle:@"Ok"];
    [alert addButtonWithTitle:@"Cancel"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTextField *textField = [[NSTextField alloc] initWithFrame:NSMakeRect(0, 0, 200, 24)];
    [textField setStringValue:[NSString stringWithFormat:@"%.01f", [defaults floatForKey:MAXIMUM_SPACE_KEY]]];
    [textField selectText:self];
    [alert setAccessoryView:textField];
    
    NSModalResponse response = [alert runModal];
    if(response == NSAlertFirstButtonReturn){
        [defaults setFloat:[textField floatValue] forKey:MAXIMUM_SPACE_KEY];
        [[DataManager sharedInstance] enforceStorageConstraints];
    }
}

#pragma mark hotkeys

- (void)setupHotkeyListener {
    
    
    [NSEvent addLocalMonitorForEventsMatchingMask:NSKeyDownMask handler:^NSEvent *(NSEvent *event) {
        [self localEventListener:event];
        return event;
    }];
    
    [NSEvent addGlobalMonitorForEventsMatchingMask:NSKeyDownMask handler:^(NSEvent *event) {
        [self globalEventListener:event];
    }];
    

}

- (void)globalEventListener:(NSEvent *)event {
    
    // Only recognize if it's ctrl + command.
    NSUInteger flags = [event modifierFlags];
    if (!(flags & NSCommandKeyMask) || !(flags & NSControlKeyMask)) {
        return;
    }
    
    
    switch (event.keyCode) {
        case KEY_K:
            [self toggleVisibility:nil];
            break;
            
        case KEY_P:
            [self toggleWindowVisiblity:nil];
            break;

        case kVK_ANSI_T:
            [TaggingViewController presentTaggingViewForMoment:self.screenRecorder.lastSignificantMoment];
            break;
            
        default:
            break;
    }
}

- (void)localEventListener:(NSEvent *)event {
    NSUInteger flags = [event modifierFlags];
    
    switch (event.keyCode) {
        case justEsc:
            [self toggleVisibility:nil];
            break;
            
        case KEY_K:
            if ((flags & NSCommandKeyMask) && (flags & NSControlKeyMask))
                [self toggleVisibility:nil];
            break;
            
        case KEY_P:
            if ((flags & NSCommandKeyMask) && (flags & NSControlKeyMask))
                [self toggleWindowVisiblity:nil];
            break;
            
        default:
            break;
    }
}

- (IBAction)toggleRecording:(NSMenuItem *)sender {
    if ([_recordingItem state] == NSOnState) {
        [_recordingItem setState:NSOffState];
        self.statusItemView.isActive = NO;
        if (_screenRecorder.isRecording)
            [_screenRecorder stopCapture];
    } else {
        [_recordingItem setState:NSOnState];
        self.statusItemView.isActive = YES;
        if (!_screenRecorder.isRecording)
            [_screenRecorder startCapture];
    }
}

- (IBAction)toggleFilterCondition:(NSMenuItem *)sender {
    if ([_filterString isEqualToString:@"count"]) {
        [_filterItem setTitle:@"Sort buttons by: most recent"];
        _filterString = @"mostRecent";
        [BlacklistHelper setUserDefaults:mostRecent forKey:filterSort];
    } else {
        [_filterItem setTitle:@"Sort buttons by: most used"];
        _filterString = @"count";
        [BlacklistHelper setUserDefaults:mostUsed forKey:filterSort];
    }
}

- (IBAction)toggleBlacklistCondition:(NSMenuItem *)sender {
    if ([_chooseBlacklistItem.title isEqualToString:@"Filter by: Blacklist"]) {
        [_chooseBlacklistItem setTitle:@"Filter by: Whitelist"];
        [BlacklistHelper setUserDefaults:FILTER_WHITELIST forKey:FILTER_BLACKLIST_SORT];
    } else {
        [_chooseBlacklistItem setTitle:@"Filter by: Blacklist"];
        [BlacklistHelper setUserDefaults:FILTER_BLACKLIST forKey:FILTER_BLACKLIST_SORT];
    }
    
    [self hideUnshownList];
}

- (IBAction)toggleLaunchOnStartup:(id)sender {
    if ([_launchItem state] == NSOnState && [LaunchAtStartupHelper isLaunchAtStartup]) {
        [_launchItem setState:NSOffState];
        [LaunchAtStartupHelper toggleLaunchAtStartup];
    } else if ([_launchItem state] == NSOffState && ![LaunchAtStartupHelper isLaunchAtStartup]) {
        [_launchItem setState:NSOnState];
        [LaunchAtStartupHelper toggleLaunchAtStartup];
    } else {
        // This shouldn't ever happen?
        NSLog(@"Something dun goofed");
        [LaunchAtStartupHelper toggleLaunchAtStartup];
        if ([LaunchAtStartupHelper isLaunchAtStartup])
            [_launchItem setState:NSOnState];
        else
            [_launchItem setState:NSOffState];
    }
}

- (void)exitSavant:(NSMenuItem *)sender {
    [NSApp terminate:self];
}

- (IBAction)toggleVisibility:(NSMenuItem *)sender {
    [_browsingController toggleVisibility:[self logSelectedText]];
}

- (IBAction)toggleWindowVisiblity:(NSMenuItem *)sender {
    [_miniWindowController toggleVisibility:[self logSelectedText]];
}

- (NSString *)logSelectedText {
    AXUIElementRef systemWideElement = AXUIElementCreateSystemWide();
    AXUIElementRef focussedElement = NULL;
    AXError error = AXUIElementCopyAttributeValue(systemWideElement, kAXFocusedUIElementAttribute, (CFTypeRef *)&focussedElement);
    if (error != kAXErrorSuccess)
    {
        return nil;
    }
    AXValueRef selectedText = NULL;
    AXError selectedTextError = AXUIElementCopyAttributeValue(focussedElement, kAXSelectedTextAttribute, (CFTypeRef *)&selectedText);          //Copy selected text attribute from focussedElement to selectedText
    if (selectedTextError == kAXErrorSuccess)
    {
        NSString* selectedTextString = (__bridge NSString *)(selectedText);
        return selectedTextString;// Selected text value retrieved
    }
    else
    {
        return nil;
    }
}

#pragma mark blacklist

- (IBAction)addToAppBlacklist:(NSMenuItem *)sender {
    NSString *prompt = @"Would you like to add an app to the recording blacklist? Apps that are blacklisted will not be recorded. Any app with a name containing the string you add will be blacklisted.";
    [BlacklistHelper addToBlacklist:prompt blacklistType:blacklist];
}

- (IBAction)addToUrlBlacklist:(NSMenuItem *)sender {
    NSString *prompt = @"Would you like to add a URL to the recording blacklist? URLs that are blacklisted will not be recorded. Any website with a URL containing the string you add will be blacklisted.";
    [BlacklistHelper addToBlacklist:prompt blacklistType:URL_BLACKLIST];
}


- (IBAction)addToAppWhitelist:(NSMenuItem *)sender {
    NSString *prompt = @"Would you like to add an application to the recording whitelist? Non browser application not containing the string you add will not be recorded.";
    [BlacklistHelper addToBlacklist:prompt blacklistType:APP_WHITELIST];
}

- (IBAction)addToUrlWhitelist:(NSMenuItem *)sender{
    NSString *prompt = @"Would you like to add a URL to the recording whitelist? URLs not containing the string you add will not be recorded.";
    [BlacklistHelper addToBlacklist:prompt blacklistType:URL_WHITELIST];
}


#pragma mark wake/sleep notifications

- (void) receiveSleepNote: (NSNotification*) note
{
    if (_screenRecorder.isRecording)
        [_screenRecorder stopCapture];
}

- (void) receiveWakeNote: (NSNotification*) note
{
    if ([_recordingItem state] == NSOnState && !_screenRecorder.isRecording)
        [_screenRecorder startCapture];
}

- (void) fileNotifications
{
    //These notifications are filed on NSWorkspace's notification center, not the default
    // notification center. You will not receive sleep/wake notifications if you file
    //with the default notification center.
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                           selector: @selector(receiveSleepNote:)
                                                               name: NSWorkspaceWillSleepNotification object: NULL];
    
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                           selector: @selector(receiveWakeNote:)
                                                               name: NSWorkspaceDidWakeNotification object: NULL];
}


#pragma mark Sparkle Delegate

- (NSArray *)feedParametersForUpdater:(SUUpdater *)updater
                 sendingSystemProfile:(BOOL)sendingProfile {
    return [[BITSystemProfile sharedSystemProfile] systemUsageData];
}

//# kindof a hack
- (IBAction)handleDelete:(id)sender {
    [self.browsingController deleteSelection];
}

- (IBAction)switchDb:(NSMenuItem *)sender {
    
     NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    [openDlg setCanChooseFiles:NO];
    [openDlg setCanChooseDirectories:YES];
    [openDlg setPrompt:@"Select"];
    NSModalResponse dlgResponse = [openDlg runModal];
    if (dlgResponse == NSAlertAlternateReturn)
        return;
    
    // After choose directory, might as well let them name it.
    NSAlert *alert = [[NSAlert alloc] init];
    alert.messageText = @"What would you like to name this database";
    [alert addButtonWithTitle:@"Ok"];
    [alert addButtonWithTitle:@"Cancel"];
    
    NSTextField *textField = [[NSTextField alloc] initWithFrame:NSMakeRect(0, 0, 400, 24)];
    [textField setStringValue:@"Other Database"];
    [textField selectText:self];
    [alert setAccessoryView:textField];
    
    NSModalResponse response = [alert runModal];
    if(response == NSAlertFirstButtonReturn){
        NSMutableDictionary *dict = [AlternateDatabaseHelper writeToUserDefaults:[textField stringValue] url:[openDlg URL]];
        
        //This happens if the name they chose is already taken
        if (!dict) {
            NSAlert *renameAlert = [[NSAlert alloc] init];
            renameAlert.messageText = @"You already have an alternate database with that name. Please choose another name";
            [renameAlert runModal];
            return;
        }
       
        // Then add the menu item and set the listener. 
        [AlternateDatabaseHelper addMenuItem:_databaseMenu dictionary:dict];
    }
}


@end
