//
//  BlacklistHelper.h
//  Savant
//
//  Created by Mark Wang on 10/27/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BlacklistHelper : NSObject

+(void)addToBlacklist:(NSString *)prompt blacklistType:(NSString *)blacklistType;

+(void)addBlacklistMenuItem:(NSString *)string menu:(NSMenu *) menu;

+ (void)setUserDefaults:(id)object forKey:(NSString *)key;

@end
