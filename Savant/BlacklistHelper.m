//
//  BlacklistHelper.m
//  Savant
//
//  Created by Mark Wang on 10/27/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "BlacklistHelper.h"
#import "AppDelegate.h"
#import "NSArray+RemoveObject.h"

@implementation BlacklistHelper

+ (void)addToBlacklist:(NSString *)prompt blacklistType:(NSString *)blacklistType{
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;

    NSMenu *menu;
    if ([blacklistType isEqualToString:blacklist])
        menu = d.appBlacklistMenu;
    else if ([blacklistType isEqualToString:URL_BLACKLIST])
        menu = d.urlBlacklistMenu;
    else if ([blacklistType isEqualToString:APP_WHITELIST])
        menu = d.appWhitelistMenu;
    else if ([blacklistType isEqualToString:URL_WHITELIST])
        menu = d.urlWhitelistMenu;

    
    
    NSString *string;
    NSAlert *alert = [NSAlert alertWithMessageText: prompt
                                     defaultButton:@"OK"
                                   alternateButton:@"Cancel"
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    NSTextField *input = [[NSTextField alloc] initWithFrame:NSMakeRect(0, 0, 200, 24)];
    [alert setAccessoryView:input];
    NSInteger button = [alert runModal];
    if (button == NSAlertDefaultReturn) {
        [input validateEditing];
        string = [[input stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([string isEqualTo:@""])
            return;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray *blacklistArray = [NSMutableArray array];
        if ([defaults objectForKey:blacklistType])
            blacklistArray = [[defaults objectForKey:blacklistType] mutableCopy];
        
        [blacklistArray addObject:string];
        [self setUserDefaults:blacklistArray forKey:blacklistType];
        [self addBlacklistMenuItem:string menu:menu];
    }
}

+ (void)addBlacklistMenuItem:(NSString *)string menu:(NSMenu *) menu{
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:string
                                                  action:@selector(blacklistItemClicked:)
                                           keyEquivalent:@""];
    [item setTarget:[BlacklistHelper class]];
    [menu insertItem:item atIndex:0];
}

+ (void)blacklistItemClicked:(NSMenuItem *)menuItem {
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    NSString *prompt;
    NSMenu *menu = [menuItem menu];
    NSString *blacklistType;
    if ([[menuItem menu] isEqualTo:d.appBlacklistMenu]) {
        prompt = [NSString stringWithFormat:@"Currently not recording all apps with the string %@ in title. Remove string from blacklist?", [menuItem title]];
        blacklistType = blacklist;
    } else if ([[menuItem menu] isEqual:d.urlBlacklistMenu]) {
        prompt = [NSString stringWithFormat:@"Currently not recording all websites with the string %@ in URL. Remove string from blacklist?", [menuItem title]];
        blacklistType = URL_BLACKLIST;
    } else if ([[menuItem menu] isEqual:d.appWhitelistMenu]) {
        prompt = [NSString stringWithFormat:@"Currently recording all applications with the string %@ in title. Remove string from whitelist?", [menuItem title]];
        blacklistType = APP_WHITELIST;
    }else if ([[menuItem menu] isEqual:d.urlBlacklistMenu]) {
        prompt = [NSString stringWithFormat:@"Currently recording all websites with the string %@ in URL. Remove string from whitelist?", [menuItem title]];
        blacklistType = URL_WHITELIST;
    }
    
    NSAlert *alert = [NSAlert alertWithMessageText: prompt
                                     defaultButton:@"OK"
                                   alternateButton:@"Cancel"
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    NSInteger button = [alert runModal];
    if (button == NSAlertDefaultReturn) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *blacklistArray = [defaults objectForKey:blacklistType];
        
        // Shouldn't happen but meh.
        if (!blacklistArray || [blacklistArray indexOfObjectIdenticalTo:[menuItem title]] == NSNotFound) {
            [menu removeItem:menuItem];
            return;
        }
        blacklistArray = [blacklistArray savantRemoveObject:[menuItem title]];
        // Order here is important. Don't remove from menu before calling this.
        [BlacklistHelper setUserDefaults:blacklistArray forKey:blacklistType];
        [menu removeItem:menuItem];
    }
}



+ (void)setUserDefaults:(id)object forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
