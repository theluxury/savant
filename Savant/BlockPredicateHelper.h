//
//  BlockPredicateHelper.h
//  Savant
//
//  Created by Mark Wang on 11/2/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Moment.h"
#import "MomentBlock.h"

@interface BlockPredicateHelper : NSObject

extern const NSInteger BLOCK_FETCH_LIMIT;

+ (NSArray *)fetchBlocks:(NSPredicate *)predicate storeArray:(NSArray *)storeArray withLimit:(NSInteger)limit ascending:(BOOL)ascending;

+(Moment *)getFirstMoment:(MomentBlock *)momentBlock storeArray:(NSArray *)array string:(NSString *)string;
+(MomentBlock *)getBlockFromMoment:(Moment *)moment storeArray:(NSArray *)storeArray;

+ (NSPredicate *)getNewCombinedPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow string:(NSString *)string;

+ (NSPredicate *)getNewCombinedPredicateWithSelection:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow timeGap:(NSInteger)timeGap string:(NSString *)string moment:(MomentBlock *)momentBlock after:(BOOL)after;


@end
