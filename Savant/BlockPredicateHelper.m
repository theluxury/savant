//
//  BlockPredicateHelper.m
//  Savant
//
//  Created by Mark Wang on 11/2/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "BlockPredicateHelper.h"
#import "DataManager.h"
#import "SharedPredicateHelper.h"
#import "PredicateHelper.h"

@implementation BlockPredicateHelper

const NSInteger BLOCK_FETCH_LIMIT = 50;

+ (NSArray *)fetchBlocks:(NSPredicate *)predicate storeArray:(NSArray *)storeArray withLimit:(NSInteger)limit ascending:(BOOL)ascending {
    
    return [SharedPredicateHelper fetch:@"momentBlock" storeArray:storeArray predicate:predicate withLimit:limit ascending:ascending];
}

+(Moment *)getFirstMoment:(MomentBlock *)momentBlock storeArray:(NSArray *)storeArray string:(NSString *)string {
    // So fetch a moment, with parentBlock like this one, then sort it, and return it. If has search term, be sure to return one with the actual text.
    
    NSPredicate *blockPredicate;
    
    if (!string)
        blockPredicate = [NSPredicate predicateWithFormat:@"momentBlock == %@", momentBlock];
    else
        blockPredicate = [NSPredicate predicateWithFormat:@"(momentBlock == %@) and (any textState.textNodes.text contains[cd] %@)", momentBlock, string];
    
    
    NSArray *array = [PredicateHelper fetchScreenshots:blockPredicate storeArray:storeArray withLimit:1 ascending:EARLIEST];
    
    if (array && [array count] != 0)
        return array[0];
    // TODO: clean up the following terrible code. It probably won't happen much, so probably okay?
    else {
        // returning nil here is probably also bad. should just return a close one.
        NSLog(@"returning approximate moment");
        // so assuming momentBlock exists, just return the moment right after. if that doesn't exist, right before I guess.
        NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp >= %@", momentBlock.startTime];
        NSArray *newArray = [PredicateHelper fetchScreenshots:timePredicate storeArray:storeArray withLimit:1 ascending:EARLIEST];
        if (newArray && [newArray count] != 0)
            return newArray[0];
        else {
            NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp <= %@", momentBlock.startTime];
            NSArray *newArray = [PredicateHelper fetchScreenshots:timePredicate storeArray:storeArray withLimit:1 ascending:MOST_RECENT];
            if (newArray && [newArray count] != 0)
                return newArray[0];
        }
        return nil;
    }
}

+ (MomentBlock *)getBlockFromMoment:(Moment *)moment storeArray:(NSArray *)storeArray {
    NSPredicate *blockPredicate = [NSPredicate predicateWithFormat:@"any moment == %@", moment];
    
    NSArray *array = [self fetchBlocks:blockPredicate storeArray:storeArray withLimit:1 ascending:EARLIEST];
    
    if (array && [array count] != 0)
        return array[0];
    else {
        NSLog(@"returning approximate momentBlock");
        // So try to return the momentBlock with the startTime immediately preceding it. That should likely be the right one.
        NSPredicate *newPredicate = [NSPredicate predicateWithFormat:@"startTime <= %@", moment.timestamp];
        NSArray *newArray = [self fetchBlocks:newPredicate storeArray:storeArray withLimit:1 ascending:MOST_RECENT];
        
        if (newArray && [newArray count] != 0)
            return newArray[0];
        else {
            // So can't even get a block right before, so get after I guess...
            NSPredicate *newPredicate = [NSPredicate predicateWithFormat:@"startTime > %@", moment.timestamp];
            NSArray *newArray = [self fetchBlocks:newPredicate storeArray:storeArray withLimit:1 ascending:EARLIEST];
            
            if (newArray && [newArray count] != 0)
                return newArray[0];
        }
        return nil;
    }
}


+ (NSPredicate *)getNewCombinedPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow string:(NSString *)string {
    
    // Button predicate is the exact same. DRY code!
    NSPredicate *buttonPredicate = [SharedPredicateHelper setButtonPredicate:isBlue isRed:isRed isGreen:isGreen isYellow:isYellow];
    // Search predicate is pretty much the exact same, but just with any moment in front of the stuff.
    NSPredicate *searchPredicate = [SharedPredicateHelper setSearchPredicate:string entity:@"momentBlock"];
    
    // So before, time gap predicate always existed. Not for this one.
    if (!buttonPredicate && !searchPredicate)
        return nil;
    else if (!searchPredicate)
        return buttonPredicate;
    else if (!buttonPredicate)
        return searchPredicate;
    else
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:buttonPredicate,  searchPredicate, nil]];
}

+ (NSPredicate *)getNewCombinedPredicateWithSelection:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow timeGap:(NSInteger)timeGap string:(NSString *)string moment:(MomentBlock *)momentBlock after:(BOOL)after {
    
    // So this is where, um.... the sausage gets made.
    NSPredicate *predicate = [self getNewCombinedPredicate:isBlue isRed:isRed isGreen:isGreen isYellow:isYellow string:string];
    
    NSPredicate *timePredicate;
    // Then adds a filter to make it only look for the stuff before or after the moment's time.
    if (after) {
        timePredicate = [NSPredicate predicateWithFormat:@"startTime > %@", [momentBlock startTime]];
    } else {
        timePredicate = [NSPredicate predicateWithFormat:@"startTime <= %@", [momentBlock startTime]];
    }
    
    // TODO: apparently, momentBlock startTime can be nul sometimes. check why. 
    
    // Forgot had to do check here to see if one of these is nil. timePredicate should never be nil.
    if (predicate == nil)
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: timePredicate, nil]];
    else
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: timePredicate, predicate, nil]];
    
}

@end
