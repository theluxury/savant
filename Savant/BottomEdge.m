//
//  BottomEdge.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "BottomEdge.h"

@implementation BottomEdge

- (instancetype)initWithFrame:(NSRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addShadow];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self addShadow];
    }
    return self;
}

- (void)addShadow {
    NSShadow *dropShadow = [[NSShadow alloc] init];
    [dropShadow setShadowColor:[NSColor blackColor]];
    [dropShadow setShadowOffset:NSMakeSize(0, 2.0)];
    [dropShadow setShadowBlurRadius:4.0];
    
    [self.superview setWantsLayer:YES];
    [self setShadow: dropShadow];
}

@end
