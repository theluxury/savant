//
//  BrowserHelper.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-07.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SpecialAppHelper.h"

extern NSString *const NSAccessibilityWebAreaRole;


// This is an abstract class; subclasses must override webAreaPath
@interface BrowserHelper : SpecialAppHelper

+ (NSArray *)webAreaPath;

@end
