//
//  BrowserHelper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-07.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "BrowserHelper.h"

NSString *const NSAccessibilityWebAreaRole = @"AXWebArea";


@implementation BrowserHelper

+ (NSString *)getURLForWindow:(AXWrapper *)window
{
    return [[[window childAtPath:self.webAreaPath] getAttribute:NSAccessibilityURLAttribute] absoluteString];
}
+ (NSString *)getTitleForWindow:(AXWrapper *)window
{
    return window.title;
}

+ (NSArray *)webAreaPath
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

@end
