//
//  ChromeHelper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-07.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ChromeHelper.h"

// Because the accessibility API is ridiculously awful, there isn't actually a way to get
// elements by the designated identifier property, so we're not using this for now
// (we could potentially maintain our own table in the application mirror)
NSString *const kChromeURLElementIdentifier = @"_NS:41";


@implementation ChromeHelper

+ (NSArray *)webAreaPath
{
    return @[NSAccessibilityScrollAreaRole, NSAccessibilityWebAreaRole];
}
            
@end
