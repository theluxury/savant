//
//  DataManager.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "Moment.h"

@interface DataManager : NSObject

+ (DataManager *)sharedInstance;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainMOC;
@property (readonly, strong, nonatomic) NSManagedObjectContext *recordingMOC;


@property (readonly, nonatomic, strong) NSCache *imageCache;
@property (readonly, nonatomic, strong) NSCache *fetchOperationsCache; // NSCache for the thread safety
@property (readonly, nonatomic, strong) NSOperationQueue *imageFetchingQueue;


- (ImageFetchOperation *)createFetchOperationForMoment:(Moment *)moment;

- (void)beginSaving;
- (void)stopSaving;
- (void)save;

- (void)beginEnforcingStorageConstraints;
- (void)stopEnforcingStorageConstraints;
- (void)enforceStorageConstraints;

- (int)checkTimegapArray:(Moment *)moment;
- (void)updateMomentBlock:(Moment *)moment;
- (void)incrementCount:(Moment *)moment;

- (void)deleteMoments:(NSSet *)momentsToDelete;


@end
