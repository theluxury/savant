//
//  DataManager.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "DataManager.h"
#import "AppDelegate.h"
#import "ApplicationCount.h"
#import "PredicateHelper.h"
#import "NSURL+SavantLocations.h"
#import "NSFileManager+FolderSize.h"

#import "ImageFetchOperation.h"
#import "MomentBlock.h"
#import "AlternateDatabaseHelper.h"

//TODO: experiment with this
#define kImageCacheSize 80
int ONE_SECOND = 1;
int FIVE_SECONDS = 5;
int THIRTY_SECONDS = 30;
int ONE_MIN = 60;
int FIVE_MIN = 60 * 5;
int THIRTY_MIN = 60 * 30;
int ONE_HOUR = 60 * 60;
int FOUR_HOUR = 60 * 60 * 4;
int ONE_DAY = 60 * 60 * 24;

NSTimeInterval DELETE_INTERVAL = 600; // 10 min
int DELETE_CHUNK_SIZE = 1200; // 20 min; this should probably be greater than the delete interval

@interface DataManager ()
@property (strong, nonatomic) NSManagedObjectContext *persistanceMOC;
@property (strong, nonatomic) NSTimer *saveTimer;
@property (strong, nonatomic) NSTimer *storageConstraintTimer;

@property (nonatomic, strong) NSMutableDictionary *activeApplicationDictionary;
@property (nonatomic, strong) NSMutableArray *activeApplicationCountArray;
@property (nonatomic, strong) MomentBlock *momentBlock;

@end

@implementation DataManager

+ (DataManager *)sharedInstance
{
    // Can change this maybe so that it doesn't set no nil?
    static DataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    if(self = [super init]){
        _imageCache = [[NSCache alloc] init];
        [_imageCache setCountLimit:kImageCacheSize];
        
        _imageFetchingQueue = [[NSOperationQueue alloc] init];
        _fetchOperationsCache = [[NSCache alloc] init];
    }
    return self;
}

#pragma mark application count and most recent


- (void)incrementCount:(Moment *)moment {
    
    if (!_activeApplicationCountArray) {
        _activeApplicationCountArray = [NSMutableArray array];
        NSArray *array = [PredicateHelper fetchFilterApplications:@"count" storeArray:@[[AlternateDatabaseHelper getMainStore]] limit:0];
        if (array) {
            [_activeApplicationCountArray addObjectsFromArray:array];
        }
        
        // This deletes the same count object if it has more than one. Shouldn't, but better safe than sorry.
        NSMutableArray *toRemoveCountObjects = [NSMutableArray array];
        if ([_activeApplicationCountArray count] > 1) {
            for (int i = 0; i < [_activeApplicationCountArray count] - 1; i ++) {
                for (int j = i + 1; j < [_activeApplicationCountArray count]; j++) {
                    ApplicationCount *count1 = _activeApplicationCountArray[i];
                    ApplicationCount *count2 = _activeApplicationCountArray[j];
                    if ([count1 isEqualTo:count2]) {
                        [toRemoveCountObjects addObject:count2];
                    }
                }
            }
        }
        
        if ([toRemoveCountObjects count] != 0) {
            for (ApplicationCount *appCount in toRemoveCountObjects) {
                [self.recordingMOC deleteObject:appCount];
            }
        }
    }
    
    for (ApplicationCount *appCount in _activeApplicationCountArray) {
        if ([moment.activeApplication isEqualToString:appCount.name]) {
            NSInteger count =[appCount.count integerValue];
            count++;
            appCount.count = [NSNumber numberWithInteger:count];
            appCount.mostRecent = [NSDate date];
            
            // Checks if icon has changed. If it has, go with new one.
            NSString *currentAppBundle = [self bundleIdentifierForApplicationName:moment.activeApplication];
            
            // apparently this crashes it if currentAppBundle is nil?
            if (!currentAppBundle || [currentAppBundle isEqualToString:@""])
                return;
            
            NSArray *runningAppArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:currentAppBundle];
            
            if (!runningAppArray || [runningAppArray count] == 0)
                return;
            
            NSRunningApplication *runningApp = [runningAppArray objectAtIndex:0];
            // if (![[appCount icon] isEqualTo:[[NSWorkspace sharedWorkspace] frontmostApplication].icon])
            if (![[appCount icon] isEqualTo:runningApp.icon])
                appCount.icon = runningApp.icon;
            return;
        }
    }
    
    // Should only get here if the app does not exist in the array.
    
    ApplicationCount *appCount = [NSEntityDescription insertNewObjectForEntityForName:@"ApplicationCount" inManagedObjectContext:[DataManager sharedInstance].recordingMOC];
//    NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
//    NSPersistentStore *mainStore = [psc persistentStoreForURL:[NSURL SVPersistentStoreUrl]];
//    [[DataManager sharedInstance].recordingMOC assignObject:appCount toPersistentStore:mainStore];
    
    appCount.count = [NSNumber numberWithInteger:1];
    appCount.name = moment.activeApplication;
    appCount.mostRecent = [NSDate date];
    [_activeApplicationCountArray addObject:appCount];
        
    // Gets the icon
    NSString *currentAppBundle = [self bundleIdentifierForApplicationName:moment.activeApplication];
    // apparently this crashes it if currentAppBundle is nil?
    if (!currentAppBundle || [currentAppBundle isEqualToString:@""])
        return;
    
    NSArray *runningAppArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:currentAppBundle];
        
    if (!runningAppArray || [runningAppArray count] == 0)
        return;
        
    NSRunningApplication *runningApp = [runningAppArray objectAtIndex:0];
    // if (![[appCount icon] isEqualTo:[[NSWorkspace sharedWorkspace] frontmostApplication].icon])
    appCount.icon = runningApp.icon;
}

- (NSString *) bundleIdentifierForApplicationName:(NSString *)appName
{
    NSWorkspace * workspace = [NSWorkspace sharedWorkspace];
    NSString * appPath = [workspace fullPathForApplication:appName];
    if (appPath) {
        NSBundle * appBundle = [NSBundle bundleWithPath:appPath];
        return [appBundle bundleIdentifier];
    }
    return nil;
}

#pragma mark - Core Data stack

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize mainMOC = _mainMOC;
@synthesize recordingMOC = _recordingMOC;

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
    NSMutableDictionary *pragmaOptions = [NSMutableDictionary dictionary];
    [pragmaOptions setObject:@"WAL" forKey:@"journal_mode"];
    [pragmaOptions setObject:@"NORMAL" forKey:@"synchronous"];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, pragmaOptions, NSSQLitePragmasOption, nil];
    
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    if (!error) {
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                       configuration:nil
                                                 URL:[[NSURL SVDataDirectory] URLByAppendingPathComponent:@"SavantData.storedata"]
                                             options:options
                                               error:&error]) {
            coordinator = nil;
        }
        _persistentStoreCoordinator = coordinator;
    }
    
    if (error) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)mainMOC
{
    if (!_mainMOC){
        _mainMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _mainMOC.parentContext = self.persistanceMOC;
        
        [[_mainMOC undoManager] disableUndoRegistration];
    }
    
    return _mainMOC;
}

- (NSManagedObjectContext *)recordingMOC
{
    if (!_recordingMOC){
        _recordingMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _recordingMOC.parentContext = self.mainMOC;
        
        [[_recordingMOC undoManager] disableUndoRegistration];
    }
    
    return _recordingMOC;
}

- (NSManagedObjectContext *)persistanceMOC
{
    if (_persistanceMOC) return _persistanceMOC;

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _persistanceMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_persistanceMOC setPersistentStoreCoordinator:coordinator];

    [[_persistanceMOC undoManager] disableUndoRegistration];
    
    return _persistanceMOC;
}


- (void)beginSaving
{
    if(!self.saveTimer){
        self.saveTimer = [NSTimer timerWithTimeInterval:10
                                                 target:self
                                               selector:@selector(save)
                                               userInfo:nil
                                                repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.saveTimer forMode:NSDefaultRunLoopMode];
    }
}
- (void)stopSaving
{
    // Invalidate on the main thread so it's removed from the right run loop
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.saveTimer invalidate];
        self.saveTimer = nil;
    });
}

- (void)save
{
    [self.mainMOC performBlock:^{
        [self.mainMOC save:nil];
        
        [self.persistanceMOC performBlock:^{
            [self.persistanceMOC save:nil];
        }];
    }];
}


// The timers could probably be done more dryly
- (void)beginEnforcingStorageConstraints
{
    [self enforceStorageConstraints];
    self.storageConstraintTimer = [NSTimer timerWithTimeInterval:DELETE_INTERVAL
                                                          target:self
                                                        selector:@selector(enforceStorageConstraints)
                                                        userInfo:nil
                                                         repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.storageConstraintTimer forMode:NSDefaultRunLoopMode];
}
- (void)stopEnforcingStorageConstraints
{
    // Invalidate on the main thread so it's removed from the right run loop
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.storageConstraintTimer invalidate];
        self.storageConstraintTimer = nil;
    });
}

- (void)enforceStorageConstraints
{
    // Don't delete things while the user is looking
    AppDelegate *d = [AppDelegate sharedInstance];
    if(d.browsingController.isActive){
        return;
    }
    
    float maxGB = [[NSUserDefaults standardUserDefaults] floatForKey:MAXIMUM_SPACE_KEY];
    unsigned long long usedSpace = [[NSFileManager defaultManager] sizeOfDirectoryAtURL:[NSURL SVDataDirectory]];
    float usedGB = usedSpace / 1e9;
    if(maxGB > usedGB){
       // Always delete a fixed-size chunk of moments; pretty crude, but should be ok for now.
        NSArray *toDelete = [Moment firstNMoments:DELETE_CHUNK_SIZE moc:self.mainMOC];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self deleteMoments:[NSSet setWithArray:toDelete]];
        });
    }
}


- (ImageFetchOperation *)createFetchOperationForMoment:(Moment *)moment
{
    ImageFetchOperation *op = [[ImageFetchOperation alloc] initWithMoment:moment];
    MomentID *mId = [moment objectID]; // ## the transient thing could be a problem after all
    [self.fetchOperationsCache setObject:op forKey:mId];
    // local var to avoid a retain cycle
    NSCache *fetchCache = self.fetchOperationsCache;
    op.completionBlock = ^{
        [fetchCache removeObjectForKey:mId];
    };
    return op;
}

#pragma mark timegap stuff

-(int)checkTimegapArray:(Moment *)moment
{
    // So have to redo this. So have to check for every single application...
    // So, first instantiate dictionary if it doesnt exist.
    if (!_activeApplicationDictionary) {
        _activeApplicationDictionary = [NSMutableDictionary dictionary];
    }
    
    // Then, check to see if the current activeApplication is in the dictionary. If not, set it.
    NSString *activeApplication = moment.activeApplication;
    
    // Apparently active application can be nil here. If that's true, return oneDay?.
    if (!activeApplication || [activeApplication isEqualToString:@""])
        return oneDay;
    NSMutableArray *activeApplicationArray = [NSMutableArray array];
    // TODO: check this also. Hopefully this solves an mutated while enumerated bug
    @synchronized(_activeApplicationDictionary) {
        if (![_activeApplicationDictionary objectForKey:activeApplication]) {
            [_activeApplicationDictionary setObject:activeApplicationArray forKey:activeApplication];
        
            NSFetchRequest *request = [self getRequest:activeApplication storeArray:@[[AlternateDatabaseHelper getMainStore]]];
            NSError *error;
            NSArray *array = [self.mainMOC executeFetchRequest:request error:&error];
            if (error) NSLog(@"Got an error trying to fetch core data %@", error);
        
            // This happens if is first time running after install.
            if (array == nil || [array count] == 0) {
                for (int i = 0; i < COUNT_TIMEGAP_ENUM; i++) {
                    [activeApplicationArray addObject:moment];
                }
                return oneDay;
            }
        
            // This happens if restarted app.
            [self setupActiveApplicationArray:activeApplicationArray activeApplication:activeApplication array:array request:request error:error];
        
        } else {
            activeApplicationArray = [_activeApplicationDictionary objectForKey:activeApplication];
        }
    }
    return [self checkTimeGap:moment array:activeApplicationArray];
}

- (void)setupActiveApplicationArray:(NSMutableArray *)activeApplicationArray activeApplication:(NSString *)activeApplication array:(NSArray *)array request:(NSFetchRequest *)request error:(NSError *)error {
    [activeApplicationArray addObject:[array objectAtIndex:0]];
    
    for (int i = fourHours; i >= oneSecond; i--) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"activeApplication ==[cd] %@ AND timeGap >= %i",activeApplication, i];
        [request setPredicate:predicate];
        NSArray *array = [self.mainMOC executeFetchRequest:request error:&error];
        [activeApplicationArray addObject:[array objectAtIndex:0]];
    }
}

-(NSFetchRequest *)getRequest:(NSString *)activeApplication storeArray:(NSArray *)storeArray {
    // if it's not found in the activeApplicationArray, means, um, it just restarted? So have to get the most recent items with the enum.
    // So first get the one with the biggest enum, which is um,.... oneDay.
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"activeApplication ==[cd] %@ AND timeGap == %i", activeApplication, oneDay];
    NSManagedObjectContext *moc = self.mainMOC;
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"Moment" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    [request setPredicate:predicate];
    [request setAffectedStores:storeArray];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timestamp" ascending:NO];
    [request setSortDescriptors:@[sortDescriptor]];
    // Just need one
    [request setFetchLimit:1];
    return request;
}



- (int) checkTimeGap:(Moment *)moment array:(NSMutableArray *)activeApplicationArray {
    // So now we definitely have the array. So just have to compare.
    NSDate *date = moment.timestamp;
    if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:0]).timestamp] > ONE_DAY) {
        // So actually should replace everything after with this too?... Sure, it's the most recent, why wouldn't you?
        for (int i = 0; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return oneDay;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:1]).timestamp] > FOUR_HOUR) {
        for (int i = 1; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return fourHours;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:2]).timestamp] > ONE_HOUR) {
        for (int i = 2; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return oneHour;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:3]).timestamp] > THIRTY_MIN) {
        for (int i = 3; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return thirtyMinutes;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:4]).timestamp] > FIVE_MIN) {
        for (int i = 4; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return fiveMinutes;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:5]).timestamp] > ONE_MIN) {
        for (int i = 5; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return oneMinute;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:6]).timestamp] > THIRTY_SECONDS) {
        for (int i = 6; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return thirtySeconds;
    } else if ([date timeIntervalSinceDate:((Moment *)[activeApplicationArray objectAtIndex:7]).timestamp] > FIVE_SECONDS) {
        for (int i = 7; i < COUNT_TIMEGAP_ENUM; i++) {
            [activeApplicationArray replaceObjectAtIndex:i withObject:moment];
        }
        return fiveSeconds;
    } else {
        [activeApplicationArray replaceObjectAtIndex:8 withObject:moment];
        return oneSecond;
    }
}

#pragma mark moment block stuff

- (void)updateMomentBlock:(Moment *)moment {
    if (!_momentBlock) {
        [self startNewMomentBlock:moment];
        return;
    }
    
    // So gets here, means momentBlock exists. So let's see if it's the same one.
    if ([_momentBlock.activeApplication isEqualToString:moment.activeApplication]) {
        // Going to go with title, though maybe should be URL?
        if (moment.url) {
            if ([_momentBlock.url isEqualToString:moment.url]) {
                // So here same application and same title, seems legit.
                // Title has a bug in gchat where the title flashes/changes. But url you sometimes get different url for the same page. hmm...
                // Lets go with url, gchat flash I think more likely than them visiting same page twice in row. for now.
                // TODO: look at the url/title thing eventually. 
                [self insertMomentIntoBlock:moment];
            } else {
                // Here not same title, so start new moment block.
                [self startNewMomentBlock:moment];
            }
        } else {
            // No title, and same application, so can continue block.
            [self insertMomentIntoBlock:moment];
        }
        return;
    } else {
        // So here, not the same app, so start a new block.
        [self startNewMomentBlock:moment];
        return;
    }
}

- (void)insertMomentIntoBlock:(Moment *)moment {
    _momentBlock.activeApplication = moment.activeApplication;
    _momentBlock.endTime = moment.timestamp;
    [_momentBlock addMomentObject:moment];
    moment.momentBlock = _momentBlock;
}


- (void)startNewMomentBlock:(Moment *)moment {
    _momentBlock = [NSEntityDescription insertNewObjectForEntityForName:@"MomentBlock" inManagedObjectContext:[DataManager sharedInstance].recordingMOC];
//    NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
//    NSPersistentStore *mainStore = [psc persistentStoreForURL:[NSURL SVPersistentStoreUrl]];
//    [[DataManager sharedInstance].recordingMOC assignObject:_momentBlock toPersistentStore:mainStore];
    _momentBlock.activeApplication = moment.activeApplication;
    _momentBlock.startTime = moment.timestamp;
    _momentBlock.endTime = moment.timestamp;
    // TODO: check if this is too clever due to async
    if (moment.url)
        _momentBlock.url = moment.url;
    if (moment.title)
        _momentBlock.title = moment.title;
    [_momentBlock addMomentObject:moment];
    moment.momentBlock = _momentBlock;
}


- (void)deleteMoments:(NSSet *)momentsToDelete
{
    // Another piece of state to reset
    self.momentBlock = nil;
    
    // Reset the screen recorder state to avoid keeping references to deleted moments
    // TODO: only reset if actually necessary
    ScreenRecorder *sr = [[AppDelegate sharedInstance] screenRecorder];
    dispatch_sync(sr.recordingQueue, ^{
        [sr resetState];
    
        [[momentsToDelete.anyObject managedObjectContext] performBlockAndWait:^{
            for (Moment *moment in momentsToDelete) {
                NSSet *dependants = [moment.dependantMoments copy];
                for (Moment *dependant in dependants) {
                    if(![momentsToDelete containsObject:dependant]){
                        [dependant makeIndependent];
                    }
                }
                [moment deleteMoment];
            }
            [[momentsToDelete.anyObject managedObjectContext] save:nil];
            [self save];
        }];
    });
}

@end
