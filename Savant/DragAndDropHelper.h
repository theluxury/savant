//
//  DragAndDropHelper.h
//  Savant
//
//  Created by Mark Wang on 10/23/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AVFoundation/AVFoundation.h>

@interface DragAndDropHelper : NSObject

+(void)writeImageToPanel:indexSet array:(NSArray *)array;
+ (void)writeVideoToPanel:(AVAssetWriter *)videoWriter indexSet:(NSIndexSet *)indexSet array:(NSArray *)array progressIndicator:(NSProgressIndicator *)progressIndicator exportLabel:(NSTextField *)exportLabel;


@end
