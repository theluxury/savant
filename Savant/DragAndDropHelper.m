//
//  DragAndDropHelper.m
//  Savant
//
//  Created by Mark Wang on 10/23/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "DragAndDropHelper.h"
#import "Moment.h"
#import "GAHelper.h"


@implementation DragAndDropHelper

+(void)writeImageToPanel:(id)indexSet array:(NSArray *)array {
    __block NSURL *url;
    // create the save panel
    NSSavePanel *panel = [NSSavePanel savePanel];
    
    // set a new file name
    [panel setNameFieldStringValue:@"Savant-image.png"];
    
    // display the panel
    [panel beginWithCompletionHandler:^(NSInteger result) {
        
        if (result == NSFileHandlingPanelOKButton) {
            
            url = [panel URL];
            __block Moment *moment;
            [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                moment = [array objectAtIndex:idx];
                *stop = YES;
            }];
            NSImage *image = moment.screenImage;
            NSString *destPath = [url path];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self saveImage:image atPath:destPath];
            });
        }
    }];
    [[GAHelper sharedInstance] exportImage];
}

+ (void)writeVideoToPanel:(AVAssetWriter *)videoWriter indexSet:(NSIndexSet *)indexSet array:(NSArray *)array progressIndicator:(NSProgressIndicator *)progressIndicator exportLabel:(NSTextField *)exportLabel {
    __block NSMutableArray *mutArray = [NSMutableArray array];
    
    __block NSURL *url;
    // create the save panel
    NSSavePanel *panel = [NSSavePanel savePanel];
    
    // set a new file name
    [panel setNameFieldStringValue:@"Savant-movie.mov"];
    
    // display the panel
    [panel beginWithCompletionHandler:^(NSInteger result) {
        
        if (result == NSFileHandlingPanelOKButton) {
            
            url = [panel URL];
            __block Moment *moment;
            [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                moment = [array objectAtIndex:idx];
                // TODO: Maybe make this more deterministic? probably works.
                [mutArray insertObject:moment.screenImage atIndex:0];
            }];
            NSString *destPath = [url path];
            NSLog(@"mut array count is %lu", [mutArray count]);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self writeImagesAsMovie:videoWriter indexSet:indexSet array:[mutArray copy] toPath:destPath progressIndicator:progressIndicator exportLabel:exportLabel];
            });
            
        }
    }];
    [[GAHelper sharedInstance] exportVideo];
}

+ (void)saveImage:(NSImage *)image atPath:(NSString *)path {
    
    CGImageRef cgRef = [image CGImageForProposedRect:NULL
                                             context:nil
                                               hints:nil];
    NSBitmapImageRep *newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgRef];
    [newRep setSize:[image size]];   // if you want the same resolution
    NSData *pngData = [newRep representationUsingType:NSPNGFileType properties:nil];
    [pngData writeToFile:path atomically:YES];
}

+ (void) writeImagesAsMovie:(AVAssetWriter *)videoWriter indexSet:(NSIndexSet *)indexSet array:(NSArray *)array toPath:(NSString*)path progressIndicator:(NSProgressIndicator *)progressIndicator exportLabel:(NSTextField *)exportLabel {
    // If video exists at path, delete it.
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
        [[NSFileManager defaultManager] removeItemAtPath: path error: nil];
    
    
    NSImage *first = [array objectAtIndex:0];
    
    
    CGSize frameSize = first.size;
    
    NSError *error = nil;
    videoWriter = [videoWriter initWithURL:
                    [NSURL fileURLWithPath:[path stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] fileType:AVFileTypeMPEG4
                                                error:&error];
    
    if(error) {
        NSAlert *alert = [NSAlert alertWithMessageText:@"Error exporting video." defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@"There was an error trying to export your video. Please try again."];
        [alert runModal];
        NSLog(@"error creating AssetWriter: %@",[error description]);
    }
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:frameSize.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:frameSize.height], AVVideoHeightKey,
                                   nil];
    
    
    
    AVAssetWriterInput* writerInput = [AVAssetWriterInput
                                       assetWriterInputWithMediaType:AVMediaTypeVideo
                                       outputSettings:videoSettings];
    
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    [attributes setObject:[NSNumber numberWithUnsignedInt:kCVPixelFormatType_32ARGB] forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
    [attributes setObject:[NSNumber numberWithUnsignedInt:frameSize.width] forKey:(NSString*)kCVPixelBufferWidthKey];
    [attributes setObject:[NSNumber numberWithUnsignedInt:frameSize.height] forKey:(NSString*)kCVPixelBufferHeightKey];
    
    AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor
                                                     assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput
                                                     sourcePixelBufferAttributes:attributes];
    
    [videoWriter addInput:writerInput];
    
    // fixes all errors
    writerInput.expectsMediaDataInRealTime = YES;
    
    //Start a session:
    BOOL start = [videoWriter startWriting];
    NSLog(@"Session started? %d", start);
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    CVPixelBufferRef buffer = NULL;
    buffer = [self pixelBufferFromNSImage:first];
    BOOL result = [adaptor appendPixelBuffer:buffer withPresentationTime:kCMTimeZero];
    
    if (result == NO) { //failes on 3GS, but works on iphone 4
        NSAlert *alert = [NSAlert alertWithMessageText:@"Error exporting video." defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@"There was an error trying to export your video. Please try again."];
        [alert runModal];
        NSLog(@"failed to append buffer");
    }
    
    if(buffer)
        CVBufferRelease(buffer);
    
    [NSThread sleepForTimeInterval:0.05];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [progressIndicator setUsesThreadedAnimation:YES];
        [progressIndicator startAnimation:nil];
        [progressIndicator setHidden:NO];
        [exportLabel setHidden:NO];
    });
    
    int fps = 1;
    
    int i = 0;
    for (NSImage *image in array)
    {
        // skip the first one since that's the first image already.
        if ([image isEqualTo:[array objectAtIndex:0]]) {
            continue;
        }
        
        if (adaptor.assetWriterInput.readyForMoreMediaData)
        {
            
            CMTime frameTime = CMTimeMake(1, fps);
            // i - 1 since the 0'th one is already counted for.
            CMTime lastTime=CMTimeMake(i, fps);
            CMTime presentTime=CMTimeAdd(lastTime, frameTime);
            
            buffer = [self pixelBufferFromNSImage:image];
            BOOL result = [adaptor appendPixelBuffer:buffer withPresentationTime:presentTime];
            i++;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [progressIndicator incrementBy:100.0/([array count] - 1)];
            });
            
            if (result == NO) //failes on 3GS, but works on iphone 4
            {
                NSAlert *alert = [NSAlert alertWithMessageText:@"Error exporting video." defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@"There was an error trying to export your video. Please try again."];
                [alert runModal];
                NSLog(@"failed to append buffer");
                NSLog(@"The error is %@", [videoWriter error]);
            }
            if(buffer)
                CVBufferRelease(buffer);
            [NSThread sleepForTimeInterval:0.05];
        }
        else
        {
            NSLog(@"error");
            NSAlert *alert = [NSAlert alertWithMessageText:@"Error exporting video." defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@"There was an error trying to export your video. Please try again."];
            [alert runModal];
            i--;
        }
        [NSThread sleepForTimeInterval:0.02];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [progressIndicator stopAnimation:nil];
        [progressIndicator setHidden:YES];
        [progressIndicator setDoubleValue:0.0];
        [exportLabel setHidden:YES];
    });
    
    //Finish the session:
    [writerInput markAsFinished];
    [videoWriter finishWritingWithCompletionHandler:^(void){
        NSLog(@"completed");
    }];
    CVPixelBufferPoolRelease(adaptor.pixelBufferPool);
}

+ (CVPixelBufferRef) pixelBufferFromNSImage: (NSImage *) image
{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    // TODO: bridge might be dangerous?
    CVPixelBufferCreate(kCFAllocatorDefault, image.size.width,
                        image.size.height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options,
                        &pxbuffer);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, image.size.width,
                                                 image.size.height, 8, 4*image.size.width, rgbColorSpace,
                                                 (CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
    
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    
    CGContextDrawImage(context, CGRectMake(0, 0, image.size.width,
                                           image.size.height), [self CGImage:image]);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}

+ (CGImageRef)CGImage:(NSImage *)nsimage{
    NSData *imageData = nsimage.TIFFRepresentation;
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)imageData, NULL);
    CGImageRef maskRef =  CGImageSourceCreateImageAtIndex(source, 0, NULL);
    return maskRef;
}

@end
