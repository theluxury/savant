//
//  FirefoxHelper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-18.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "FirefoxHelper.h"

@implementation FirefoxHelper

+ (NSString *)getURLForWindow:(AXWrapper *)window
{
    NSString *urlFieldValue = [[window childAtPath:[self urlFieldPath]] value];
    return [urlFieldValue hasPrefix:@"http"] ? urlFieldValue : [@"http://" stringByAppendingString:urlFieldValue];
}
+ (NSString *)getTitleForWindow:(AXWrapper *)window
{
    return window.title;
}

+ (NSArray *)urlFieldPath
{
    return @[@"AXGroup",@"AXToolbar::1",@"AXTextField"];
}

@end
