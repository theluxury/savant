//
//  SavantWindow.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-03.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FocusableWindow : NSWindow

@end
