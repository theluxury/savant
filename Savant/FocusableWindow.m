//
//  SavantWindow.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-03.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "FocusableWindow.h"

@implementation FocusableWindow

- (BOOL)canBecomeKeyWindow
{
    return YES;
}
- (BOOL)canBecomeMainWindow
{
    return YES;
}

@end
