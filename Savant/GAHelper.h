//
//  GAHelper.h
//  Savant
//
//  Created by Mark Wang on 9/26/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GAHelper : NSObject <NSURLConnectionDataDelegate>

+(GAHelper *)sharedInstance;

#pragma mark sessions
-(void)startBrowse;
-(void)endBrowse;
-(void)startRecording;
-(void)endRecording;

#pragma mark event browse
- (void)toggleZoom;
- (void)search;
- (void)toggleBlue;
- (void)toggleRed;
- (void)toggleGreen;
- (void)toggleYellow;
- (void)selectMoment;
- (void)selectImage;
- (void)scrollDown;
- (void)scrollUp;

#pragma mark event other
-(void)exportImage;
-(void)exportVideo;
-(void)openApplication;
-(void)openUrl;

#pragma mark event video
- (void)goBack;
- (void)goForward;
- (void)play;
- (void)pause;
- (void)selectTextNode;


#pragma mark screenview

- (void)viewBrowsingWindow;
- (void)viewVideoWindow;

@end
