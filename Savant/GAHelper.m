//
//  GAHelper.m
//  Savant
//
//  Created by Mark Wang on 9/26/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "GAHelper.h"

@interface GAHelper ()
@property NSString *cid;
@end

@implementation GAHelper

static NSString *DEFAULTS_CID = @"cid";

// First one is for dev.
#ifdef DEBUG
static const NSString *TID = @"UA-55206704-2";
static const NSString *RECORDING_TID = @"UA-55206704-2";
#else
static const NSString *TID = @"UA-55206704-1";
static const NSString *RECORDING_TID = @"UA-55206704-3";
#endif


// types can't be renamed.
static const NSString *TYPE_EVENT = @"event";
static const NSString *EVENT_CATEGORY_RECORDING = @"recording";
static const NSString *SESSION_CONTROL_START = @"start";
static const NSString *SESSION_CONTROL_END = @"end";

static const NSString *EVENT_CATEGORY_BROWSE = @"browse";
static const NSString *EVENT_ACTION_START = @"start";
static const NSString *EVENT_ACTION_END = @"end";

static const NSString *SCREENVIEW_CD_BROWSINGWINDOW = @"browsingWindow";

static const NSString *EVENT_ACTION_TOGGLE_ZOOM = @"toggleZoom";
static const NSString *EVENT_ACTION_SEARCH = @"search";
static const NSString *EVENT_ACTION_TOGGLE1 = @"toggle1";
static const NSString *EVENT_ACTION_TOGGLE2 = @"toggle2";
static const NSString *EVENT_ACTION_TOGGLE3 = @"toggle3";
static const NSString *EVENT_ACTION_TOGGLE4 = @"toggle4";
static const NSString *EVENT_ACTION_SELECT_MOMENT = @"selectMoment";
static const NSString *EVENT_ACTION_SELECT_IMAGE = @"selectImage";
static const NSString *EVENT_ACTION_SCROLL_UP = @"scrollUp";
static const NSString *EVENT_ACTION_SCROLL_DOWN = @"scrollDown";

static const NSString *EVENT_CATEGORY_OTHER = @"other";
static const NSString *EVENT_ACTION_EXPORT_IMAGE = @"exportImage";
static const NSString *EVENT_ACTION_EXPORT_VIDEO = @"exportVideo";
static const NSString *EVENT_ACTION_OPEN_APPLICATION = @"openApplication";
static const NSString *EVENT_ACTION_OPEN_URL = @"openUrl";

static const NSString *EVENT_CATEGORY_VIDEO = @"video";

static const NSString *TYPE_SCREENVIEW = @"screenview";

static const NSString *SCREENVIEW_CD_VIDEOWINDOW = @"videoWindow";

// Currently only covers button presses.
static const NSString *EVENT_ACTION_GOBACK = @"goBack";
static const NSString *EVENT_ACTION_GOFORWARD = @"goForward";
static const NSString *EVENT_ACTION_PLAYBACK_PLAY = @"play";
static const NSString *EVENT_ACTION_PLAYBACK_PAUSE = @"pause";
static const NSString *EVENT_ACTION_SELECT_TEXTNODE = @"selectTextNode";

static const NSString *EVENT_CATEGORY_PLAYBACK = @"playback";


+ (GAHelper *)sharedInstance {
    static GAHelper *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init
{
    if(self = [super init]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults objectForKey:DEFAULTS_CID]) {
            [defaults setObject:[[NSUUID UUID] UUIDString] forKey:DEFAULTS_CID];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        _cid = [defaults objectForKey:DEFAULTS_CID];
    }
    return self;
}

- (void) connectToGA:(NSString *)bodyData {
    // TODO: Actually make it change the version number dynamically
    
    // Create the request.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google-analytics.com/collect"]  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    // This is how we set header fields
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // Convert your data and set your request's HTTPBody property
    NSData *requestBodyData = [bodyData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!conn) {
        NSLog(@"no connection to GA :(");
    }

    
}

#pragma mark session


- (void)startBrowse {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@&sc=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_START, SESSION_CONTROL_START];
    [self connectToGA:stringData];
}

- (void)endBrowse {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@&sc=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_END, SESSION_CONTROL_END];
    [self connectToGA:stringData];
}

- (void)startRecording {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@&sc=%@", RECORDING_TID, _cid, TYPE_EVENT, EVENT_CATEGORY_RECORDING, EVENT_ACTION_START, SESSION_CONTROL_START];
    [self connectToGA:stringData];
}

- (void)endRecording {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@&sc=%@", RECORDING_TID, _cid, TYPE_EVENT, EVENT_CATEGORY_RECORDING, EVENT_ACTION_END, SESSION_CONTROL_END];
    [self connectToGA:stringData];
}


#pragma mark event browse

- (void)toggleZoom {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_TOGGLE_ZOOM];
    [self connectToGA:stringData];
}

- (void)search {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_SEARCH];
    [self connectToGA:stringData];
}

- (void)toggleBlue {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_TOGGLE1];
    [self connectToGA:stringData];
}

- (void)toggleRed {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_TOGGLE2];
    [self connectToGA:stringData];
}

- (void)toggleGreen {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_TOGGLE3];
    [self connectToGA:stringData];
}

- (void)toggleYellow {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_TOGGLE4];
    [self connectToGA:stringData];
}

- (void)selectMoment {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_SELECT_MOMENT];
    [self connectToGA:stringData];
}

- (void)selectImage {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_SELECT_IMAGE];
    [self connectToGA:stringData];
}

- (void)scrollDown {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_SCROLL_DOWN];
    [self connectToGA:stringData];
}

- (void)scrollUp {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_BROWSE, EVENT_ACTION_SCROLL_UP];
    [self connectToGA:stringData];
}

- (void)exportImage {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_OTHER, EVENT_ACTION_EXPORT_IMAGE];
    [self connectToGA:stringData];
}

- (void)exportVideo {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_OTHER, EVENT_ACTION_EXPORT_VIDEO];
    [self connectToGA:stringData];
}

- (void)openApplication {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_OTHER, EVENT_ACTION_OPEN_APPLICATION];
    [self connectToGA:stringData];
}

- (void)openUrl {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_OTHER, EVENT_ACTION_OPEN_URL];
    [self connectToGA:stringData];
}

#pragma mark event video

- (void)goBack {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_VIDEO, EVENT_ACTION_GOBACK];
    [self connectToGA:stringData];
}

- (void)goForward {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_VIDEO, EVENT_ACTION_GOFORWARD];
    [self connectToGA:stringData];
}

- (void)play {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_VIDEO, EVENT_ACTION_PLAYBACK_PLAY];
    [self connectToGA:stringData];
}

- (void)pause {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_VIDEO, EVENT_ACTION_PLAYBACK_PAUSE];
    [self connectToGA:stringData];
}

- (void)selectTextNode {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&ec=%@&ea=%@", TID, _cid, TYPE_EVENT, EVENT_CATEGORY_VIDEO, EVENT_ACTION_SELECT_TEXTNODE];
    [self connectToGA:stringData];
}


#pragma mark screenview

// Not sure these do anything.
- (void)viewBrowsingWindow {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&cd=%@", TID, _cid, TYPE_SCREENVIEW, SCREENVIEW_CD_BROWSINGWINDOW];
    [self connectToGA:stringData];
}

- (void)viewVideoWindow {
    NSString *stringData = [NSString stringWithFormat:@"v=1&tid=%@&cid=%@&t=%@&cd=%@", TID, _cid, TYPE_SCREENVIEW, SCREENVIEW_CD_VIDEOWINDOW];
    [self connectToGA:stringData];
}



#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    NSLog(@"connection error %@", error);
}

@end
