//
//  GeneralPlaybackController.h
//  Savant
//
//  Created by Mark Wang on 11/18/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Moment.h"

@interface GeneralPlaybackController : NSWindowController

@property (nonatomic, strong) Moment *currShowingMoment;
@property (nonatomic, strong) NSMutableArray *momentsArray;
@property (nonatomic, strong) NSPersistentStore *currShowingStore;

- (BOOL)gotMoreStuff:(NSArray *)array moment:(Moment *)moment;
- (void)sortMomentsArray;
- (void)setupTrackingAreas;
- (void)actualSetupTrackingArea;

- (void)skipBack;
- (void)skipForward;
- (void)goBackOneFrame;
- (void)goForwardOneFrame;
- (NSPredicate *)getCombinedPredicate:(NSPredicate *)predicate;
- (void)refreshImage;

@end
