//
//  GeneralPlaybackController.m
//  Savant
//
//  Created by Mark Wang on 11/18/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "GeneralPlaybackController.h"
#import "VideoWindow.h"
#import "PredicateHelper.h"

@interface GeneralPlaybackController ()

@end

@implementation GeneralPlaybackController
#define KEY_LEFT_ARROW 123
#define KEY_RIGHT_ARROW 124

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (BOOL)gotMoreStuff:(NSArray *)array moment:(Moment *)moment {
    
    if (!array || [array count] == 0)
        return NO;
    
    [_momentsArray removeAllObjects];
    [_momentsArray addObjectsFromArray:array];
    if (moment)
        [_momentsArray addObject:_currShowingMoment];
    [self sortMomentsArray];
    return YES;
}

- (void)sortMomentsArray {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp"
                                                 ascending:YES];
    [_momentsArray sortUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
}

- (void)setupTrackingAreas {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    // Remove Tracking areas
    for (NSTrackingArea *ta in [[self.window contentView] trackingAreas]) {
        [[self.window contentView] removeTrackingArea:ta];
    }
    
    // So here order matters, because you want to set the picture before you show the player, or else there's lag.
    [self performSelector:@selector(actualSetupTrackingArea) withObject:self afterDelay:0.1];
}

- (void)actualSetupTrackingArea {
    @synchronized(self.currShowingMoment.textState.textNodes) {
        for (TextNode *element in self.currShowingMoment.textState.textNodes) {
            CGRect videoRect = [(VideoWindow *)self.window getVideoRectFromAXElement:element];
            // So want to add some userData so that knows which element it is. hmmm
            NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:NSStringFromRect(videoRect), @"rect", element.text, @"text", nil];
            NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:videoRect options:NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp | NSTrackingAssumeInside  owner:self.window userInfo:dictionary];
            [[(VideoWindow *)self.window contentView] addTrackingArea:trackingArea];
        }
    }
}

- (void)skipBack {
    NSInteger index = [self.momentsArray indexOfObject:self.currShowingMoment];
    Moment *earliestMoment = self.momentsArray[0];
    
    NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp < %@", earliestMoment.timestamp];
    NSPredicate *combinedPredicate = [self getCombinedPredicate:timePredicate];
    
    NSArray *fetchedArray = [PredicateHelper fetchScreenshots:combinedPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:MOST_RECENT];
    
    if (![self gotMoreStuff:fetchedArray moment:nil])
        return;
    
    if (index < [self.momentsArray count] )
        self.currShowingMoment = self.momentsArray[index];
    else
        self.currShowingMoment = self.momentsArray[0];
    
    [self refreshImage];
}

- (void)skipForward {
    NSInteger index = [self.momentsArray indexOfObject:self.currShowingMoment];
    Moment *latestMoment = self.momentsArray[[self.momentsArray count] - 1];
    NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp > %@", latestMoment.timestamp];
    NSPredicate *combinedPredicate = [self getCombinedPredicate:timePredicate];
    
    NSArray *fetchedArray = [PredicateHelper fetchScreenshots:combinedPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:EARLIEST];
    
    if (![self gotMoreStuff:fetchedArray moment:nil])
        return;
    
    if (index < [self.momentsArray count] )
        self.currShowingMoment = self.momentsArray[index];
    else
        self.currShowingMoment = self.momentsArray[[self.momentsArray count] - 1];
    
    [self refreshImage];
}

- (void)goBackOneFrame {
    // Get index of curr showing moment, then set the one less than that as current and show.
    NSInteger currRow = [self.momentsArray indexOfObject:self.currShowingMoment];
    if (currRow == NSNotFound)
        return;
    
    if (currRow == 0) {
        NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp < %@", self.currShowingMoment.timestamp];
        NSPredicate *combinedPredicate = [self getCombinedPredicate:timePredicate];
        NSArray *fetchedArray = [PredicateHelper fetchScreenshots:combinedPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:MOST_RECENT];
        
        if (![self gotMoreStuff:fetchedArray moment:self.currShowingMoment]) {
            return;
        }
        
        currRow = [self.momentsArray indexOfObject:self.currShowingMoment];
    }
    
    self.currShowingMoment = self.momentsArray[currRow - 1];
    [self refreshImage];
}

- (void)goForwardOneFrame {
    NSInteger currRow = [self.momentsArray indexOfObject:self.currShowingMoment];
    if (currRow == NSNotFound)
        return;
    
    if (currRow == [self.momentsArray count] - 1) {
        NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp > %@", self.currShowingMoment.timestamp];
        NSPredicate *combinedPredicate = [self getCombinedPredicate:timePredicate];
        NSArray *fetchedArray = [PredicateHelper fetchScreenshots:combinedPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:EARLIEST];
        
        if (![self gotMoreStuff:fetchedArray moment:self.currShowingMoment])
            return;
        
        currRow = [self.momentsArray indexOfObject:self.currShowingMoment];
    }
    
    self.currShowingMoment = self.momentsArray[currRow + 1];
    [self refreshImage];
}

- (NSPredicate *)getCombinedPredicate:(NSPredicate *)predicate {
    return predicate;
}

- (void)refreshImage {
    
}

// Need this so that it doesn't make a sound when you use it.
- (void)keyDown:(NSEvent *)theEvent {
    NSLog(@"key down");
    switch (theEvent.keyCode) {
        case KEY_LEFT_ARROW:
            [self goBackOneFrame];
            break;
            
        case KEY_RIGHT_ARROW:
            [self goForwardOneFrame];
            break;
            
        default:
            break;
    }
}


@end
