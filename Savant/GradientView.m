//
//  GradientView.m
//  Savant
//
//  Created by Mark Wang on 8/12/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "GradientView.h"

@implementation GradientView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSColor *lighterGunMetal = [NSColor colorWithRed:76 / 255.0 green:76 / 255.0 blue:76 / 255.0 alpha:1.0];
    NSColor *gunMetal = [NSColor colorWithRed:51 / 255.0 green:51 / 255.0 blue:51 / 255.0 alpha:1.0];
    [super drawRect:dirtyRect];
    NSGradient *gradient = [[NSGradient alloc] initWithStartingColor:gunMetal endingColor:lighterGunMetal];
    NSRect windowFrame = [self frame];
    [gradient drawInRect:windowFrame angle:225];
}

@end
