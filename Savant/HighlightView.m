//
//  HighlightView.m
//  Savant
//
//  Created by Mark Wang on 8/31/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "HighlightView.h"

@implementation HighlightView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];

    NSRect newRect = NSMakeRect(dirtyRect.origin.x, dirtyRect.origin.y, dirtyRect.size.width, dirtyRect.size.height);
    
    NSBezierPath *textViewSurround = [NSBezierPath bezierPathWithRoundedRect:newRect xRadius:2 yRadius:2];
    [textViewSurround setLineWidth:1.5];
    [[NSColor blueColor] set];
    [textViewSurround stroke];
    [self setAlphaValue:0.5];
}


@end
