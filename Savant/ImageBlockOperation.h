//
//  ImageBlockOperation.h
//  Savant
//
//  Created by Paul Musgrave on 2014-09-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ImagePipelineOperation.h"


typedef void (^ImageBlock)(NSImage *);


@interface ImageBlockOperation : ImagePipelineOperation

@property (nonatomic, strong) ImageBlock imageBlock;

+ (instancetype)withImageBlock:(ImageBlock)block;

@end
