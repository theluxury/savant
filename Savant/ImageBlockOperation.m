//
//  ImageBlockOperation.m
//  Savant
//
//  Created by Paul Musgrave on 2014-09-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ImageBlockOperation.h"

@implementation ImageBlockOperation

+ (instancetype)withImageBlock:(ImageBlock)imageBlock
{
    ImageBlockOperation *op = [[self alloc] init];
    op.imageBlock = imageBlock;
    op.queuePriority = NSOperationQueuePriorityHigh;
    return op;
}

- (void)main
{
    self.imageBlock(self.inImage);
    [self cleanup];
}

@end
