//
//  ImageFetchOperation.h
//  Savant
//
//  Created by Paul Musgrave on 2014-09-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ImagePipelineOperation.h"


@class Moment;


@interface ImageFetchOperation : ImagePipelineOperation

@property (nonatomic, strong) NSImage *fetchedImage;

- (instancetype)initWithMoment:(Moment *)moment;

- (void)addDependantImageOperation:(ImagePipelineOperation *)op;
- (void)removeDependantImageOperation:(ImagePipelineOperation *)op;

+ (NSURL *)convertUrl:(NSURL *)imageUrl;

@end
