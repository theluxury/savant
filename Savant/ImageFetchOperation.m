//
//  ImageFetchOperation.m
//  Savant
//
//  Created by Paul Musgrave on 2014-09-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ImageFetchOperation.h"
#import "Moment.h"
#import "NSImage+Diff.h"
#import "AppDelegate.h"
#import "SVBrowsingWindowController.h"

@interface ImageFetchOperation ()

@property (nonatomic, strong) MomentID *momentID;

@property (nonatomic) BOOL isKeyFrame;
@property (nonatomic, strong) NSURL *srcURL;
@property (nonatomic) NSRect diffBox;

@property (nonatomic, strong) NSMutableSet *dependentImageOperations;

@end


@implementation ImageFetchOperation

- (instancetype)initWithMoment:(Moment *)moment
{
    if(self = [super init]){
        _momentID = [moment objectID];
        // So there is an issue with the URL being incorrect if the moments are from DB from someone else's computer.
        
        if (moment.imageURL){
            _isKeyFrame = YES;
            _srcURL = [ImageFetchOperation convertUrl:moment.imageURL];;
        } else {
            _srcURL = [ImageFetchOperation convertUrl:moment.diffURL];;
            _diffBox = moment.diffBox;
        }
        _dependentImageOperations = [NSMutableSet set];
    }
    return self;
}

+ (NSURL *)convertUrl:(NSURL *)imageUrl {
    // What's a quick hack? take the image path afer the final "/", then add that to the location of the db/recording/name. That's fine for now...
    // This sometimes throws nul exceptions. Think it's if dif in nul?
    NSArray *stringArray = [[imageUrl absoluteString] componentsSeparatedByString:@"/"];
    NSString *imageName = [stringArray lastObject];
    NSString *appendedImageName = [@"/recording/" stringByAppendingString:imageName];
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    SVBrowsingWindowController *svb = d.browsingController;
    NSDictionary *currShowingDict = svb.currShowingDict;
    NSString *directoryUrlString = [currShowingDict objectForKey:@"url"];
    NSString *fixedDirectoryString = [directoryUrlString stringByReplacingOccurrencesOfString:@"/SavantData.storedata" withString:@""];
    NSString *finalString = [fixedDirectoryString stringByAppendingString:appendedImageName];
    return [NSURL URLWithString:finalString];
}

- (void)main
{
    if (self.isKeyFrame) {
        self.fetchedImage = [[NSImage alloc] initByReferencingURL:self.srcURL];
    } else {
        self.fetchedImage = [NSImage applyDiff: [[NSImage alloc] initByReferencingURL:self.srcURL]
                                       toImage: self.inImage
                                       diffBox: NSRectFromCGRect(self.diffBox)];
    }

    for (ImagePipelineOperation *op in self.dependentImageOperations){
        op.inImage = self.fetchedImage;
    }
    [self cleanup];
}

- (void)cleanup
{
    [super cleanup];
    self.dependentImageOperations = nil;
}

- (void)addDependantImageOperation:(ImagePipelineOperation *)op
{
    [self.dependentImageOperations addObject:op];
}
- (void)removeDependantImageOperation:(ImagePipelineOperation *)op
{
    [self.dependentImageOperations removeObject:op];
    if([self.dependentImageOperations count] == 0){
        [self cancel];
    }
}

@end
