//
//  DependencyCountingOperation.h
//  Savant
//
//  Created by Paul Musgrave on 2014-09-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ImageFetchOperation;


@interface ImagePipelineOperation : NSOperation

@property (nonatomic, strong) NSImage *inImage;

- (void)addImageDependency:(ImageFetchOperation *)op;

- (void)cleanup;

@end
