//
//  DependencyCountingOperation.m
//  Savant
//
//  Created by Paul Musgrave on 2014-09-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ImagePipelineOperation.h"
#import "ImageFetchOperation.h"

@interface ImagePipelineOperation ()
@property (nonatomic, weak) ImageFetchOperation *imageDependency;
@end

@implementation ImagePipelineOperation

- (void)addImageDependency:(ImageFetchOperation *)op
{
    self.imageDependency = op;
    [op addDependantImageOperation:self];
    [self addDependency:op];
}

- (void)cancel
{
    [super cancel];
    [self cleanup];
    [self.imageDependency removeDependantImageOperation:self];
}

// This is called by cancel, and should be called at the end of main in all subclasses
- (void)cleanup
{
    // We don't want to retain these, and it does by default
    for (NSOperation *op in self.dependencies) {
        [self removeDependency:op];
    }
}

@end
