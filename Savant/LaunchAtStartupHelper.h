//
//  LaunchAtStartupController.h
//  Savant
//
//  Created by Mark Wang on 9/6/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LaunchAtStartupHelper : NSObject

extern NSString *const firstRun;

+ (BOOL)isLaunchAtStartup;
+ (void)toggleLaunchAtStartup;


@end
