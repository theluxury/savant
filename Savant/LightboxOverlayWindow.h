//
//  LightboxOverlayWindow.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-19.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LightboxOverlayWindow : NSWindow

- (instancetype)initWithContentRect:(NSRect)contentRect;

@end
