//
//  LightboxOverlayWindow.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-19.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "LightboxOverlayWindow.h"
#import "AppDelegate.h"
#import "VideoWindowController.h"

@interface LightboxOverlayWindow()
@property (weak) VideoWindowController *videoController;
@end

@implementation LightboxOverlayWindow

- (instancetype)initWithContentRect:(NSRect)contentRect
{
    self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    if(self){
        AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
        _videoController = d.videoController;
        [self setAlphaValue:0.8];
        [self setBackgroundColor:[NSColor blackColor]];
        [self setLevel:NSStatusWindowLevel];
    }
    return self;
}

- (void)mouseDown:(NSEvent *)theEvent {
    // ## this is a bit lame
    [_videoController dismissPlayer];
}

@end
