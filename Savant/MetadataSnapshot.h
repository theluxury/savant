//
//  MetadataSnapshot.h
//  Savant
//
//  Created by Paul Musgrave on 2014-10-09.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextState.h"

// There's a delay between the screen capture and the callback with the buffer (usually ~2.3s)
// so we capture the metadata independently and sync them
@interface MetadataSnapshot : NSObject

@property (nonatomic) BOOL skipMoment;

@property (nonatomic, strong) TextState *textState;
@property (nonatomic, strong) NSString *activeApplication;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *activeAppRect;


+ (MetadataSnapshot *)takeSnapshotWithRefreshRect:(NSRect)rect;

@end
