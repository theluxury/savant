//
//  MetadataSnapshot.m
//  Savant
//
//  Created by Paul Musgrave on 2014-10-09.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "MetadataSnapshot.h"
#import "SVAccessibilityScraper.h"
#import <AppKit/NSWorkspace.h>
#import "AppDelegate.h"
#import "NSString+Contains.h"


@implementation MetadataSnapshot

// TODO: this is dumb now, clean it up
+ (MetadataSnapshot *)takeSnapshotWithRefreshRect:(NSRect)refreshRect
{
    MetadataSnapshot *snapshot = [[MetadataSnapshot alloc] init];
    
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    SVAccessibilityScraper *sc = [SVAccessibilityScraper sharedInstance];
    
    NSRunningApplication *activeApp = [[NSWorkspace sharedWorkspace] frontmostApplication];
    
    if(d.shouldRecordAccessibilityText){
        [sc refreshStateOfRect:refreshRect];
        snapshot.textState = [sc currentTextState];
    }
    
    snapshot.activeApplication = activeApp.localizedName;
    snapshot.url = [sc getUrl];
    snapshot.title = [sc getTitle];
    snapshot.activeAppRect = [sc getActiveApplicationRect];

    return snapshot;
}

@end
