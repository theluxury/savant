//
//  MiniWindowController.h
//  Savant
//
//  Created by Mark Wang on 11/10/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Moment.h"
#import "GeneralPlaybackController.h"

@interface MiniWindowController : GeneralPlaybackController <NSWindowDelegate>

@property (weak) IBOutlet NSImageView *imageView;

@property (weak) IBOutlet NSPopover *popover;
@property (strong) IBOutlet NSTextView *popoverText;

@property (weak) IBOutlet NSSlider *momentSlider;
@property (weak) IBOutlet NSPopUpButton *dbChooser;

- (IBAction)skipForward:(NSButton *)sender;
- (IBAction)skipBack:(NSButton *)sender;
- (IBAction)sliderMoved:(NSSlider *)sender;
- (IBAction)chooseDb:(NSPopUpButton *)sender;

- (void)toggleVisibility:(NSString *)string;

@end
