//
//  MiniWindowController.m
//  Savant
//
//  Created by Mark Wang on 11/10/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "MiniWindowController.h"
#import "Moment.h"
#import "AppDelegate.h"
#import "PredicateHelper.h"
#import "VideoWindow.h"
#import "SharedPredicateHelper.h"
#import <HockeySDK/HockeySDK.h>
#import "AlternateDatabaseHelper.h"
#import "NSString+TrimUtil.h"

@interface MiniWindowController ()

@property (nonatomic) BOOL isPaused;
@property (nonatomic) BOOL isShowing;
@property (nonatomic, strong) NSString *searchString;
@property (nonatomic, strong) NSDictionary *currShowingStoreDict;
// This is most likely terrible.
@property (nonatomic, strong) NSArray *currDbArray;
@end

@implementation MiniWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    [(VideoWindow *)self.window setController:self];
    [self.window setStyleMask:[self.window styleMask] & ~NSResizableWindowMask];
    _popover.animates = false;
}

// Should probably have some array with the most recent moments of the particular application.

- (instancetype)init {
    if(self = [super initWithWindowNibName:@"MiniWindowController"]){
        self.momentsArray = [NSMutableArray array];
    }
    return self;
}

- (void)sliderMoved:(NSSlider *)sender {
    // Now should move the array
    self.currShowingMoment = self.momentsArray[[_momentSlider intValue]];
    [self refreshImage];
}

- (void)toggleVisibility:(NSString *)string {
    if (_isShowing) {
        #ifndef DEBUG
            BITSystemProfile *bsp = [BITSystemProfile sharedSystemProfile];
            [bsp stopUsage];
        #endif
        [self.window orderOut:self];
        _isShowing = NO;
        return;
    }
    
    #ifndef DEBUG
        BITSystemProfile *bsp = [BITSystemProfile sharedSystemProfile];
        [bsp startUsage];
    #endif
    
    // Following is the new hotness.
    // Okay, so have to first get an array of the earliest showing with the predicate.
    NSArray *storeDbDictionary = [NSArray array];
    
    // If the string is nil or empty, just get the most recent from every array
    if (!string || [[string trimmedString] length] == 0)
        storeDbDictionary = [AlternateDatabaseHelper fetchSortedFromEveryDb:nil];
    else
        storeDbDictionary = [AlternateDatabaseHelper fetchSortedFromEveryDb:string];
    // So this is already sorted. So, um, make an array with the names, and show that somewhere, and get the 50 from the first array, I guess.

    // so what do I want to do? So I guess for the earliest moment, get the ~50 from that store for that one? Sure...
    // Bleh, if you don't have a string, you probably want the most recent right? Hmmm... Eh, that would be confusing.
    // Just do earliest now for everytihng, and fix it later.
    // TODO: Figure out something to do if you can't find anything from the string. 
    // The following is probably fine, if you just restrict it to the earliest db
    if ([storeDbDictionary count]==0)
        return;
    
    _currDbArray = storeDbDictionary;
    [_dbChooser removeAllItems];
    for (NSDictionary *dict in storeDbDictionary) {
        [_dbChooser addItemWithTitle:[dict objectForKey:@"name"]];
    }
    
    _currShowingStoreDict = storeDbDictionary[0];
    self.currShowingStore = [_currShowingStoreDict objectForKey:@"store"];
    [self refreshView];
  
}


// So to grab the text, similar to the other window one, obviously.


- (void)windowDidResignMain:(NSNotification *)notification {
    [[AppDelegate sharedInstance].windowVisibilityItem setTitle:@"Find in Savant"];
    _isShowing = NO;
}

- (void)windowDidBecomeMain:(NSNotification *)notification {
    [[AppDelegate sharedInstance].windowVisibilityItem setTitle:@"Hide Text Replay"];
    _isShowing = YES;
}

- (void)refreshImage {
    [_momentSlider setMaxValue:[self.momentsArray count] - 1];
    [_momentSlider setIntegerValue:[self.momentsArray indexOfObject:self.currShowingMoment]];
    [_popover close];
    [_imageView setImage:self.currShowingMoment.screenImage];
    [(VideoWindow *)self.window removeAllSubview];
    [self setupTrackingAreas];
    [(VideoWindow *)self.window setupSearchTextNodes:self.currShowingMoment string:_searchString];
    
}

- (IBAction)skipBack:(NSButton *)sender {
    [self skipBack];
}

- (IBAction)skipForward:(NSButton *)sender {
    [self skipForward];
}

- (IBAction)chooseDb:(NSPopUpButton *)sender {
    NSString *selectedStore = [[sender selectedItem] title];
    // So what do we actually want it to do if we select a new DB? If we select the same one, I guess nothing. If we select a new one... the normal stuff, but with the new one I guess.
    
    for (NSDictionary *dict in _currDbArray) {
        if ([[dict objectForKey:@"name"] isEqualToString:selectedStore]) {
            if ([dict isEqualTo:_currShowingStoreDict])
                return;
            else {
                _currShowingStoreDict = dict;
                self.currShowingStore = [_currShowingStoreDict objectForKey:@"store"];
            }
        }
    }
    
    [self refreshView];
    
    
}

- (void) refreshView {
    [[[DataManager sharedInstance] imageCache] removeAllObjects];
    NSLog(@"removing all objects");
    [self.momentsArray removeAllObjects];
    
    [self.momentsArray addObject:[_currShowingStoreDict objectForKey:@"moment"]];
    
    // Then, get the 49 latest moments before that
    self.currShowingMoment = self.momentsArray[0];
    NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"timestamp < %@", self.currShowingMoment.timestamp];
    
    // Grab different amounts depending on if you're just opening or selecting with text.
    if (_searchString && ![[_searchString trimmedString]isEqualToString:@""]) {
        [self.momentsArray addObjectsFromArray:[PredicateHelper fetchScreenshots:timePredicate storeArray:@[[_currShowingStoreDict objectForKey:@"store"] ] withLimit:24 ascending:MOST_RECENT]];
        NSPredicate *otherTimePredicate = [NSPredicate predicateWithFormat:@"timestamp > %@", self.currShowingMoment.timestamp];
        [self.momentsArray addObjectsFromArray:[PredicateHelper fetchScreenshots:otherTimePredicate storeArray:@[[_currShowingStoreDict objectForKey:@"store"]] withLimit:24 ascending:EARLIEST]];
    }
    else
        [self.momentsArray addObjectsFromArray:[PredicateHelper fetchScreenshots:timePredicate storeArray:@[[_currShowingStoreDict objectForKey:@"store"]] withLimit:49 ascending:MOST_RECENT]];
    
    
    [self sortMomentsArray];
    [self refreshImage];
    
    [self showWindow:self];
    // So could bring it up so that... it's the same size as the other one. Kasey doesn't like it though; I'm indifferent.
    [NSApp activateIgnoringOtherApps:YES];
    [self.window makeKeyAndOrderFront:self];
    _isShowing = YES;
    
}

@end
