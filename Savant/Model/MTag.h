#import "_MTag.h"

@interface MTag : _MTag {}

+ (MTag *)findOrCreate:(NSString *)name context:(NSManagedObjectContext *)moc;
+ (MTag *)find:(NSString *)name context:(NSManagedObjectContext *)moc;


@end
