#import "MTag.h"


@interface MTag ()
@end


@implementation MTag

+ (MTag *)findOrCreate:(NSString *)name context:(NSManagedObjectContext *)moc
{
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[MTag entityName]];
    req.fetchLimit = 1;
    req.predicate = [NSPredicate predicateWithFormat:@"name ==[cd] %@", name];
    NSArray *res = [moc executeFetchRequest:req error:nil];
    if(res.count > 0){
        return res[0];
    } else {
        MTag *newTag = [MTag insertInManagedObjectContext:moc];
        newTag.name = name;
        return newTag;
    }
}

+ (MTag *)find:(NSString *)name context:(NSManagedObjectContext *)moc
{
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[MTag entityName]];
    req.fetchLimit = 1;
    req.predicate = [NSPredicate predicateWithFormat:@"name ==[cd] %@", name];
    NSArray *res = [moc executeFetchRequest:req error:nil];
    if(res.count > 0){
        return res[0];
    } else {
        return nil;
    }
}


@end
