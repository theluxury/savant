#import "_Moment.h"
#import <AppKit/NSColor.h>
#import "ImagePipelineOperation.h"
#import "MetadataSnapshot.h"

@interface Moment : _Moment {}

typedef enum { oneSecond = 5, fiveSeconds, thirtySeconds, oneMinute, fiveMinutes, thirtyMinutes, oneHour, fourHours, oneDay
} timeGap;
extern const int COUNT_TIMEGAP_ENUM;


@property (strong, nonatomic) NSString *imageKey;
@property (strong, nonatomic, readonly) NSImage *screenImage;

@property (nonatomic) NSRect diffBox;

@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSURL *diffURL;

- (void)executeImageOperationWithScreenImage:(ImagePipelineOperation *)op;

- (void)setMetadata:(MetadataSnapshot *)metadata;
- (void)setAsNullDiffFromMoment:(Moment *)baseMoment;

- (void)makeIndependent;
- (void)deleteMoment;

+ (NSArray *)firstNMoments:(NSInteger)n moc:(NSManagedObjectContext *)moc;

@end
