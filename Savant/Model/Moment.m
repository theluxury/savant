#import "Moment.h"
#import "DataManager.h"
#import "NSImage+Diff.h"
#import "NSColor+PrettyColors.h"
#import "NSString+Contains.h"
#import "NSImage+SaveImageRef.h"
#import "NSManagedObject+Find.h"

#import "MomentBlock.h"
#import "ApplicationCount.h"

#import "ImageFetchOperation.h"

@implementation Moment
const int COUNT_TIMEGAP_ENUM = 9;

@synthesize imageKey = _imageKey;

- (void)setDiffBox:(NSRect)diffBox
{
    self.diffRectString = NSStringFromRect(diffBox);
}
- (NSRect)diffBox
{
    return NSRectFromString(self.diffRectString);
}

- (NSURL *)imageURL
{
    return [NSURL URLWithString:self.imagePath];
}
- (void)setImageURL:(NSURL *)imageURL
{
    self.imagePath = [imageURL absoluteString];
}
- (NSURL *)diffURL
{
    return [NSURL URLWithString:self.diffPath];
}
- (void)setDiffURL:(NSURL *)diffURL
{
    self.diffPath = [diffURL absoluteString];
}

- (NSString *)imageKey
{
    if(_imageKey) return _imageKey;
    if(self.isNullDiffValue) return _imageKey = [self.baseMoment imageKey];
    return _imageKey = self.imagePath.lastPathComponent ?: self.diffPath.lastPathComponent;
}


- (NSImage *)screenImage
{
    NSCache *cache = [[DataManager sharedInstance] imageCache];
    NSImage *image = [cache objectForKey:self.imageKey];
    if (!image) {
        if(self.imagePath) {
            NSURL *imageUrl = [ImageFetchOperation convertUrl:self.imageURL];
            image = [[NSImage alloc] initByReferencingURL:imageUrl];
        } else {
            NSURL *diffUrl = [ImageFetchOperation convertUrl:self.diffURL];
            image = [NSImage applyDiff: [[NSImage alloc] initByReferencingURL:diffUrl]
                               toImage: [self.baseMoment screenImage]
                               diffBox: NSRectFromCGRect(self.diffBox)];
        }
        [cache setObject:image forKey:self.imageKey];
    }
    return image;
}

- (void)executeImageOperationWithScreenImage:(ImagePipelineOperation *)op
{
    DataManager *dm = [DataManager sharedInstance];
    NSImage *image = [dm.imageCache objectForKey:self.imageKey];
    
    if (image){
        op.inImage = image;
    } else {
        
        // TODO: should probably just record moments with the base moment against which they are not null
        Moment *moment = self;
        while (moment.isNullDiffValue){
            moment = moment.baseMoment;
        }
        NSCache *fetchCache = dm.fetchOperationsCache;
        ImageFetchOperation *fetchOp = [fetchCache objectForKey:[moment objectID]];
        if(fetchOp){
            @synchronized(fetchOp) {
                if (fetchOp.isFinished) {
                    op.inImage = fetchOp.fetchedImage;
                } else {
                    [op addImageDependency:fetchOp];
                }
            }
        } else {
            fetchOp = [dm createFetchOperationForMoment:moment];
            [op addImageDependency:fetchOp];
            // If not a base image, recurse
            if(!moment.imageURL){
                [moment.baseMoment executeImageOperationWithScreenImage:fetchOp];
            } else {
                [dm.imageFetchingQueue addOperation:fetchOp];
            }
        }
    }
    
    [dm.imageFetchingQueue addOperation:op];
}

- (void)setMetadata:(MetadataSnapshot *)metadata
{
    self.activeApplication = metadata.activeApplication;
    self.textState = metadata.textState;
    if (metadata.url && ![metadata.url isEqualToString:@""])
        self.url = metadata.url;
    if (metadata.title && ![metadata.title isEqualToString:@""])
        self.title = metadata.title;
    if (metadata.activeAppRect && ![metadata.activeAppRect isEqualToString:@""]) {
        self.activeApplicationRect = metadata.activeAppRect;
    }
}

- (void)setAsNullDiffFromMoment:(Moment *)baseMoment
{
    self.isNullDiff = @YES;
    self.baseMoment = baseMoment;
    self.activeApplication = baseMoment.activeApplication;
    self.textState = baseMoment.textState;
    self.url = baseMoment.url;
    self.title = baseMoment.title;
}


// For debug purposes
- (void)printChain
{
    NSLog(@"%@ (isNullDiff:%d, hasImageURL:%d)", self.timestamp, self.isNullDiffValue, !!self.imageURL);
    if(self.baseMoment){
        [self.baseMoment printChain];
    }
}
// Prints the entire chain tree of which this is a part
- (void)printTreeChain
{
    Moment *m = self;
    while (m.baseMoment){
        m = m.baseMoment;
    }
    [m printDescendantTreeWithPrefix:@""];
}
- (void)printDescendantTreeWithPrefix:(NSString *)prefix
{
    NSLog(@"%@%@ (isNullDiff:%d)", prefix, self.timestamp, self.isNullDiffValue);
    NSArray *dependantArray = [self.dependantMoments sortedArrayUsingDescriptors:@[
                                                                                   [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES]
                                                                                   ]];
    for (Moment *d in dependantArray) {
        [d printDescendantTreeWithPrefix:[prefix stringByAppendingString:@"-"]];
    }
}


// These might well explode if you're concurrently fetching images or something; for now,
// just try not to do that
- (void)makeIndependent
{
    NSString *idxStr = [self.diffPath componentsSeparatedByString:@"-"].lastObject;
    self.imageURL = [self.screenImage saveImageWithIdentifier:idxStr];
    
    self.baseMoment = nil;
    
    [[NSFileManager defaultManager] removeItemAtPath:self.diffPath error:nil];
    self.diffPath = nil;
    
    self.diffRectString = nil;
    self.isNullDiff = @NO;
    // Technically we should also recursively update the diffDepth, but we don't use
    // it for anything other than debugging atm
}

- (void)deleteMoment
{
    // We do this explicitly so that the count check will work properly when deleting multiple at once
    MomentBlock *block = self.momentBlock;
    [block removeMomentObject:self];
    if(block.moment.count == 0){
        [self.managedObjectContext deleteObject:block];
    }
    
    ApplicationCount *ac = [ApplicationCount findWithPredicate:[NSPredicate predicateWithFormat:@"name == %@", self.activeApplication]
                                                       context:self.managedObjectContext];
    // Not sure why there sometimes isn't an applicationCount, but it does seem to happen
    // Probably a bug, but fine for now
    if(ac){
        ac.countValue = ac.countValue - 1;
        if(ac.countValue == 0){
            [self.managedObjectContext deleteObject:ac];
        }
    }
    
    NSURL *url = self.imageURL ?: self.diffURL;
    [[NSFileManager defaultManager] removeItemAtURL:url error:nil];
    
    [self.managedObjectContext deleteObject:self];
}

+ (NSArray *)firstNMoments:(NSInteger)n moc:(NSManagedObjectContext *)moc
{
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    req.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES]];
    req.fetchLimit = n;
    
    return [moc executeFetchRequest:req error:nil];
}


@end
