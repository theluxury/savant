// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ApplicationCount.h instead.

#import <CoreData/CoreData.h>


extern const struct ApplicationCountAttributes {
	__unsafe_unretained NSString *count;
	__unsafe_unretained NSString *icon;
	__unsafe_unretained NSString *mostRecent;
	__unsafe_unretained NSString *name;
} ApplicationCountAttributes;

extern const struct ApplicationCountRelationships {
} ApplicationCountRelationships;

extern const struct ApplicationCountFetchedProperties {
} ApplicationCountFetchedProperties;



@class NSObject;



@interface ApplicationCountID : NSManagedObjectID {}
@end

@interface _ApplicationCount : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ApplicationCountID*)objectID;





@property (nonatomic, strong) NSNumber* count;



@property int64_t countValue;
- (int64_t)countValue;
- (void)setCountValue:(int64_t)value_;

//- (BOOL)validateCount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id icon;



//- (BOOL)validateIcon:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecent;



//- (BOOL)validateMostRecent:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;






@end

@interface _ApplicationCount (CoreDataGeneratedAccessors)

@end

@interface _ApplicationCount (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCount;
- (void)setPrimitiveCount:(NSNumber*)value;

- (int64_t)primitiveCountValue;
- (void)setPrimitiveCountValue:(int64_t)value_;




- (id)primitiveIcon;
- (void)setPrimitiveIcon:(id)value;




- (NSDate*)primitiveMostRecent;
- (void)setPrimitiveMostRecent:(NSDate*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




@end
