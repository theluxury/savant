// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ApplicationCount.m instead.

#import "_ApplicationCount.h"

const struct ApplicationCountAttributes ApplicationCountAttributes = {
	.count = @"count",
	.icon = @"icon",
	.mostRecent = @"mostRecent",
	.name = @"name",
};

const struct ApplicationCountRelationships ApplicationCountRelationships = {
};

const struct ApplicationCountFetchedProperties ApplicationCountFetchedProperties = {
};

@implementation ApplicationCountID
@end

@implementation _ApplicationCount

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ApplicationCount" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ApplicationCount";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ApplicationCount" inManagedObjectContext:moc_];
}

- (ApplicationCountID*)objectID {
	return (ApplicationCountID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic count;



- (int64_t)countValue {
	NSNumber *result = [self count];
	return [result longLongValue];
}

- (void)setCountValue:(int64_t)value_ {
	[self setCount:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCountValue {
	NSNumber *result = [self primitiveCount];
	return [result longLongValue];
}

- (void)setPrimitiveCountValue:(int64_t)value_ {
	[self setPrimitiveCount:[NSNumber numberWithLongLong:value_]];
}





@dynamic icon;






@dynamic mostRecent;






@dynamic name;











@end
