// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MTag.h instead.

#import <CoreData/CoreData.h>


extern const struct MTagAttributes {
	__unsafe_unretained NSString *name;
} MTagAttributes;

extern const struct MTagRelationships {
	__unsafe_unretained NSString *moments;
} MTagRelationships;

extern const struct MTagFetchedProperties {
} MTagFetchedProperties;

@class Moment;



@interface MTagID : NSManagedObjectID {}
@end

@interface _MTag : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MTagID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *moments;

- (NSMutableSet*)momentsSet;





@end

@interface _MTag (CoreDataGeneratedAccessors)

- (void)addMoments:(NSSet*)value_;
- (void)removeMoments:(NSSet*)value_;
- (void)addMomentsObject:(Moment*)value_;
- (void)removeMomentsObject:(Moment*)value_;

@end

@interface _MTag (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveMoments;
- (void)setPrimitiveMoments:(NSMutableSet*)value;


@end
