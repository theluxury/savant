// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MTag.m instead.

#import "_MTag.h"

const struct MTagAttributes MTagAttributes = {
	.name = @"name",
};

const struct MTagRelationships MTagRelationships = {
	.moments = @"moments",
};

const struct MTagFetchedProperties MTagFetchedProperties = {
};

@implementation MTagID
@end

@implementation _MTag

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MTag" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MTag";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MTag" inManagedObjectContext:moc_];
}

- (MTagID*)objectID {
	return (MTagID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic name;






@dynamic moments;

	
- (NSMutableSet*)momentsSet {
	[self willAccessValueForKey:@"moments"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"moments"];
  
	[self didAccessValueForKey:@"moments"];
	return result;
}
	






@end
