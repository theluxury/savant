// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Moment.h instead.

#import <CoreData/CoreData.h>


extern const struct MomentAttributes {
	__unsafe_unretained NSString *activeApplication;
	__unsafe_unretained NSString *activeApplicationRect;
	__unsafe_unretained NSString *diffDepth;
	__unsafe_unretained NSString *diffPath;
	__unsafe_unretained NSString *diffRectString;
	__unsafe_unretained NSString *imagePath;
	__unsafe_unretained NSString *isNullDiff;
	__unsafe_unretained NSString *timeGap;
	__unsafe_unretained NSString *timestamp;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *url;
} MomentAttributes;

extern const struct MomentRelationships {
	__unsafe_unretained NSString *baseMoment;
	__unsafe_unretained NSString *dependantMoments;
	__unsafe_unretained NSString *momentBlock;
	__unsafe_unretained NSString *tags;
	__unsafe_unretained NSString *textState;
} MomentRelationships;

extern const struct MomentFetchedProperties {
} MomentFetchedProperties;

@class Moment;
@class Moment;
@class MomentBlock;
@class MTag;
@class TextState;













@interface MomentID : NSManagedObjectID {}
@end

@interface _Moment : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MomentID*)objectID;





@property (nonatomic, strong) NSString* activeApplication;



//- (BOOL)validateActiveApplication:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* activeApplicationRect;



//- (BOOL)validateActiveApplicationRect:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* diffDepth;



@property int32_t diffDepthValue;
- (int32_t)diffDepthValue;
- (void)setDiffDepthValue:(int32_t)value_;

//- (BOOL)validateDiffDepth:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* diffPath;



//- (BOOL)validateDiffPath:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* diffRectString;



//- (BOOL)validateDiffRectString:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* imagePath;



//- (BOOL)validateImagePath:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* isNullDiff;



@property BOOL isNullDiffValue;
- (BOOL)isNullDiffValue;
- (void)setIsNullDiffValue:(BOOL)value_;

//- (BOOL)validateIsNullDiff:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* timeGap;



@property double timeGapValue;
- (double)timeGapValue;
- (void)setTimeGapValue:(double)value_;

//- (BOOL)validateTimeGap:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* timestamp;



//- (BOOL)validateTimestamp:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Moment *baseMoment;

//- (BOOL)validateBaseMoment:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *dependantMoments;

- (NSMutableSet*)dependantMomentsSet;




@property (nonatomic, strong) MomentBlock *momentBlock;

//- (BOOL)validateMomentBlock:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *tags;

- (NSMutableSet*)tagsSet;




@property (nonatomic, strong) TextState *textState;

//- (BOOL)validateTextState:(id*)value_ error:(NSError**)error_;





@end

@interface _Moment (CoreDataGeneratedAccessors)

- (void)addDependantMoments:(NSSet*)value_;
- (void)removeDependantMoments:(NSSet*)value_;
- (void)addDependantMomentsObject:(Moment*)value_;
- (void)removeDependantMomentsObject:(Moment*)value_;

- (void)addTags:(NSSet*)value_;
- (void)removeTags:(NSSet*)value_;
- (void)addTagsObject:(MTag*)value_;
- (void)removeTagsObject:(MTag*)value_;

@end

@interface _Moment (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveActiveApplication;
- (void)setPrimitiveActiveApplication:(NSString*)value;




- (NSString*)primitiveActiveApplicationRect;
- (void)setPrimitiveActiveApplicationRect:(NSString*)value;




- (NSNumber*)primitiveDiffDepth;
- (void)setPrimitiveDiffDepth:(NSNumber*)value;

- (int32_t)primitiveDiffDepthValue;
- (void)setPrimitiveDiffDepthValue:(int32_t)value_;




- (NSString*)primitiveDiffPath;
- (void)setPrimitiveDiffPath:(NSString*)value;




- (NSString*)primitiveDiffRectString;
- (void)setPrimitiveDiffRectString:(NSString*)value;




- (NSString*)primitiveImagePath;
- (void)setPrimitiveImagePath:(NSString*)value;




- (NSNumber*)primitiveIsNullDiff;
- (void)setPrimitiveIsNullDiff:(NSNumber*)value;

- (BOOL)primitiveIsNullDiffValue;
- (void)setPrimitiveIsNullDiffValue:(BOOL)value_;




- (NSNumber*)primitiveTimeGap;
- (void)setPrimitiveTimeGap:(NSNumber*)value;

- (double)primitiveTimeGapValue;
- (void)setPrimitiveTimeGapValue:(double)value_;




- (NSDate*)primitiveTimestamp;
- (void)setPrimitiveTimestamp:(NSDate*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (Moment*)primitiveBaseMoment;
- (void)setPrimitiveBaseMoment:(Moment*)value;



- (NSMutableSet*)primitiveDependantMoments;
- (void)setPrimitiveDependantMoments:(NSMutableSet*)value;



- (MomentBlock*)primitiveMomentBlock;
- (void)setPrimitiveMomentBlock:(MomentBlock*)value;



- (NSMutableSet*)primitiveTags;
- (void)setPrimitiveTags:(NSMutableSet*)value;



- (TextState*)primitiveTextState;
- (void)setPrimitiveTextState:(TextState*)value;


@end
