// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Moment.m instead.

#import "_Moment.h"

const struct MomentAttributes MomentAttributes = {
	.activeApplication = @"activeApplication",
	.activeApplicationRect = @"activeApplicationRect",
	.diffDepth = @"diffDepth",
	.diffPath = @"diffPath",
	.diffRectString = @"diffRectString",
	.imagePath = @"imagePath",
	.isNullDiff = @"isNullDiff",
	.timeGap = @"timeGap",
	.timestamp = @"timestamp",
	.title = @"title",
	.url = @"url",
};

const struct MomentRelationships MomentRelationships = {
	.baseMoment = @"baseMoment",
	.dependantMoments = @"dependantMoments",
	.momentBlock = @"momentBlock",
	.tags = @"tags",
	.textState = @"textState",
};

const struct MomentFetchedProperties MomentFetchedProperties = {
};

@implementation MomentID
@end

@implementation _Moment

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Moment" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Moment";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Moment" inManagedObjectContext:moc_];
}

- (MomentID*)objectID {
	return (MomentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"diffDepthValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"diffDepth"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isNullDiffValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isNullDiff"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"timeGapValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"timeGap"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic activeApplication;






@dynamic activeApplicationRect;






@dynamic diffDepth;



- (int32_t)diffDepthValue {
	NSNumber *result = [self diffDepth];
	return [result intValue];
}

- (void)setDiffDepthValue:(int32_t)value_ {
	[self setDiffDepth:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDiffDepthValue {
	NSNumber *result = [self primitiveDiffDepth];
	return [result intValue];
}

- (void)setPrimitiveDiffDepthValue:(int32_t)value_ {
	[self setPrimitiveDiffDepth:[NSNumber numberWithInt:value_]];
}





@dynamic diffPath;






@dynamic diffRectString;






@dynamic imagePath;






@dynamic isNullDiff;



- (BOOL)isNullDiffValue {
	NSNumber *result = [self isNullDiff];
	return [result boolValue];
}

- (void)setIsNullDiffValue:(BOOL)value_ {
	[self setIsNullDiff:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsNullDiffValue {
	NSNumber *result = [self primitiveIsNullDiff];
	return [result boolValue];
}

- (void)setPrimitiveIsNullDiffValue:(BOOL)value_ {
	[self setPrimitiveIsNullDiff:[NSNumber numberWithBool:value_]];
}





@dynamic timeGap;



- (double)timeGapValue {
	NSNumber *result = [self timeGap];
	return [result doubleValue];
}

- (void)setTimeGapValue:(double)value_ {
	[self setTimeGap:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveTimeGapValue {
	NSNumber *result = [self primitiveTimeGap];
	return [result doubleValue];
}

- (void)setPrimitiveTimeGapValue:(double)value_ {
	[self setPrimitiveTimeGap:[NSNumber numberWithDouble:value_]];
}





@dynamic timestamp;






@dynamic title;






@dynamic url;






@dynamic baseMoment;

	

@dynamic dependantMoments;

	
- (NSMutableSet*)dependantMomentsSet {
	[self willAccessValueForKey:@"dependantMoments"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"dependantMoments"];
  
	[self didAccessValueForKey:@"dependantMoments"];
	return result;
}
	

@dynamic momentBlock;

	

@dynamic tags;

	
- (NSMutableSet*)tagsSet {
	[self willAccessValueForKey:@"tags"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"tags"];
  
	[self didAccessValueForKey:@"tags"];
	return result;
}
	

@dynamic textState;

	






@end
