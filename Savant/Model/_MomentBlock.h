// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MomentBlock.h instead.

#import <CoreData/CoreData.h>


extern const struct MomentBlockAttributes {
	__unsafe_unretained NSString *activeApplication;
	__unsafe_unretained NSString *endTime;
	__unsafe_unretained NSString *startTime;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *url;
} MomentBlockAttributes;

extern const struct MomentBlockRelationships {
	__unsafe_unretained NSString *moment;
} MomentBlockRelationships;

extern const struct MomentBlockFetchedProperties {
} MomentBlockFetchedProperties;

@class Moment;







@interface MomentBlockID : NSManagedObjectID {}
@end

@interface _MomentBlock : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MomentBlockID*)objectID;





@property (nonatomic, strong) NSString* activeApplication;



//- (BOOL)validateActiveApplication:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* endTime;



//- (BOOL)validateEndTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* startTime;



//- (BOOL)validateStartTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *moment;

- (NSMutableSet*)momentSet;





@end

@interface _MomentBlock (CoreDataGeneratedAccessors)

- (void)addMoment:(NSSet*)value_;
- (void)removeMoment:(NSSet*)value_;
- (void)addMomentObject:(Moment*)value_;
- (void)removeMomentObject:(Moment*)value_;

@end

@interface _MomentBlock (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveActiveApplication;
- (void)setPrimitiveActiveApplication:(NSString*)value;




- (NSDate*)primitiveEndTime;
- (void)setPrimitiveEndTime:(NSDate*)value;




- (NSDate*)primitiveStartTime;
- (void)setPrimitiveStartTime:(NSDate*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (NSMutableSet*)primitiveMoment;
- (void)setPrimitiveMoment:(NSMutableSet*)value;


@end
