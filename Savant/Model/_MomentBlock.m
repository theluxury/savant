// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MomentBlock.m instead.

#import "_MomentBlock.h"

const struct MomentBlockAttributes MomentBlockAttributes = {
	.activeApplication = @"activeApplication",
	.endTime = @"endTime",
	.startTime = @"startTime",
	.title = @"title",
	.url = @"url",
};

const struct MomentBlockRelationships MomentBlockRelationships = {
	.moment = @"moment",
};

const struct MomentBlockFetchedProperties MomentBlockFetchedProperties = {
};

@implementation MomentBlockID
@end

@implementation _MomentBlock

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MomentBlock" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MomentBlock";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MomentBlock" inManagedObjectContext:moc_];
}

- (MomentBlockID*)objectID {
	return (MomentBlockID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic activeApplication;






@dynamic endTime;






@dynamic startTime;






@dynamic title;






@dynamic url;






@dynamic moment;

	
- (NSMutableSet*)momentSet {
	[self willAccessValueForKey:@"moment"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"moment"];
  
	[self didAccessValueForKey:@"moment"];
	return result;
}
	






@end
