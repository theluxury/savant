// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextNode.h instead.

#import <CoreData/CoreData.h>


extern const struct TextNodeAttributes {
	__unsafe_unretained NSString *rect;
	__unsafe_unretained NSString *text;
} TextNodeAttributes;

extern const struct TextNodeRelationships {
	__unsafe_unretained NSString *textStates;
} TextNodeRelationships;

extern const struct TextNodeFetchedProperties {
} TextNodeFetchedProperties;

@class TextState;




@interface TextNodeID : NSManagedObjectID {}
@end

@interface _TextNode : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TextNodeID*)objectID;





@property (nonatomic, strong) NSString* rect;



//- (BOOL)validateRect:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *textStates;

- (NSMutableSet*)textStatesSet;





@end

@interface _TextNode (CoreDataGeneratedAccessors)

- (void)addTextStates:(NSSet*)value_;
- (void)removeTextStates:(NSSet*)value_;
- (void)addTextStatesObject:(TextState*)value_;
- (void)removeTextStatesObject:(TextState*)value_;

@end

@interface _TextNode (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveRect;
- (void)setPrimitiveRect:(NSString*)value;




- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;





- (NSMutableSet*)primitiveTextStates;
- (void)setPrimitiveTextStates:(NSMutableSet*)value;


@end
