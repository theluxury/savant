// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextNode.m instead.

#import "_TextNode.h"

const struct TextNodeAttributes TextNodeAttributes = {
	.rect = @"rect",
	.text = @"text",
};

const struct TextNodeRelationships TextNodeRelationships = {
	.textStates = @"textStates",
};

const struct TextNodeFetchedProperties TextNodeFetchedProperties = {
};

@implementation TextNodeID
@end

@implementation _TextNode

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TextNode" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TextNode";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TextNode" inManagedObjectContext:moc_];
}

- (TextNodeID*)objectID {
	return (TextNodeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic rect;






@dynamic text;






@dynamic textStates;

	
- (NSMutableSet*)textStatesSet {
	[self willAccessValueForKey:@"textStates"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"textStates"];
  
	[self didAccessValueForKey:@"textStates"];
	return result;
}
	






@end
