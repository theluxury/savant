// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextState.h instead.

#import <CoreData/CoreData.h>


extern const struct TextStateAttributes {
} TextStateAttributes;

extern const struct TextStateRelationships {
	__unsafe_unretained NSString *moments;
	__unsafe_unretained NSString *textNodes;
} TextStateRelationships;

extern const struct TextStateFetchedProperties {
} TextStateFetchedProperties;

@class Moment;
@class TextNode;


@interface TextStateID : NSManagedObjectID {}
@end

@interface _TextState : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TextStateID*)objectID;





@property (nonatomic, strong) NSSet *moments;

- (NSMutableSet*)momentsSet;




@property (nonatomic, strong) NSSet *textNodes;

- (NSMutableSet*)textNodesSet;





@end

@interface _TextState (CoreDataGeneratedAccessors)

- (void)addMoments:(NSSet*)value_;
- (void)removeMoments:(NSSet*)value_;
- (void)addMomentsObject:(Moment*)value_;
- (void)removeMomentsObject:(Moment*)value_;

- (void)addTextNodes:(NSSet*)value_;
- (void)removeTextNodes:(NSSet*)value_;
- (void)addTextNodesObject:(TextNode*)value_;
- (void)removeTextNodesObject:(TextNode*)value_;

@end

@interface _TextState (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveMoments;
- (void)setPrimitiveMoments:(NSMutableSet*)value;



- (NSMutableSet*)primitiveTextNodes;
- (void)setPrimitiveTextNodes:(NSMutableSet*)value;


@end
