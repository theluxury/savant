// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextState.m instead.

#import "_TextState.h"

const struct TextStateAttributes TextStateAttributes = {
};

const struct TextStateRelationships TextStateRelationships = {
	.moments = @"moments",
	.textNodes = @"textNodes",
};

const struct TextStateFetchedProperties TextStateFetchedProperties = {
};

@implementation TextStateID
@end

@implementation _TextState

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TextState" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TextState";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TextState" inManagedObjectContext:moc_];
}

- (TextStateID*)objectID {
	return (TextStateID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic moments;

	
- (NSMutableSet*)momentsSet {
	[self willAccessValueForKey:@"moments"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"moments"];
  
	[self didAccessValueForKey:@"moments"];
	return result;
}
	

@dynamic textNodes;

	
- (NSMutableSet*)textNodesSet {
	[self willAccessValueForKey:@"textNodes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"textNodes"];
  
	[self didAccessValueForKey:@"textNodes"];
	return result;
}
	






@end
