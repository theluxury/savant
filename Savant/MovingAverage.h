//
//  MovingAverage.h
//  Savant
//
//  Created by Mark Wang on 10/1/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovingAverage : NSObject {
    NSMutableArray *samples;
    int sampleCount;
    int averageSize;
}
-(id)initWithSize:(int)size;
-(void)addSample:(NSInteger)sample;
-(NSInteger)movingAverage;

@end

