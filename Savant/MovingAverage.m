//
//  MovingAverage.m
//  Savant
//
//  Created by Mark Wang on 10/1/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "MovingAverage.h"

@implementation MovingAverage
-(id)initWithSize:(int)size {
    if (self = [super init]) {
        samples = [[NSMutableArray alloc] initWithCapacity:size];
        sampleCount = 0;
        averageSize = size;
    }
    return self;
}
-(void)addSample:(NSInteger)sample {
    int pos = fmodf(sampleCount++, (float)averageSize);
    [samples setObject:[NSNumber numberWithDouble:sample] atIndexedSubscript:pos];
}
-(NSInteger)movingAverage {
    //return [[samples valueForKeyPath:@"@sum.doubleValue"] doubleValue]/(sampleCount > averageSize-1?averageSize:sampleCount);
    // This is actually incorrect, but it's fine so that it doesn't bug out when you first start the app. 
    return [[samples valueForKeyPath:@"@sum.doubleValue"] doubleValue]/(averageSize);
}
@end
