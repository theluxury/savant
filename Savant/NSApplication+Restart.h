//
//  NSApplication+Restart.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-17.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSApplication (Restart)

+ (void)restart;

@end
