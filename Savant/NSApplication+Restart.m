//
//  NSApplication+Restart.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-17.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSApplication+Restart.h"

@implementation NSApplication (Restart)

+ (void)restart
{
    NSString *restartScript = [NSString stringWithFormat:@"kill %d \n open \"%@\"",
                               [[NSProcessInfo processInfo] processIdentifier],
                               [[NSBundle mainBundle] bundlePath]];
    NSTask *restartTask = [[NSTask alloc] init];
    [restartTask setLaunchPath:@"/bin/sh"];
    [restartTask setArguments:@[@"-c", restartScript]];
    [restartTask launch];
    [restartTask waitUntilExit];
}

@end
