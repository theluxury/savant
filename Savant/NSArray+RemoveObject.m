//
//  NSArray+RemoveObject.m
//  Savant
//
//  Created by Mark Wang on 10/3/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSArray+RemoveObject.h"

@implementation NSArray (RemoveObject)

- (NSArray *)savantRemoveObject:(id)object {
    NSMutableArray *mutableArray = [self mutableCopy];
    [mutableArray removeObjectIdenticalTo:object];
    return (NSArray *)mutableArray;
}

@end
