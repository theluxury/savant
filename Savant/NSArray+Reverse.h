//
//  NSArray+Reverse.h
//  Savant
//
//  Created by Mark Wang on 8/28/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Reverse)

- (NSArray *)reversedArray;

@end
