//
//  NSArrayController+SavantHelper.h
//  Savant
//
//  Created by Mark Wang on 8/16/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Moment.h"

@interface NSArrayController (SavantHelper)

- (void)removeAllObjects;

- (void)savantSetArray:(NSArray *)array predicate:(NSPredicate *)predicate zoomLevel:(NSInteger)zoomLevel;

- (void)setZoomedArray:(Moment *)moment beforeArray:(NSArray *)beforeArray afterArray:(NSArray *)afterArray predicate:(NSPredicate *)predicate zoomLevel:(NSInteger)zoomLevel;

@end
