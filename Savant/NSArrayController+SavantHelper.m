//
//  NSArrayController+SavantHelper.m
//  Savant
//
//  Created by Mark Wang on 8/16/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSArrayController+SavantHelper.h"

@implementation NSArrayController (RemoveAllObjects)

- (void)removeAllObjects {
    NSRange range = NSMakeRange(0, [[self arrangedObjects] count]);
    [self removeObjectsAtArrangedObjectIndexes:[NSIndexSet indexSetWithIndexesInRange:range]];
}

- (void)savantSetArray:(NSArray *)array predicate:(NSPredicate *)predicate zoomLevel:(NSInteger)zoomLevel {
    
    // Don't do anything if it's one.
    // So shouldn't not do anything if it's one, should just remove and add all, since the remove and adding is no longer done before this.
    // IMPORTANT need to remove filter predicate first otherwise there's an error with addObjects.
    [self removeAllObjects];
    [self setFilterPredicate:nil];
    if (zoomLevel == 1) {
        [self addObjects:array];
        [self setFilterPredicate:predicate];
        return;
    }
    
    // So what does it have to do? So you have to look at the first object, then only add objects afte that occur after every blank?
    NSMutableArray *zoomArray = [NSMutableArray array];
    Moment *originalMoment = [array objectAtIndex:0];
    NSDate *addedDate = [originalMoment timestamp];
    
    [zoomArray addObject:originalMoment];
    
    for (Moment *moment in array) {
        if ([addedDate timeIntervalSinceDate:[moment timestamp]] > zoomLevel) {
            [zoomArray addObject:moment];
            addedDate = [moment timestamp];
        }
    }
    // So this is the array that you should set _arrayController to. Also adding objects deletes the filters for some reason, so hence the predicate stuff.
    [self addObjects:zoomArray];
    [self setFilterPredicate:predicate];
}

- (void)setZoomedArray:(Moment *)moment beforeArray:(NSArray *)beforeArray afterArray:(NSArray *)afterArray predicate:(NSPredicate *)predicate zoomLevel:(NSInteger)zoomLevel {
    
    [self removeAllObjects];
    [self setFilterPredicate:nil];
    // Don't do anything if it's one.
    if (zoomLevel == 1) {
        [self addObjects:beforeArray];
        [self addObjects:afterArray];
        [self setFilterPredicate:predicate];
        return;
    }
    
    // So what does it have to do? So you have to look at the first object, then only add objects afte that occur after every blank?
    NSMutableArray *zoomArray = [NSMutableArray array];
    // I think this shold be fine if either of the arrays are nil?
    // So first adds the after array by first adding the selected Moment...
    [zoomArray addObject:moment];
    NSDate *addedDate = [moment timestamp];
    
    // So first adds the stuff for the before array
    for (Moment *beforeMoment in beforeArray) {
        if ([addedDate timeIntervalSinceDate:[beforeMoment timestamp]] > zoomLevel) {
            [zoomArray addObject:beforeMoment];
            addedDate = [beforeMoment timestamp];
        }
    }
    
    // Then adds teh stuff for the after array
    for (Moment *afterMoment in afterArray) {
        Moment *mostRecentMoment = [zoomArray objectAtIndex:0];
        if ([[afterMoment timestamp] timeIntervalSinceDate:[mostRecentMoment timestamp]] > zoomLevel) {
            [zoomArray insertObject:afterMoment atIndex:0];
        }
    }
    // So this is the array that you should set _arrayController to. Also adding objects deletes the filters for some reason, so hence the predicate stuff.
    [self addObjects:zoomArray];
    [self setFilterPredicate:predicate];
}

@end
