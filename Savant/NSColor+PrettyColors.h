//
//  NSColor+PrettyColors.h
//  Savant
//
//  Created by Mark Wang on 8/15/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (PrettyColors)

+ (NSColor *)savantRedColor;
+ (NSColor *)savantGreenColor;
+ (NSColor *)savantBlueColor;
+ (NSColor *)savantYellowColor;
+ (NSColor *)savantBackgroundGradientStart;
+ (NSColor *)savantBackgroundGradientEnd;
+ (NSColor *)savantTimeColor;
+ (NSColor *)savantFocusedSelectionColor;

+ (NSColor *)savantTintColor;

@end
