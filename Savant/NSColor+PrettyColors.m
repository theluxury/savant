//
//  NSColor+PrettyColors.m
//  Savant
//
//  Created by Mark Wang on 8/15/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSColor+PrettyColors.h"

@implementation NSColor (PrettyColors)

+ (NSColor *)savantRedColor {
    return [NSColor colorWithRed:238/255.0 green:75/255.0 blue:28/255.0 alpha:1.0];
}

+ (NSColor *)savantGreenColor {
    return [NSColor colorWithRed:17/255.0 green:186/255.0 blue:61/255.0 alpha:1.0];
}

+ (NSColor *)savantBlueColor {
    return [NSColor colorWithRed:44/255.0 green:155/255.0 blue:182/255.0 alpha:1.0];
}

+ (NSColor *)savantYellowColor {
    return [NSColor colorWithRed:251/255.0 green:222/255.0 blue:94/255.0 alpha:1.0];
}

+ (NSColor *)savantBackgroundGradientStart {
    return [NSColor colorWithRed:51 / 255.0 green:51 / 255.0 blue:51 / 255.0 alpha:1.0];
}

+ (NSColor *)savantBackgroundGradientEnd {
    return [NSColor colorWithRed:76 / 255.0 green:76 / 255.0 blue:76 / 255.0 alpha:1.0];
}

+ (NSColor *)savantTimeColor {
    return [NSColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
}

+ (NSColor *)savantFocusedSelectionColor {
    return [NSColor colorWithRed:100/255.0 green:149/255.0 blue:233/255.0 alpha:1];
}
+ (NSColor *)savantTintColor {
    return [NSColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:0.8];
}


@end
