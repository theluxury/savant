//
//  NSDate+CompareDates.h
//  Savant
//
//  Created by Mark Wang on 8/19/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (CompareDates)

- (BOOL)isSameMinuteWithDate:(NSDate*)date;
- (BOOL)isSameHourWithDate:(NSDate*)date;


@end
