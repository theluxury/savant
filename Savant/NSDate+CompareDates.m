//
//  NSDate+CompareDates.m
//  Savant
//
//  Created by Mark Wang on 8/19/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSDate+CompareDates.h"

@implementation NSDate (CompareDates)

// Following is to see if two NSDates have the same minute, to know which one to display.
- (BOOL)isSameMinuteWithDate:(NSDate*)date {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year] &&
    [comp1 hour] == [comp2 hour] &&
    [comp1 minute] == [comp2 minute];
}

// Following is to see if two NSDates have the same minute, to know which one to display.
- (BOOL)isSameHourWithDate:(NSDate*)date {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year] &&
    [comp1 hour] == [comp2 hour];
}
@end
