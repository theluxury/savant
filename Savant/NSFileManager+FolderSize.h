//
//  NSFileManager+FolderSize.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-20.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

// It's pretty dumb that this doesn't already exist...
@interface NSFileManager (FolderSize)

// Value is in bytes
- (unsigned long long)sizeOfDirectoryAtURL:(NSURL *)url;

@end
