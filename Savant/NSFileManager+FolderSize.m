//
//  NSFileManager+FolderSize.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-20.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSFileManager+FolderSize.h"

@implementation NSFileManager (FolderSize)

- (unsigned long long)sizeOfDirectoryAtURL:(NSURL *)url
{
    unsigned long long size = 0;
    NSArray *subPaths = [self subpathsAtPath:[url path]];
    for (NSString *path in subPaths) {
        NSDictionary *fileAttrs = [self attributesOfItemAtPath:path error:nil];
        size += [fileAttrs fileSize];
    }
    return size;
}

@end
