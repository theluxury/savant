//
//  CIImage+CIImage_Diff.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-03.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface NSImage (Diff)

+ (NSImage *)applyDiff:(NSImage *)diff toImage:(NSImage *)baseImg diffBox:(NSRect)diffBox;

@end
