//
//  CIImage+CIImage_Diff.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-03.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSImage+Diff.h"

@implementation NSImage (Diff)

+ (NSImage *)applyDiff:(NSImage *)diff toImage:(NSImage *)baseImg diffBox:(NSRect)diffBox
{
    NSBitmapImageRep* offscreenRep = nil;
    
    //NSLog(@"base image size is %f by %f", baseImg.size.width, baseImg.size.height);
    //NSLog(@"diff size is %f by %f", diff.size.width, diff.size.height);
    offscreenRep = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:nil
                                                           pixelsWide:baseImg.size.width
                                                           pixelsHigh:baseImg.size.height
                                                        bitsPerSample:8
                                                      samplesPerPixel:4
                                                             hasAlpha:YES
                                                             isPlanar:NO
                                                       colorSpaceName:NSCalibratedRGBColorSpace
                                                         bitmapFormat:0
                                                          bytesPerRow:(4 * baseImg.size.width)
                                                         bitsPerPixel:32];
    
    [NSGraphicsContext saveGraphicsState];
    [NSGraphicsContext setCurrentContext:[NSGraphicsContext
                                          graphicsContextWithBitmapImageRep:offscreenRep]];
    
    // @@?? also don't recreate context if true
    NSRect r = NSMakeRect(0, 0, baseImg.size.width, baseImg.size.height);
    // This created 15% of the lag when zooming out. 
    [baseImg drawInRect:r fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
    // This created other 85% of the lag.
    // ## test rect version performance/working
    [diff drawAtPoint:diffBox.origin fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    
    [NSGraphicsContext restoreGraphicsState];
    
    NSImage *result = [[NSImage alloc] initWithSize:baseImg.size];
    [result addRepresentation:offscreenRep];
    return result;
}

@end
