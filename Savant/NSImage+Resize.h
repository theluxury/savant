//
//  NSImage+Resize.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-31.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kThumbnailWidth 118
#define kThumbnailHeight 73

@interface NSImage (Resize)

- (NSImage *)resizedTo:(NSSize)size;
- (NSImage *)thumbnail;

@end
