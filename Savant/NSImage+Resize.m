//
//  NSImage+Resize.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-31.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSImage+Resize.h"

@implementation NSImage (Resize)

- (NSImage *)resizedTo:(NSSize)size
{
    NSImage *resizedImage = [[NSImage alloc] initWithSize:size];
    [resizedImage lockFocus];
    [self drawInRect:NSMakeRect(0, 0, size.width, size.height) fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
    [resizedImage unlockFocus];
    return resizedImage;
}

- (NSImage *)thumbnail
{
    return [self resizedTo:NSMakeSize(kThumbnailWidth, kThumbnailHeight)];
}

@end
