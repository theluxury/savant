//
//  NSImage+SaveImageRef.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-17.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (SaveImageRef)

+ (NSURL *)saveImageRef:(CGImageRef)imgRef idx:(NSInteger)idx;
+ (NSURL *)saveImageRef:(CGImageRef)imgRef idx:(NSInteger)idx prefix:(NSString *)pre;
- (NSURL *)saveImageWithIdentifier:(NSString *)prefix;

@end
