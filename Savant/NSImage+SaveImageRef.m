//
//  NSImage+SaveImageRef.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-17.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSImage+SaveImageRef.h"
#import "NSURL+SavantLocations.h"

@implementation NSImage (SaveImageRef)

+ (NSURL *)saveImageRef:(CGImageRef)imgRef idx:(NSInteger)idx
{
    return [self saveImageRef:imgRef idx:idx prefix:@""];
}

+ (NSURL *)saveImageRef:(CGImageRef)imgRef idx:(NSInteger)idx prefix:(NSString *)pre
{
    NSURL *destURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%ld.png", pre, (long)idx] relativeToURL:[NSURL SVRecordingDirectory]];
    
    // ## try using that multiple image param (the 1)
    CGImageDestinationRef idest =  CGImageDestinationCreateWithURL((__bridge CFURLRef)(destURL), kUTTypePNG, 1, NULL);
    CGImageDestinationAddImage(idest, imgRef, 0);
    CGImageDestinationFinalize(idest);
    CFRelease(idest);
    
    [self optimizeImageAtURL:destURL];
    
    return destURL;
}

+ (void)optimizeImageAtURL:(NSURL *)url
{
    // TODO: this should use the library
    // TODO: experiment with arguments
    NSString *path = [[NSBundle mainBundle] pathForResource:@"pngquant" ofType:nil];
    NSArray *args = @[url, @"--ext=.png", @"--force", @"--speed=10"];
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath:path];
    [task setArguments:args];
    [task launch];
}


// # this is a complete hack
- (NSURL *)saveImageWithIdentifier:(NSString *)prefix
{
    return [NSImage saveImageRef:[self CGImageForProposedRect:NULL context:nil hints:nil] idx:0 prefix:prefix];
}

@end
