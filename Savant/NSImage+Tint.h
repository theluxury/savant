//
//  NSImage+Tint.h
//  Savant
//
//  Created by Mark Wang on 9/20/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (Tint)

- (NSImage *)imageTintedWithColor:(NSColor *)tint;

@end
