//
//  NSImage+Tint.m
//  Savant
//
//  Created by Mark Wang on 9/20/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSImage+Tint.h"

@implementation NSImage (Tint)

- (NSImage *)imageTintedWithColor:(NSColor *)tint
{
    NSImage *image = [self copy];
    if (tint) {
        [image lockFocus];
        [tint set];
        NSRect imageRect = {NSZeroPoint, [image size]};
        NSRectFillUsingOperation(imageRect, NSCompositeSourceAtop);
        [image unlockFocus];
    }
    return image;
}

@end
