//
//  NSManagedObject+Find.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-17.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Find)

// Warning: this assumes that the managed object subclass implements +entityName
// (MOGenerator provides this by default).
+ (instancetype)findWithPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)moc;

@end
