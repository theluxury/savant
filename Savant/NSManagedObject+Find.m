//
//  NSManagedObject+Find.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-17.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSManagedObject+Find.h"

@implementation NSManagedObject (Find)

// This is assumed to overridden by the concrete subclasses
+ (NSString *)entityName
{
    return nil;
}

+ (instancetype)findWithPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)moc
{
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    req.fetchLimit = 1;
    req.predicate = predicate;
    NSArray *res = [moc executeFetchRequest:req error:nil];
    if(res.count > 0){
        return res[0];
    }
    return nil;
}

@end
