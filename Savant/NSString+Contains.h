//
//  NSString+Contains.h
//  Savant
//
//  Created by Mark Wang on 10/3/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Contains)

- (BOOL)contains:(NSString *)string;

@end
