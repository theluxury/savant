//
//  NSString+Contains.m
//  Savant
//
//  Created by Mark Wang on 10/3/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSString+Contains.h"

@implementation NSString (Contains)

- (BOOL)contains:(NSString *)string {
    if ([self rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
        return NO;
    
    return YES;
}


@end
