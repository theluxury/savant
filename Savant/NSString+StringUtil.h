//
//  NSString+CurrentApplication.h
//  Savant
//
//  Created by Nick Hain on 8/2/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringUtil)

- (NSRange)fullRange;

- (CGPoint)convertToPoint;

- (CGSize)convertToSize;

- (CGRect)convertToRect;

@end

@interface NSMutableAttributedString (StringUtil)
- (NSRange)fullRange;
@end