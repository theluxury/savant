
//
//  NSString+CurrentApplication.m
//  Savant
//
//  Created by Nick Hain on 8/2/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSString+StringUtil.h"
#import <AppKit/AppKit.h>

#import "Availability.h"

@implementation NSString (StringUtil)

- (NSRange)fullRange {
    return (NSRange){0, [self length]};
}

- (CGPoint)convertToPoint {
    // String should be of form {x, y}
    // First remove brackets
    NSString *tempString = [self stringByReplacingOccurrencesOfString:@"{" withString:@""];
    tempString = [tempString stringByReplacingOccurrencesOfString:@"}" withString:@""];
    // Then get parts sepererated by comma
    CGFloat x = [[tempString componentsSeparatedByString:@","][0] floatValue];
    CGFloat y = [[tempString componentsSeparatedByString:@","][1] floatValue];
    return CGPointMake(x, y);
}

- (CGSize)convertToSize {
    // String should also be of form {width, height}
    NSString *tempString = [self stringByReplacingOccurrencesOfString:@"{" withString:@""];
    tempString = [tempString stringByReplacingOccurrencesOfString:@"}" withString:@""];
    // Then get parts sepererated by comma
    CGFloat width = [[tempString componentsSeparatedByString:@","][0] floatValue];
    CGFloat height = [[tempString componentsSeparatedByString:@","][1] floatValue];
    return CGSizeMake(width, height);
}

- (CGRect)convertToRect {
    NSRect rect = NSRectFromString(self);
    // Have to do this step because it's stupid. Might be doing this wrong since I also have the flipUpsideDown call in VideoWindow class, but if I remove both it doesn't work, so think it's good for now.
    rect.origin.y = [[NSScreen mainScreen] frame].size.height - (rect.origin.y + rect.size.height);
    return rect;
}

@end


@implementation NSMutableAttributedString (StringUtil)

- (NSRange)fullRange {
    return (NSRange){0, [self length]};
}

@end