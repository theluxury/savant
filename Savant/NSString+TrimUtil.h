//
//  NSString+TrimUtil.h
//  Savant
//
//  Created by Mark Wang on 12/13/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TrimUtil)

- (NSString *)trimmedString;

@end
