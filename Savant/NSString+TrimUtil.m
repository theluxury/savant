//
//  NSString+TrimUtil.m
//  Savant
//
//  Created by Mark Wang on 12/13/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSString+TrimUtil.h"

@implementation NSString (TrimUtil)

- (NSString *)trimmedString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
