//
//  NSURL+SavantLocations.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-30.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (SavantLocations)

+ (NSURL *)SVDataDirectory;
+ (NSURL *)SVRecordingDirectory;
+ (NSURL *)SVPersistentStoreUrl;

@end
