//
//  NSURL+SavantLocations.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-30.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "NSURL+SavantLocations.h"
#import "AppDelegate.h"

@implementation NSURL (SavantLocations)

+ (NSURL *)SVDataDirectory {
    NSURL *baseURL = [NSURL fileURLWithPath:[[[NSUserDefaults standardUserDefaults] stringForKey:DATA_LOCATION_KEY] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
#if (DEBUG && !UNSIGNED_RELEASE)
    return [[baseURL URLByAppendingPathComponent:@"com.savant.Savant"] URLByAppendingPathComponent:@"DEBUG"];
#else
    return [baseURL URLByAppendingPathComponent:@"com.savant.Savant"];
#endif
}

+ (NSURL *)SVRecordingDirectory {
    return [[self SVDataDirectory] URLByAppendingPathComponent:@"recording"];
}

+ (NSURL *)SVPersistentStoreUrl {
    return [[self SVDataDirectory]URLByAppendingPathComponent:@"SavantData.storedata"];
}

- (void) stuffImWriting {
    // and this is the stuff I'm working on.
}

@end
