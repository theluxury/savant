//
//  NSWindow+canBecomeKeyWindow.h
//  Savant
//
//  Created by Paul Musgrave on 2015-01-06.
//  Copyright (c) 2015 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSWindow (canBecomeKeyWindow)

@end
