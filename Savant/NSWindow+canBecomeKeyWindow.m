//
//  NSWindow+canBecomeKeyWindow.m
//  Savant
//
//  Created by Paul Musgrave on 2015-01-06.
//  Copyright (c) 2015 Paul Musgrave. All rights reserved.
//

#import "NSWindow+canBecomeKeyWindow.h"

@implementation NSWindow (canBecomeKeyWindow)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
- (BOOL)canBecomeKeyWindow
{
    return YES;
}
#pragma clang diagnostic pop

@end
