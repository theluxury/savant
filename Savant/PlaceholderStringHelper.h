//
//  PlaceholderStringHelper.h
//  Savant
//
//  Created by Mark Wang on 8/10/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NSString+StringUtil.h"

@interface PlaceholderStringHelper : NSObject

extern const int PLATEAU_LENGTH;
extern const int FADE_LENGTH;
extern const int BRIGHTEN_LENGTH;

+ (void)setupPlaceholderArray:(NSMutableArray *)placeholderArray;
+ (NSMutableAttributedString *)setupString:(NSString *)string;
+ (void)changeStringColor:(NSMutableAttributedString *)attributedString textColor:(float)textColor;
+ (void)changePlaceholderText:(NSInteger)index
             placeholderArray:(NSMutableArray *)placeholderStrings
                    textField:(NSSearchField *)searchField;



@end
