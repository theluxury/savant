//
//  PlaceholderStringHelper.m
//  Savant
//
//  Created by Mark Wang on 8/10/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "PlaceholderStringHelper.h"

@implementation PlaceholderStringHelper : NSObject

const int PLATEAU_LENGTH = 60;
const int FADE_LENGTH = 20;
const int BRIGHTEN_LENGTH = 20;

static NSString * const PLACEHOLDER_STRING_ONE = @"@chrome cats";
static NSString * const PLACEHOLDER_STRING_TWO = @"~reddit memes";
static NSString * const PLACEHOLDER_STRING_THREE = @"cats";






+ (void)setupPlaceholderArray:(NSMutableArray *)placeholderArray {
    
    NSMutableAttributedString * string1 = [self setupString:PLACEHOLDER_STRING_ONE];
    NSMutableAttributedString * string2 = [self setupString:PLACEHOLDER_STRING_TWO];
    NSMutableAttributedString * string3 = [self setupString:PLACEHOLDER_STRING_THREE];
    
    [placeholderArray addObject:string1];
    [placeholderArray addObject:string2];
    [placeholderArray addObject:string3];
}

+ (NSMutableAttributedString *)setupString:(NSString *)string {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSColor *color = [NSColor colorWithCalibratedRed:0.5 green:0.5 blue:0.5 alpha:1.0];
    [attributedString addAttribute:NSForegroundColorAttributeName value:color range:[string fullRange]];
    return attributedString;
}

+ (void)changeStringColor:(NSMutableAttributedString *)attributedString textColor:(float)textColor {
    NSColor *color = [NSColor colorWithCalibratedRed:textColor green:textColor blue:textColor alpha:1.0];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:color forKey:NSForegroundColorAttributeName];
    [attributedString setAttributes:dictionary range:[attributedString fullRange]];
}

+ (void)changePlaceholderText:(NSInteger)index
             placeholderArray:(NSMutableArray *)placeholderStrings
                    textField:(NSSearchField *)searchField {
    // 3 chunks of of each time.
    
    int cycle = PLATEAU_LENGTH + FADE_LENGTH + BRIGHTEN_LENGTH;
    int indexInt = index % (cycle * 3) ;
    
    float indexFloat;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] init];
    
    // Break the 450 up into chunks. Every string should get 150 units of time, so break it up into thirds. But the way its set up like this is so the switch between the lines is at when it's not showing.
    
    if (indexInt < FADE_LENGTH) {
        string = [placeholderStrings objectAtIndex:0];
    } else if (indexInt < FADE_LENGTH + cycle) {
        string = [placeholderStrings objectAtIndex:1];
    } else if (indexInt < FADE_LENGTH + cycle * 2) {
        string = [placeholderStrings objectAtIndex:2];
    } else {
        string = [placeholderStrings objectAtIndex:0];
    }
    
    // Suchs hacks.
    if (indexInt < FADE_LENGTH) {
        // Darker to lighter as 0.5 -> 1
        indexFloat = 0.5 + indexInt / (FADE_LENGTH * 2.0);
    } else if (indexInt < FADE_LENGTH + BRIGHTEN_LENGTH) {
        // Lighter to darker 1->0.5
        indexInt -= FADE_LENGTH;
        indexFloat = 1 - indexInt / (BRIGHTEN_LENGTH * 2.0);
    } else if (indexInt < cycle) {
        // stays at 0.5
        indexFloat = 0.5;
    } else if (indexInt < cycle + FADE_LENGTH) {
        // 0.5 -> 1
        indexInt -= cycle;
        indexFloat = 0.5 + indexInt / (FADE_LENGTH * 2.0);
    } else if (indexInt < cycle + FADE_LENGTH + BRIGHTEN_LENGTH) {
        // 1 -> 0.5=
        indexInt -= (cycle + FADE_LENGTH);
        indexFloat = 1 - indexInt / (BRIGHTEN_LENGTH * 2.0);
    } else if (indexInt < 2 * cycle) {
        // stays at 0.5
        indexFloat = 0.5;
    } else if (indexInt < 2 * cycle + FADE_LENGTH) {
        // 0.5 -> 1
        indexInt -= 2 * cycle;
        indexFloat = 0.5 + indexInt / (FADE_LENGTH * 2.0);
    } else if (indexInt < 2 * cycle + FADE_LENGTH + BRIGHTEN_LENGTH) {
        // 1 -> 0.5
        indexInt -= (2 * cycle + FADE_LENGTH);
        indexFloat = 1 - indexInt / (BRIGHTEN_LENGTH * 2.0);
    } else {
        indexFloat = 0.5;
    }
    
    [self changeStringColor:string textColor:indexFloat];
    [[searchField cell] setPlaceholderAttributedString:string];
}

@end
