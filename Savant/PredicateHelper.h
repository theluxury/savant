//
//  PredicateHelper.h
//  Savant
//
//  Created by Mark Wang on 8/16/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Moment.h"
#import "DataManager.h"
#import "NSArrayController+SavantHelper.h"
#import "NSColor+PrettyColors.h"
#import "SharedPredicateHelper.h"

@interface PredicateHelper : NSObject

extern const NSInteger FETCH_LIMIT;
extern const NSInteger ZOOM_FETCH_LIMIT;
extern const NSInteger PLAYBACK_FETCH_LIMIT;

+ (NSPredicate *)getNewCombinedPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow timeGap:(NSInteger)timeGap string:(NSString *)string;

+ (NSPredicate *)getNewCombinedPredicateWithSelection:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow timeGap:(NSInteger)timeGap string:(NSString *)string moment:(Moment *)moment after:(BOOL)after;

+ (NSPredicate *)getPlaybackPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow moment:(Moment *)moment after:(BOOL)after;

+ (NSArray *)fetchScreenshots:(NSPredicate *)predicate storeArray:(NSArray *)storeArray withLimit:(NSInteger)limit ascending:(BOOL)ascending;

+ (NSArray *)fetchFilterApplications:(NSString *)attribute storeArray:(NSArray *)storeArray limit:(NSInteger)limit;


@end
