//
//  PredicateHelper.m
//  Savant
//
//  Created by Mark Wang on 8/16/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "PredicateHelper.h"
#import "SVBrowsingWindowController.h"
#import "AppDelegate.h"

@implementation PredicateHelper

// these should be a multiple of 5, since that's how many moments one screenshot takes. 
const NSInteger FETCH_LIMIT = 50;
const NSInteger ZOOM_FETCH_LIMIT = 50;
const NSInteger PLAYBACK_FETCH_LIMIT = 50;

// Ascending NO means the most recent. 

+ (NSPredicate *)setTimeGapPredicate:(NSInteger)timeGap {
    return [NSPredicate predicateWithFormat:@"timeGap >= %i", timeGap];
}

// I guess order here should be time, button, then search? Ideally want to combine the button and the wording on search predicate.

+ (NSPredicate *)getNewCombinedPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow timeGap:(NSInteger)timeGap string:(NSString *)string {
    
    NSPredicate *buttonPredicate = [SharedPredicateHelper setButtonPredicate:isBlue isRed:isRed isGreen:isGreen isYellow:isYellow];
    NSPredicate *timeGapPredicate = [PredicateHelper setTimeGapPredicate:timeGap];
    NSPredicate *searchPredicate = [SharedPredicateHelper setSearchPredicate:string entity:@"moment"];
    
    if (!buttonPredicate && !searchPredicate)
        return timeGapPredicate;
    else if (!searchPredicate)
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:timeGapPredicate, buttonPredicate,  nil]];
    else if (!buttonPredicate)
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: timeGapPredicate, searchPredicate, nil]];
    else
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:timeGapPredicate, buttonPredicate,  searchPredicate, nil]];
}

// TODO: Rename these later. 
+ (NSPredicate *)getNewCombinedPredicateWithSelection:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow timeGap:(NSInteger)timeGap string:(NSString *)string moment:(Moment *)moment after:(BOOL)after {
    
    // So this is where, um.... the sausage gets made.
    NSPredicate *predicate = [self getNewCombinedPredicate:isBlue isRed:isRed isGreen:isGreen isYellow:isYellow timeGap:timeGap string:string];
    
    NSPredicate *timePredicate;
    // Then adds a filter to make it only look for the stuff before or after the moment's time.
    if (after) {
        timePredicate = [NSPredicate predicateWithFormat:@"timestamp > %@", [moment timestamp]];
    } else {
        timePredicate = [NSPredicate predicateWithFormat:@"timestamp <= %@", [moment timestamp]];
    }
    
    // Forgot had to do check here to see if one of these is nil. timePredicate should never be nil.
    if (predicate == nil)
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: timePredicate, nil]];
    else
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: timePredicate, predicate, nil]];
}

+ (NSPredicate *)getPlaybackPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow moment:(Moment *)moment after:(BOOL)after {
    NSPredicate *predicate = [SharedPredicateHelper setButtonPredicate:isBlue isRed:isRed isGreen:isGreen isYellow:isYellow];
    NSPredicate *timePredicate;
    // Then adds a filter to make it only look for the stuff before or after the moment's time.
    if (after) {
        timePredicate = [NSPredicate predicateWithFormat:@"timestamp > %@", [moment timestamp]];
    } else {
        timePredicate = [NSPredicate predicateWithFormat:@"timestamp <= %@", [moment timestamp]];
    }
    
    // Forgot had to do check here to see if one of these is nil. timePredicate should never be nil.
    if (predicate == nil) {
        return timePredicate;
    } else {
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, timePredicate, nil]];
    }
}

+(NSArray *)fetchScreenshots:(NSPredicate *)predicate storeArray:(NSArray *)storeArray withLimit:(NSInteger)limit ascending:(BOOL)ascending {
    NSPredicate *nonNullPredicate = [NSPredicate predicateWithFormat:@"isNullDiff == nil"];
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: nonNullPredicate, predicate, nil]];
    
    return [SharedPredicateHelper fetch:@"moment" storeArray:storeArray predicate:compoundPredicate withLimit:limit ascending:ascending];
}


+ (NSArray *)fetchFilterApplications:(NSString *)attribute storeArray:(NSArray *)storeArray limit:(NSInteger)limit {
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"ApplicationCount" inManagedObjectContext:[[DataManager sharedInstance] mainMOC]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    [request setAffectedStores:storeArray];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:attribute ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    if (limit != 0)
        [request setFetchLimit:limit];
    NSError *error;
    return [[[DataManager sharedInstance] mainMOC] executeFetchRequest:request error:&error];
}



@end
