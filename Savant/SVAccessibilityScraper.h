//
//  SVAccessibilityScraper.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-26.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXApplicationMirror.h"
#import "AXWrapper.h"
#import "Moment.h"
#import "TextState.h"


@interface SVAccessibilityScraper : NSObject <AXApplicationMirrorDelegate>

+ (SVAccessibilityScraper *)sharedInstance;

- (void)refreshStateOfRect:(CGRect)rect;

- (TextState *)currentTextState;
- (NSString *)getActiveApplicationRect;

// Special application methods - will be nil unless the current application has a special handler
- (NSString *)getUrl;
- (NSString *)getTitle;

- (void)refreshFocusedWindow;

@end
