//
//  SVAccessibilityScraper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-26.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "SVAccessibilityScraper.h"
#import "DataManager.h"
#import "NSString+Contains.h"

#import "SpecialAppHelper.h"
#import "ChromeHelper.h"
#import "SafariHelper.h"
#import "FirefoxHelper.h"
#import "NSURL+SavantLocations.h"


// 's' + 'v' = 228
#define kRunningApplicationsChangedContext 228

NSString *NSAccessibilityEnhancedUserInterfaceAttribute = @"AXEnhancedUserInterface";


@interface SVAccessibilityScraper ()

@property (nonatomic, strong) AXWrapper *systemAXWrapper;
@property (nonatomic, strong) NSMutableDictionary *applicationMirrors;

// We want to make sure that we always use the same focused window within a frame,
// so we cache it
@property (nonatomic, strong) AXWrapper *curFocusedWindow;
// This is a bit lame, but should be ok for now
@property (nonatomic, strong) AXWrapper *prevFocusedWindow;

@property (nonatomic, strong) NSManagedObjectContext *moc;

@property (nonatomic, strong) NSMutableSet *currentTextNodes;
@property (nonatomic, strong) TextState *currentTextState;

@property BOOL textStateIsDirty;


@end


@implementation SVAccessibilityScraper

+ (SVAccessibilityScraper *)sharedInstance
{
    static SVAccessibilityScraper *sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^ { sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

-(instancetype)init
{
    if(self = [super init]){
        _systemAXWrapper = [AXWrapper wrapperForElement:AXUIElementCreateSystemWide()];
        _applicationMirrors = [NSMutableDictionary dictionary];
        _currentTextNodes = [NSMutableSet set];
        _textStateIsDirty = YES;
        
        _moc = [DataManager sharedInstance].recordingMOC;
        
        [self configureScraper]; //##?
    }
    return self;
}

- (void)configureScraper
{
    [[NSWorkspace sharedWorkspace] addObserver:self forKeyPath:@"runningApplications" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew) context:(void *)kRunningApplicationsChangedContext];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    switch ((int)context) {
        case kRunningApplicationsChangedContext:
            for (NSRunningApplication *app in change[NSKeyValueChangeNewKey]) {
                // ## hack
                // If we do it immediately, it won't work. Also tried waiting for isFinishedLaunching to be true,
                // but that doesn't work either
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSelector:@selector(enableAccessibilityForApplication:) withObject:app afterDelay:1.0];
                });
//                [self mirrorApplication:app.processIdentifier];
            }
            break;
            
        default:
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)enableAccessibilityForApplication:(NSRunningApplication *)app
{
    AXWrapper *axApp = [AXWrapper wrapperForPID:app.processIdentifier];
    [axApp setValue:@YES forAttribute:NSAccessibilityEnhancedUserInterfaceAttribute];
}

// ## withWindow is a total hack
- (void)mirrorApplication:(pid_t)pid withWindow:(AXWrapper *)windowWrapper
{
    AXApplicationMirror *mirror = [AXApplicationMirror mirrorForPid:pid];
    mirror.delegate = self;
    [mirror beginMirroringTextForWindow:windowWrapper];
    self.applicationMirrors[@(pid)] = mirror;
}
- (void)stopMirroringApplication:(pid_t)pid
{
    [self.applicationMirrors[@(pid)] stopMirroring];
    [self.applicationMirrors removeObjectForKey:@(pid)];
}

- (void)textNodeAdded:(TextNode *)textNode
{
    self.textStateIsDirty = YES;
    // TODO: @sync check
    @synchronized(_currentTextNodes) {
        [self.currentTextNodes addObject:textNode];
    }
}
- (void)textNodeRemoved:(TextNode *)textNode
{
    self.textStateIsDirty = YES;
    // TODO: @sync check
    @synchronized(_currentTextNodes) {
        [self.currentTextNodes removeObject:textNode];
    }
}

- (TextState *)currentTextState
{
    if(self.textStateIsDirty){
        @synchronized(self.currentTextNodes) {
            _currentTextState = [TextState insertInManagedObjectContext:self.moc];
//            NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
//            NSPersistentStore *mainStore = [psc persistentStoreForURL:[NSURL SVPersistentStoreUrl]];
//            [self.moc assignObject:_currentTextState toPersistentStore:mainStore];
            [_currentTextState setTextNodes:[self.currentTextNodes copy]];
        }
        self.textStateIsDirty = NO;
    }
    return _currentTextState;
}

- (NSString *)getActiveApplicationRect {
    return NSStringFromRect(self.curFocusedWindow.visibleRect);
}

- (NSString *)getUrl
{
    return [[self helperForCurrentApplication] getURLForWindow:self.curFocusedWindow];
}
- (NSString *)getTitle
{
    return [[self helperForCurrentApplication] getTitleForWindow:self.curFocusedWindow];
}

- (Class)helperForCurrentApplication
{
    NSString *appTitle = self.curFocusedWindow.parent.title;
    if([appTitle contains:@"chrome"]){
        return [ChromeHelper class];
    } else if([appTitle contains:@"safari"]){
        return [SafariHelper class];
    // TODO: crashes when url changes (gets a nil value). fix and reenable
//    } else if([appTitle contains:@"firefox"]){
//        return [FirefoxHelper class];
    }
    return nil;
}



//- (NSString *)scrapeCurrentState:(Moment *)moment
//{
//    NSMutableString *aggregateString = [NSMutableString string];
//    //## todo: reuse
////    for (NSRunningApplication *app in [self visibleApplications]){
////        AXWrapper *axApp = [AXWrapper wrapperForPID:app.processIdentifier];
////        for (AXWrapper *el in axApp.children) {
////            // Only scrape things that are in windows (exclude e.g. menuitems)
////            if ([el.role rangeOfString:@"AXWindow"].location != NSNotFound) {
////                [aggregateString appendString:[el aggregateVisibleText]];
////            }
////        }
////    }
//    
//    AXWrapper *focusedWindow = [self focusedAXWindow];
//    for (AXWrapper *el in focusedWindow.children) {
//        [aggregateString appendString:[el aggregateVisibleText:moment rect:[el getRect]]];
//    }
//    return aggregateString;
//}

- (void)refreshStateOfRect:(CGRect)rect
{
    if(![self.prevFocusedWindow isEqual:self.curFocusedWindow]){
        if(self.prevFocusedWindow){
            [self stopMirroringApplication:self.prevFocusedWindow.pid];
        }
        if(self.curFocusedWindow){
            [self mirrorApplication:self.curFocusedWindow.pid withWindow:self.curFocusedWindow];
        }
    } else {
        [[self.applicationMirrors objectForKey:@(self.curFocusedWindow.pid)] refreshContentInRect:rect];
    }
}

- (void)refreshFocusedWindow {
    // Observing the focused window is annoying, so poll for now
    self.prevFocusedWindow = self.curFocusedWindow;
    self.curFocusedWindow = [self focusedAXWindow];
}


- (AXWrapper *)focusedAXWindow
{
    return [[self.systemAXWrapper getAttribute:(__bridge NSString*)kAXFocusedApplicationAttribute] getAttribute:NSAccessibilityFocusedWindowAttribute];
}


//##
- (NSArray *)visibleApplications
{
    NSMutableArray *visibleApps = [NSMutableArray array];
    NSArray *runningApplications = [[NSWorkspace sharedWorkspace] runningApplications];
    for (NSRunningApplication *app in runningApplications){
        // ## pos. or NSApplicationActivationPolicyAccessory
        if (app.activationPolicy == NSApplicationActivationPolicyRegular && !app.hidden) {
            [visibleApps addObject:app];
        }
    }
    return visibleApps;
}


@end
