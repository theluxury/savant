//
//  SVBrowsingWindowController.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-30.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Moment.h"
#import "SynchroScrollView.h"
#import "SavantTableView.h"
#import "VideoWindowController.h"

@interface SVBrowsingWindowController : NSWindowController <NSTableViewDelegate, NSTableViewDataSource, NSURLConnectionDataDelegate, NSMenuDelegate>

@property (weak) IBOutlet SynchroScrollView *momentScrollView;
@property (weak) IBOutlet SynchroScrollView *imageScrollView;

@property (weak) IBOutlet SavantTableView *momentTableView;
@property (weak) IBOutlet SavantTableView *imageTableView;

@property (weak) VideoWindowController *videoController;

@property (weak) IBOutlet NSSearchField *searchField;

@property (weak) IBOutlet NSButton *blueButton;
@property (weak) IBOutlet NSButton *redButton;
@property (weak) IBOutlet NSButton *greenButton;
@property (weak) IBOutlet NSButton *yellowButton;
@property (weak) IBOutlet NSButton *zoomButton;

@property (weak) IBOutlet NSTextField *blueTag;
@property (weak) IBOutlet NSTextField *redTag;
@property (weak) IBOutlet NSTextField *greenTag;
@property (weak) IBOutlet NSTextField *yellowTag;


@property (weak) IBOutlet NSArrayController *arrayController;
@property (weak) IBOutlet NSArrayController *imageArrayController;

@property (weak) IBOutlet NSTextField *exportLabel;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;

@property (weak) IBOutlet NSPopUpButton *dbChooser;

- (IBAction)filterYellow:(NSButton *)sender;
- (IBAction)filterGreen:(NSButton *)sender;
- (IBAction)filterRed:(NSButton *)sender;
- (IBAction)filterBlue:(NSButton *)sender;
- (IBAction)performSearch:(NSSearchField *)sender;
- (IBAction)closeWindow:(NSButton *)sender;
- (IBAction)switchTableView:(NSButton *)sender;


- (IBAction)exportMomentTableView:(NSMenuItem *)sender;
- (IBAction)exportImageTableView:(NSMenuItem *)sender;
- (IBAction)zoomInMomentTableView:(NSMenuItem *)sender;
- (IBAction)zoomInImageTableView:(NSMenuItem *)sender;
- (IBAction)openApplicationMomentTableView:(NSMenuItem *)sender;
- (IBAction)openApplicationImageTableView:(NSMenuItem *)sender;
- (IBAction)switchFromSearch:(NSMenuItem *)sender;

- (IBAction)chooseDb:(NSPopUpButton *)sender;

@property (weak) IBOutlet NSMenu *momentContextMenu;
@property (weak) IBOutlet NSMenu *imageContextMenu;



- (void)clearData;

- (void)toggleVisibility:(NSString *)string;
- (BOOL)isActive;
- (void)closeVideoWindow;

- (NSString *)getSearchText;

- (void)deleteSelection;

@property BOOL isRed;
@property BOOL isBlue;
@property BOOL isGreen;
@property BOOL isYellow;

@property (nonatomic, strong) NSString *BLUE_APP_NAME;
@property (nonatomic, strong) NSString *RED_APP_NAME;
@property (nonatomic, strong) NSString *GREEN_APP_NAME;


@property (nonatomic, strong) NSDictionary *currShowingDict;
@property (nonatomic, strong) NSPersistentStore *currShowingStore;


@end
