//
//  SVBrowsingWindowController.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-30.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SVBrowsingWindowController.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "ImageBlockOperation.h"
#import "SavantRowView.h"
#import "PredicateHelper.h"
#import "PlaceholderStringHelper.h"
#import "NSDate+CompareDates.h"
#import "NSColor+PrettyColors.h"
#import "NSImage+Resize.h"
#import "ApplicationCount.h"
#import "NSImage+Tint.h"
#import "GAHelper.h"
#import "NSURL+SavantLocations.h"
#import "SavantTableView.h"
#import "DragAndDropHelper.h"
#import "NSURL+SavantLocations.h"
#import "BlockPredicateHelper.h"
#import "MomentBlock.h"
#import "SharedPredicateHelper.h"
#import "MTag.h"
#import <HockeySDK/HockeySDK.h>
#import "AlternateDatabaseHelper.h"
#import "NSString+TrimUtil.h"

// These are related change at your own risk!!!!>!!
const int THUMB_RATIO = 5;
const int MOMENT_CELL_HEIGHT = 15;
const int IMAGE_CELL_HEIGHT = 73;
const int BLOCK_CELL_HEIGHT = 75;

// identifiers for action taken on table view.
const int CLICKED_MOMENT = 1;
const int CLICKED_IMAGE = 2;
const int ARROW_KEY_MOMENT = 3;
const int ARROW_KEY_IMAGE = 4;

@interface SVBrowsingWindowController ()

@property (nonatomic, strong) Moment *selectedMoment;

@property (nonatomic, strong) NSMutableArray *placeholderStrings;
@property (nonatomic) NSInteger placeholderStringIndex;
@property (nonatomic) NSInteger zoomLevel;

@property (nonatomic, strong) NSDateFormatter *minimalDateFormatter;
@property (nonatomic, strong) NSDateFormatter *longerDateFormatter;
@property (nonatomic, strong) NSDateFormatter *dateOnlyDateFormatter;

@property (nonatomic, strong) NSMutableDictionary *pendingRowOperations;

@property (nonatomic, strong) NSMutableArray *applicationCountArray;
@property (nonatomic, strong) NSMutableArray *momentBlockArray;
@property (nonatomic) BOOL isFetching;
@property (nonatomic) BOOL directoryView;

@property (nonatomic, strong) NSArray *currDbArray;
@property (nonatomic, strong) NSString *codeSearchString;

// This needs to be here to be strong
@property (nonatomic, strong) AVAssetWriter *videoWriter;

@end


@implementation SVBrowsingWindowController
#define KEY_LEFT_ARROW 123
#define KEY_RIGHT_ARROW 124

- (instancetype)init {
    if(self = [super initWithWindowNibName:@"BrowsingWindow"]){
        [self setupButtons];
        [self setupDateFormat];
        
        // TODO: just setting it at 1 second for now always, using directory view.
        _zoomLevel = oneSecond;
        _videoController = [AppDelegate sharedInstance].videoController;
                
        _pendingRowOperations = [NSMutableDictionary dictionary];
        
        _placeholderStrings = [[NSMutableArray alloc] init];
        [self resetPlaceholderStrings];
        // This url is just placeholder. shouldn't actually ever be used. 
        _videoWriter = [[AVAssetWriter alloc] initWithURL:[NSURL SVDataDirectory] fileType:AVFileTypeMPEG4 error:nil];
    }
    return self;
}

- (void)windowDidLoad {
    NSLog(@"window did load");
    // This only gets called the first time you open the window. successive opens don't call this.
    [super windowDidLoad];
    // Want to try initiating with directory view.
    _directoryView = YES;
    
    // @@ What to do with this part?
    CGFloat screenHeight = self.window.screen.frame.size.height - [[[NSApplication sharedApplication] mainMenu] menuBarHeight];
    
    [self.window setStyleMask:NSBorderlessWindowMask];
    NSRect r = NSMakeRect(0, 0, 215, screenHeight);
    [self.window setFrame:r display:YES];
    // [self.window setLevel:CGShieldingWindowLevel()]; Use this if you want it to be top always. Even in front of menu.
    [self.window setLevel:NSStatusWindowLevel];
    
    //So this needs to be here because for whatever reason, setupStoreSelection and setupArrayController don't work the first time you open the window. hmmm...
    // This won't work here, because here there's no way to know what the search text is, unless you do a hacky static variable.
    // HACKY SOLUTION HERE WE COME.
    [self setupStoreSelection:_codeSearchString];
    [self setupArrayController:nil];
    [self setupMomentTableView];
    [self setupImageTableView];
    [self hideProgressIndicator];

    
    // Makes it so placeholder text changes.
    [NSTimer scheduledTimerWithTimeInterval:0.06
                                     target:self
                                   selector:@selector(updatePlaceholderText)
                                   userInfo:nil
                                    repeats:YES];
    
    // On scroll listener.
    NSView *contentsView = [_momentScrollView contentView];
    [contentsView setPostsBoundsChangedNotifications:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(boundsDidChange:)
                                                 name:NSViewBoundsDidChangeNotification
                                               object:contentsView];
    
    
}


#pragma mark Buttons

- (void)setupButtons {
    _isYellow = YES;
    _isBlue = YES;
    _isRed = YES;
    _isGreen = YES;
}

- (IBAction)filterYellow:(NSButton *)sender {
    if ([_yellowButton state] == NSOffState)
        _isYellow = true;
    else
        _isYellow = false;
    
    [[GAHelper sharedInstance] toggleYellow];
    [self fetchWithoutSelection];
}

- (IBAction)filterGreen:(NSButton *)sender {
    if ([_greenButton state] == NSOffState)
        _isGreen = true;
    else
        _isGreen = false;
    
    [[GAHelper sharedInstance] toggleGreen];
    [self fetchWithoutSelection];
}

- (IBAction)filterRed:(NSButton *)sender {
    if ([_redButton state] == NSOffState)
        _isRed = true;
    else
        _isRed = false;
    
    [[GAHelper sharedInstance] toggleRed];
    [self fetchWithoutSelection];
}

- (IBAction)filterBlue:(NSButton *)sender {
    if ([_blueButton state] == NSOffState)
        _isBlue = true;
    else
        _isBlue = false;
    
    [[GAHelper sharedInstance] toggleBlue];
    [self fetchWithoutSelection];
}


- (IBAction)performSearch:(NSSearchField *)sender {
    [self fetchWithoutSelection];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(GASearch) withObject:self afterDelay:5.0];
}

- (void)GASearch {
    [[GAHelper sharedInstance] search];
}

- (IBAction)switchTableView:(NSButton *)sender {
    
    if ([_momentTableView selectedRow] == -1 && [_imageTableView selectedRow] == -1)
        [self zoomWithoutSelection];
    else
        [self zoomWithSelection];
    
}

- (IBAction)closeWindow:(NSButton *)sender {
    [self toggleVisibility:nil];
}

#pragma mark TableView stuff
- (void)setupMomentTableView {
    // Removes borders of the table views.
    [self.momentTableView setIntercellSpacing:NSMakeSize(0, 0)];
    [self.momentScrollView setBorderType:NSNoBorder];
    
    // Synchronizes views
    [self.momentScrollView setSynchronizedScrollView:_imageScrollView];
    
    [self.momentTableView setBackgroundColor:[NSColor savantBackgroundGradientStart]];
    
    [self.momentTableView setRowHeight:BLOCK_CELL_HEIGHT];
    
    [self.momentTableView setBackgroundColor:[NSColor clearColor]];
    [[self.momentTableView enclosingScrollView] setDrawsBackground:NO];
    
    [_momentTableView setAllowsMultipleSelection:YES];
    
    [_momentTableView setTarget:self];
    [_momentTableView setAction:@selector(momentTableViewSelector)];
    [_momentTableView deselectAll:self];
    
    // [_momentTableView setDoubleAction:@selector(doubleClick)];
    // Double click seems way too easy to trigger accidently. 
}

- (void)doubleClick {
    // This seems to work. Sweet.
    if (_directoryView) {
        [_zoomButton setImage:[NSImage imageNamed:@"new_minus"]];
        _directoryView = NO;
        [_momentTableView setRowHeight:MOMENT_CELL_HEIGHT];
        [self fetchWithSelection:[_searchField stringValue]];
    }
}

- (void)setupImageTableView {
    [self.imageScrollView setBorderType:NSNoBorder];
    
    [self.imageScrollView setSynchronizedScrollView:_momentScrollView];
    
    [self.imageTableView setBackgroundColor:[NSColor savantBackgroundGradientStart]];
    
    //  IF YOU CHANGE THIS ALSO CHANGE ONE IN momentTableView
    [self.imageTableView setRowHeight:IMAGE_CELL_HEIGHT];
    
    [self.imageTableView setBackgroundColor:[NSColor clearColor]];
    [[self.imageTableView enclosingScrollView] setDrawsBackground:NO];
    
    [_imageTableView setTarget:self];
    [_imageTableView setAction:@selector(imageTableViewSelector)];
    [_imageTableView setDataSource:self];
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
    if (aTableView == _momentTableView) {
        return [(NSArray *)[_arrayController arrangedObjects] count];
    }
    else {
        return [(NSArray *)[_imageArrayController arrangedObjects] count];
    }
}

- (void)momentTableViewSelector {
    [self tableViewSelected:YES controller:_arrayController];
}

- (void)imageTableViewSelector {
    [self tableViewSelected:YES controller:_imageArrayController];
}



- (void)tableViewSelected:(BOOL)clicked controller:(NSArrayController *)controller {
    NSInteger row;
    NSTableView *view;
    if ([controller isEqualTo:_arrayController]) {
        view = _momentTableView;
        [[GAHelper sharedInstance] selectMoment];
    } else {
        view = _imageTableView;
        [[GAHelper sharedInstance] selectImage];
    }
    
    if (clicked)
        row = view.clickedRow;
    else {
        row = view.selectedRow;
    }
    
    if(row < [(NSArray *)controller.arrangedObjects count]){
        // [_videoController jumpToFrame:row array:[controller arrangedObjects]];
        _selectedMoment = [[controller arrangedObjects] objectAtIndex:row];
        [_videoController tableViewMomentWasSelected:_selectedMoment];
    }
}

-(void)boundsDidChange:(NSNotification *)notification {
    [self checkForFetchMore];
}

- (void)checkForFetchMore {
    
    // Make sure it doesn't fetch more than once at a time.
    if (_isFetching)
        return;
    
    CGRect visibleRect = _momentScrollView.contentView.visibleRect;
    NSRange range = [_momentTableView rowsInRect:visibleRect];
    
    // so check to see if the last item of arrayController is in here
    // Need the != 0 or there's an error where boudnsDidChange is called before arrayController is set.

    if ([(NSArray *)[_arrayController arrangedObjects] count] == range.location + range.length  && [(NSArray *)[_arrayController arrangedObjects] count] != 0) {
        if (!_directoryView)
            [self fetchMore:YES];
        else
            [self blockFetchMore:YES];
    }
    
    if (range.location == 0 && [(NSArray *)[_arrayController arrangedObjects] count] != 0) {
        if (!_directoryView)
            [self fetchMore:NO];
        else
            [self blockFetchMore:NO];
    }
    
}


// So need a second case here for, um, when it's supposed to be in directoryView.


- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSTableCellView *cellView = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    
    if(tableView == self.momentTableView) {
        
        // Think sometimes images load a bit slow, and arrayController gets reset before this line.
        if (self.arrayController.arrangedObjects == nil || [(NSArray *)self.arrayController.arrangedObjects count] == 0)
            return nil;
        
        NSTextField *textField = cellView.textField;
        NSMutableArray *array = [self.arrayController arrangedObjects];
        Moment *moment = array[row];
        
        if (!_directoryView) {
            if([tableColumn.identifier isEqualToString:@"application"]){
                [textField setBackgroundColor:[self getMomentColor:moment]];
            
            } else if([tableColumn.identifier isEqualToString:@"time"]){
                // So this is the only place where the text changes because of what kind of view it is...
                NSDate *time1 = moment.timestamp;
                NSString *timeString;
                if (row != [array count] - 1) {
                    Moment *moment2 =[array objectAtIndex:row+1];
                    NSDate *time2 = [moment2 timestamp];
                
                    if (self.zoomLevel < oneDay) {
                        timeString = [time1 isSameMinuteWithDate:time2] ? @"" : [self.minimalDateFormatter stringFromDate:time1];
                    } else {
                        timeString = [time1 isSameHourWithDate:time2] ? @"" : [self.   longerDateFormatter stringFromDate:time1];
                    }
                } else {
                    timeString = (self.zoomLevel < oneDay) ? [self.minimalDateFormatter stringFromDate:time1] : [self.longerDateFormatter stringFromDate:time1];
                }
            
                [textField setStringValue:timeString];
            }
        } else {
            MomentBlock *momentBlock = _momentBlockArray[row];
            if([tableColumn.identifier isEqualToString:@"application"]){
                [textField setBackgroundColor:[self getMomentColor:moment]];
                
            } else if([tableColumn.identifier isEqualToString:@"time"]){
                NSDate *startTime = momentBlock.startTime;
                // TODO: probably want to change these strings later. Eventually don't want to display date for all of  them, obviously.
                NSString *startTimeString = [self.minimalDateFormatter stringFromDate:startTime];
                NSString *dateString = [self.dateOnlyDateFormatter stringFromDate:startTime];
                NSString *descriptionString;
                if (!momentBlock.title || [momentBlock.title isEqualToString:@""])
                    descriptionString = [NSString stringWithFormat:@"%@\n%@\n%@", momentBlock.activeApplication, startTimeString, dateString];
                else
                    descriptionString = [NSString stringWithFormat:@"%@\n%@\n%@", momentBlock.title, startTimeString, dateString];
            
                [textField setStringValue:descriptionString];
            }
        }
        
    } else if(tableView == self.imageTableView) {
        // Think sometimes images load a bit slow, imageArrayController gets reset before this line. 
        if (self.imageArrayController.arrangedObjects == nil || [(NSArray *)self.imageArrayController.arrangedObjects count] == 0)
            return nil;
        
        NSImageView *imageView = cellView.imageView;
        // TODO: This fucks up? beyond index.
        // Think it has to do with adding more blocks because when you first open it, the blocks don't exist yet, but by the time you scroll down, it does.
        if (row >= [(NSArray *)self.arrayController.arrangedObjects count] )
            return nil;
        
        Moment *moment = self.imageArrayController.arrangedObjects[row];
        
        imageView.image = nil; // TODO: placeholder image
        
        NSMutableDictionary *pendingDict = self.pendingRowOperations;
        ImageBlockOperation *setCellImageOp = [ImageBlockOperation withImageBlock:^(NSImage *screenImage) {
            NSImage *thumb = [screenImage thumbnail];
            dispatch_async(dispatch_get_main_queue(), ^{
                imageView.image = thumb;
                [pendingDict removeObjectForKey:@(row)];
            });
        }];
        
        pendingDict[@(row)] = setCellImageOp;
        
        [moment executeImageOperationWithScreenImage:setCellImageOp];
    }
    
    return cellView;
}

// Custom row class to change highlight color
- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row
{
    SavantRowView *rowView = [tableView makeViewWithIdentifier:kSavantRowViewIdentifier owner:self];
    if(!rowView){
        rowView = [[SavantRowView alloc] initForTableView:tableView];
    }
    return rowView;
}

- (NSColor *)getMomentColor:(Moment *)moment {
    if ([moment.activeApplication isEqualToString:_BLUE_APP_NAME])
        return [NSColor savantBlueColor];
    else if ([moment.activeApplication isEqualToString:_RED_APP_NAME])
        return [NSColor savantRedColor];
    else if ([moment.activeApplication isEqualToString:_GREEN_APP_NAME])
        return [NSColor savantGreenColor];
    else
        return [NSColor savantYellowColor];
}
- (NSColor *)getMomentBlockColor:(MomentBlock *)momentBlock {
    if ([momentBlock.activeApplication isEqualToString:_BLUE_APP_NAME])
        return [NSColor savantBlueColor];
    else if ([momentBlock.activeApplication isEqualToString:_RED_APP_NAME])
        return [NSColor savantRedColor];
    else if ([momentBlock.activeApplication isEqualToString:_GREEN_APP_NAME])
        return [NSColor savantGreenColor];
    else
        return [NSColor savantYellowColor];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    if (![_videoController.playbackWindow isVisible])
        return;
    if(notification.object == self.momentTableView){
        [self tableViewSelected:NO controller:_arrayController];
    } else if (notification.object == self.imageTableView){
        [self tableViewSelected:NO controller:_imageArrayController];
    }
}

//- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
//    NSLog(@"Should select row %li", row);
//    if(tableView == self.momentTableView){
//        [self tableViewSelected:NO controller:_arrayController];
//    } else if (tableView == self.imageTableView){
//        [self tableViewSelected:NO controller:_imageArrayController];
//    }
//    return YES;
//}

-(void)tableView:(NSTableView *)tableView didRemoveRowView:(NSTableRowView *)rowView forRow:(NSInteger)row
{
    if(tableView == self.imageTableView){
        ImageBlockOperation *op = self.pendingRowOperations[@(row)];
        if(op){
            [op cancel];
            [self.pendingRowOperations removeObjectForKey:@(row)];
        }
    }
}

#pragma mark Dragging

- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard
{
    
    [pboard clearContents];
    [pboard declareTypes:[NSArray arrayWithObject:NSFilesPromisePboardType] owner:nil];
    NSArray *fileExtensionArray;
    
    if (aTableView == _momentTableView) {
        
        // In this case, multiple files, so should make a video from it?
        // TODO: Eventually want a way to check multiple png's I guess.
        if ([rowIndexes count] > 1) {
            
            fileExtensionArray = [NSArray arrayWithObject:@"mp4"];
        } else {

            fileExtensionArray = [NSArray arrayWithObject:@"png"];
        }
        
    } else if (aTableView == _imageTableView) {
        fileExtensionArray = [NSArray arrayWithObject:@"png"];
    }
    
    [pboard setPropertyList:fileExtensionArray forType:NSFilesPromisePboardType];
    return YES;
}

- (NSArray *)tableView:(NSTableView *)tableView namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination forDraggedRowsWithIndexes:(NSIndexSet *)indexSet {
    
    
    if (tableView == _momentTableView) {
        if ([indexSet count] > 1) {
            [DragAndDropHelper writeVideoToPanel:_videoWriter indexSet:indexSet array:[_arrayController arrangedObjects] progressIndicator:_progressIndicator exportLabel:_exportLabel];
            
        } else {
            [DragAndDropHelper writeImageToPanel:indexSet array:[_arrayController arrangedObjects]];
        }
    } else {
        [DragAndDropHelper writeImageToPanel:indexSet array:[_imageArrayController arrangedObjects]];
    }
    return [NSArray arrayWithObject:@"random-bs"];
}

-(void)menuNeedsUpdate:(NSMenu *)menu {
    
    NSLog(@"menu needs update");
    NSIndexSet *selectedRows;
    if (menu == _momentContextMenu)
        selectedRows = [_momentTableView selectedRowIndexes];
    else
        selectedRows = [_imageTableView selectedRowIndexes];
    
    if (!selectedRows || [selectedRows count] < 2) {
        [[menu itemAtIndex:0] setTitle:@"Export image"];
    } else {
        [[menu itemAtIndex:0] setTitle:@"Export video"];
    }
    
    if (menu == _momentContextMenu)
        [self setSelectedMomentFromClick:_momentTableView controller:_arrayController];
    else
        [self setSelectedMomentFromClick:_imageTableView controller:_imageArrayController];
    
    if (_directoryView) {
        [[menu itemAtIndex:1] setTitle:@"Zoom in" ];
    } else {
        [[menu itemAtIndex:1] setTitle:@"Zoom out" ];
    }
    
    if (_selectedMoment.url) {
        [[menu itemAtIndex:3] setTitle:@"Open in browser"];
    }
    else
        [[menu itemAtIndex:3] setTitle:@"Open application"];
    
}


- (void)hideProgressIndicator {
    [_exportLabel setHidden:YES];
    [_progressIndicator setHidden:YES];
}



#pragma mark Predicate Filtering

- (void)fetchMore:(BOOL)scrollDown {
    
    _isFetching = YES;
    
    // So, have to get more stuff, but with the condition that... it's after the last item.
   NSPredicate *fetchPredicate = [PredicateHelper getNewCombinedPredicate:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow timeGap:_zoomLevel string:[_searchField stringValue]];
    
    NSPredicate *scrollPredicate;
    NSArray *fetchedArray;
    if (scrollDown) {
        NSInteger numberOfObjects = [(NSArray *)[_arrayController arrangedObjects] count];
        Moment *lastMoment = [_arrayController arrangedObjects][numberOfObjects - 1];
        scrollPredicate = [NSPredicate predicateWithFormat:@"timestamp < %@", [lastMoment timestamp]];
        NSPredicate *combinedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:fetchPredicate, scrollPredicate, nil]];
        
        fetchedArray = [PredicateHelper fetchScreenshots:combinedPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:NO];
    } else {
        Moment *firstMoment = [_arrayController arrangedObjects][0];
        scrollPredicate = [NSPredicate predicateWithFormat:@"timestamp > %@", [firstMoment timestamp]];
        NSPredicate *combinedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:fetchPredicate, scrollPredicate, nil]];

        fetchedArray = [PredicateHelper fetchScreenshots:combinedPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:YES];
    }
    
    
    if (!fetchedArray || [fetchedArray count] == 0) {
        _isFetching = NO;
        return;
    }
    
    [_arrayController addObjects:fetchedArray];
    [_momentTableView deselectAll:self];
    [_momentTableView reloadData];
    
    // This happens if you get less than Fetch Limit, which is a problem because now you don't have multiple's of 50.
    if ([fetchedArray count] != FETCH_LIMIT && !scrollDown) {
        [self arrangeImageController:YES scrolledDown:scrollDown];
    } else {
        [self arrangeImageController:NO scrolledDown:scrollDown];
    }
    // how is this ever -1? Whatever. Select the previous moment.
    if (_selectedMoment && [[_arrayController arrangedObjects] indexOfObject:_selectedMoment] != NSNotFound) {
        [self selectPreviousMoment];
    }

    // Scrolls to previous location since when you scroll up, it puts stuff at front of array, and that messed up view.
    if (!scrollDown) {
        float y = ([fetchedArray count] + 1) * MOMENT_CELL_HEIGHT;
        [_momentTableView scrollPoint:CGPointMake(0, y)];
    }
    
    if (scrollDown)
        [[GAHelper sharedInstance] scrollDown];
    else
        [[GAHelper sharedInstance] scrollUp];
    
    // tells it it can fetch more again. 
    _isFetching = NO;
}

- (void)blockFetchMore:(BOOL)scrollDown {
    
    _isFetching = YES;
    
    // So, have to get more stuff, but with the condition that... it's after the last item.
    NSPredicate *fetchPredicate = [BlockPredicateHelper getNewCombinedPredicate:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow string:[_searchField stringValue]];
    
    NSPredicate *scrollPredicate;
    NSArray *fetchedArray;
    if (scrollDown) {
        NSInteger numberOfObjects = [_momentBlockArray count];
        MomentBlock *lastBlock = _momentBlockArray[numberOfObjects - 1];
        scrollPredicate = [NSPredicate predicateWithFormat:@"startTime < %@", [lastBlock startTime]];
        
        // Note: for this one order matters. needs to be scroll -> fetch, since fetch could be nil.
        NSPredicate *combinedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:scrollPredicate, fetchPredicate, nil]];
        
        
        fetchedArray = [BlockPredicateHelper fetchBlocks:combinedPredicate storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:NO];
    } else {
        MomentBlock *firstBlock = _momentBlockArray[0];
        scrollPredicate = [NSPredicate predicateWithFormat:@"startTime > %@", [firstBlock startTime]];
        NSPredicate *combinedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:scrollPredicate, fetchPredicate, nil]];
        
        fetchedArray = [BlockPredicateHelper fetchBlocks:combinedPredicate storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:YES];
    }
    
    
    if (!fetchedArray || [fetchedArray count] == 0) {
        _isFetching = NO;
        return;
    }
    
    [_momentBlockArray addObjectsFromArray:fetchedArray];
    // Probably should have this here.
    [self sortBlockArray];
    // So need a function here to add the new objects to the _arrayController.
    [self addBlocksToArrayController];
    [_momentTableView deselectAll:self];
    [_momentTableView reloadData];
    
    [self arrangeImageController:NO scrolledDown:scrollDown];
    // how is this ever -1? Whatever. Select the previous moment.
    if (_selectedMoment && [[_arrayController arrangedObjects] indexOfObject:_selectedMoment] != NSNotFound) {
        [self selectPreviousMoment];
    }
    
    // Scrolls to previous location since when you scroll up, it puts stuff at front of array, and that messed up view.
    if (!scrollDown) {
        float y = ([fetchedArray count] + 1) * BLOCK_CELL_HEIGHT;
        [_momentTableView scrollPoint:CGPointMake(0, y)];
    }
    
    if (scrollDown)
        [[GAHelper sharedInstance] scrollDown];
    else
        [[GAHelper sharedInstance] scrollUp];
    
    // tells it it can fetch more again.
    _isFetching = NO;
}

- (void)addBlocksToArrayController {
    // If more moments than blocks, return.
    if ([_momentBlockArray count] <= [(NSArray *)[_arrayController arrangedObjects] count])
        return;
    
    for (MomentBlock *momentBlock in _momentBlockArray) {
        Moment *moment = [BlockPredicateHelper getFirstMoment:momentBlock storeArray:@[_currShowingStore] string:[self getSearchText]];
        if (moment && [[_arrayController arrangedObjects] indexOfObject:moment] == NSNotFound) {
            [_arrayController addObject:moment];
        }
    }
}

- (void)sortBlockArray {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startTime"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [_momentBlockArray sortUsingDescriptors:sortDescriptors];
}


- (void)fetchWithoutSelection {
    // TODO: Think about how to handle zoom with search. Probably option to search "within" viewable.
    
    // HACK: tags override everything. one tag only.
    NSString *searchString = [_searchField stringValue] ;
    if([searchString hasPrefix:@"#"] && [searchString length] > 1){
        
        // We want to deal with moments, not blocks, so force us to be zoomed in
        if(_directoryView){
            [self zoomUIChanges];
        }
        
        NSString *tagName = [searchString substringFromIndex:1];
        MTag *tag = [MTag find:tagName context:[[DataManager sharedInstance] mainMOC]];
        if(tag){
            NSArray *moments = [tag.moments sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO]]];
            [self fetchWithRefresh:moments];
        } else {
            [self fetchWithRefresh:@[]];
        }
        return;
    }
    //TODO: set row size to block mode for tags, and do one image per tag
    
    
    if (!_directoryView) {
        NSPredicate *fetchPredicate = [PredicateHelper getNewCombinedPredicate:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow timeGap:_zoomLevel string:[_searchField stringValue]];
    
        NSArray *fetchedArray = [PredicateHelper fetchScreenshots:fetchPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:NO];
        [self fetchWithRefresh:fetchedArray];
    } else {
        
        // So for the blocks, timegap doesn't exist
        NSPredicate *fetchPredicate = [BlockPredicateHelper getNewCombinedPredicate:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow string:[_searchField stringValue]];
        
        [self blockFetchWithRefresh:[BlockPredicateHelper fetchBlocks:fetchPredicate storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:NO]];
    }
}

- (void)fetchWithSelection:(NSString *)string {
    
    // These might not be the same. It will always be the same if you use arranged objects though.
    
    // Yeha, you need this, since there's no other way to gaurantee selectedMoment exists.
    // TODO: What if there are multiple selected rows?
    NSInteger selectedRow = [_momentTableView selectedRow];
    _selectedMoment = [[_arrayController arrangedObjects] objectAtIndex:selectedRow];
    
    // So for this, need to know what is currently selected...
    // Filter predicate is what you're setting array filter, after and before predicates are what you are fetcing by.
    NSPredicate *afterPredicate = [PredicateHelper getNewCombinedPredicateWithSelection:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow timeGap:_zoomLevel string:string moment:_selectedMoment after:YES];
    NSPredicate *beforePredicate = [PredicateHelper getNewCombinedPredicateWithSelection:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow timeGap:_zoomLevel string:string moment:_selectedMoment after:NO];
    
    // So should first grab the stuff from the before.
    NSArray *beforeScreenshots = [PredicateHelper fetchScreenshots:beforePredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:NO];
    // Then grabs the after
    NSArray *afterScreenshots =[PredicateHelper fetchScreenshots:afterPredicate storeArray:@[_currShowingStore] withLimit:FETCH_LIMIT ascending:YES];
    
    // So then run a zoom array thing with these two arrays? And the original idea was that there would be a selcted moment?
    // Don't think I need a sort descriptor since those stay on.
    NSMutableArray *newArray = [NSMutableArray array];
    [newArray addObjectsFromArray:beforeScreenshots];
    [newArray addObjectsFromArray:afterScreenshots];
    // If it doesn't have the selected moment, add it.
    if ([beforeScreenshots indexOfObject:_selectedMoment] == NSNotFound)
        [newArray addObject:_selectedMoment];
    
    [self fetchWithRefresh:[newArray copy]];
    [self selectPreviousMoment];
    [self scrollToSelectedMoment];
}

- (void)blockFetchWithSelection:(NSString *)string {
    // These might not be the same. It will always be the same if you use arranged objects though.
//    NSInteger selectedRow = [_momentTableView selectedRow];
//    _selectedMoment = [[_arrayController arrangedObjects] objectAtIndex:selectedRow];
    
    MomentBlock *selectedBlock = [BlockPredicateHelper getBlockFromMoment:_selectedMoment storeArray:@[_currShowingStore]];
    
    // So for this, need to know what is currently selected...
    // Filter predicate is what you're setting array filter, after and before predicates are what you are fetcing by.
    NSPredicate *afterPredicate = [BlockPredicateHelper getNewCombinedPredicateWithSelection:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow timeGap:_zoomLevel string:string moment:selectedBlock after:YES];
    NSPredicate *beforePredicate = [BlockPredicateHelper getNewCombinedPredicateWithSelection:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow timeGap:_zoomLevel string:string moment:selectedBlock after:NO];
    
    
    // So should first grab the stuff from the before.
    NSArray *beforeBlocks = [BlockPredicateHelper fetchBlocks:beforePredicate storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:NO];
    // Then grabs the after
    NSArray *afterBlocks =[BlockPredicateHelper fetchBlocks:afterPredicate storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:YES];
    
    
    // Don't think I need a sort descriptor since those stay on.
    NSMutableArray *newArray = [NSMutableArray array];
    [newArray addObjectsFromArray:beforeBlocks];
    [newArray addObjectsFromArray:afterBlocks];
    // If it doesn't have the selected block, add it.
    if ([beforeBlocks indexOfObject:selectedBlock] == NSNotFound)
        [newArray addObject:selectedBlock];
    
    [self blockFetchWithRefresh:[newArray copy]];
    [self selectPreviousBlock:selectedBlock];
    [self scrollToPreviousBlock:selectedBlock];
}

- (void)fetchWithRefresh:(NSArray *)momentArray {
    
    [_arrayController removeAllObjects];
    [_arrayController addObjects:momentArray];
    [self arrangeImageController:YES scrolledDown:YES];
    [_momentTableView deselectAll:self];
}

- (void)blockFetchWithRefresh:(NSArray *)blockArray {
    [_momentBlockArray removeAllObjects];
    [_momentBlockArray addObjectsFromArray:blockArray];
    [self sortBlockArray];
    
    
    // So what do we have to do? grab the moments for the corresponding blocks, and then get the images, sure.
    NSMutableArray *momentArray = [NSMutableArray array];
    for (MomentBlock *momentBlock in blockArray) {
        Moment *moment = [BlockPredicateHelper getFirstMoment:momentBlock storeArray:@[_currShowingStore] string:[self getSearchText]];
        if (moment)
            [momentArray addObject:moment];
    }
    
    [self fetchWithRefresh:momentArray];
}

- (void)selectPreviousMoment {
    NSIndexSet *selectedIndex = [NSIndexSet indexSetWithIndex:[[_arrayController arrangedObjects] indexOfObject:_selectedMoment]];
    // TODO: So this actually is being selected, but not showing up when you're zooming from image views. meh, fix later, if ever.
    [_momentTableView selectRowIndexes:selectedIndex byExtendingSelection:NO];
}

- (void)scrollToSelectedMoment {
    NSArray *array = [_arrayController arrangedObjects];
    NSInteger newRow = [array indexOfObject:_selectedMoment];
    [_momentTableView scrollRowToVisible:newRow];
}

- (void)selectPreviousBlock:(MomentBlock *)selectedBlock {
    NSIndexSet *selectedIndex = [NSIndexSet indexSetWithIndex:[_momentBlockArray indexOfObject:selectedBlock]];
    [_momentTableView selectRowIndexes:selectedIndex byExtendingSelection:NO];
}

- (void)scrollToPreviousBlock:(MomentBlock *)selectedBlock {
    NSInteger newRow = [_momentBlockArray indexOfObject:selectedBlock];
    [_momentTableView scrollRowToVisible:newRow];
}

#pragma mark ImageArray

// TODO: make this more deterministic
- (void)arrangeImageController:(BOOL)refresh scrolledDown:(BOOL)scrolledDown{
    
    if (refresh) {
        [_imageArrayController removeAllObjects];
    }
    
    if (!_directoryView) {
        int i = 0;
        for (Moment *moment in [_arrayController arrangedObjects]) {
            i++;
        
            if (i % THUMB_RATIO != 1)
                continue;
            
            if (![[_imageArrayController arrangedObjects] containsObject:moment]) {
                [_imageArrayController addObject:moment];
            }
        }
    } else {
        for (Moment *moment in [_arrayController arrangedObjects]) {
            if (![[_imageArrayController arrangedObjects] containsObject:moment]) {
                [_imageArrayController addObject:moment];
            }
        }
    }
    
    if (refresh) {
        [_momentTableView reloadData];
        [_imageTableView reloadData];
        return;
    }
    
    // If it's scrolled up, insert at the top. noteNumberOfRowsChanged will refresh view if you insert at top.
    if (!scrolledDown) {
        NSRange theRange = NSMakeRange(0, 10);
        NSIndexSet* theIndexSet = [NSIndexSet indexSetWithIndexesInRange:theRange];
        [_imageTableView insertRowsAtIndexes:theIndexSet withAnimation:NSTableViewAnimationEffectNone];
    } else {
        [_imageTableView noteNumberOfRowsChanged];
    }
}

#pragma mark placeholder text

- (void)resetPlaceholderStrings {
    _placeholderStringIndex = 0;
    [_placeholderStrings removeAllObjects];
    [PlaceholderStringHelper setupPlaceholderArray:_placeholderStrings];
}


- (void)updatePlaceholderText {
    _placeholderStringIndex++;
    // This is so it doesn't crash if int gets too big. Needs to be 3* (plat + fade + brighten)
    if (_placeholderStringIndex > 3 * (PLATEAU_LENGTH + FADE_LENGTH + BRIGHTEN_LENGTH) )
        _placeholderStringIndex = _placeholderStringIndex % (3 * (PLATEAU_LENGTH + FADE_LENGTH + BRIGHTEN_LENGTH));
    
    [PlaceholderStringHelper changePlaceholderText:_placeholderStringIndex
                                  placeholderArray:_placeholderStrings
                                         textField:self.searchField];
}

#pragma mark misc setup

- (void)setupDateFormat {
    self.minimalDateFormatter = [[NSDateFormatter alloc] init];
    [self.minimalDateFormatter setDateFormat:@"h:mma"];
    self.longerDateFormatter = [[NSDateFormatter alloc] init];
    [self.longerDateFormatter setDateFormat:@"M/d ha"];
    self.dateOnlyDateFormatter = [[NSDateFormatter alloc] init];
    [self.dateOnlyDateFormatter setDateFormat:@"M/d"];
}

- (void)setupArrayController:(Moment *)moment {
    
    if (!moment)
        moment = [_currShowingDict objectForKey:@"moment"];
    
    _momentBlockArray = [NSMutableArray array];
    [_momentTableView setRowHeight:BLOCK_CELL_HEIGHT];
    
    // All I really have to do is put in new predicates making it after the found moment right?
    
    NSPredicate *fetchPredicate = [BlockPredicateHelper getNewCombinedPredicate:_isBlue isRed:_isRed isGreen:_isGreen isYellow:_isYellow string:nil];
    NSPredicate *timePredicate = [NSPredicate predicateWithFormat:@"startTime <= %@", moment.timestamp];
    // Order here matters since fetchPredicate can be nil.
    NSCompoundPredicate *combinedPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:timePredicate, fetchPredicate, nil]];
    
    NSArray *array = [BlockPredicateHelper fetchBlocks:combinedPredicate storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:NO];
    [_momentBlockArray addObjectsFromArray:array];
    
    // TODO: this is wrong I think. FIgure out why the dates are set wrong, will probably fix it.
    // get 10 from after too?
    NSPredicate *timePredicate2 = [NSPredicate predicateWithFormat:@"startTime > %@", moment.timestamp];
    // Order here matters since fetchPredicate can be nil.
    NSCompoundPredicate *combinedPredicate2 = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:timePredicate2, fetchPredicate, nil]];
    NSArray *array2 = [BlockPredicateHelper fetchBlocks:combinedPredicate2 storeArray:@[_currShowingStore] withLimit:BLOCK_FETCH_LIMIT ascending:NO];
    [_momentBlockArray addObjectsFromArray:array2];
    // so moment block theoretically shouldn't have to be sorted...
    // Meh, try quick fix of sorting array
    NSSortDescriptor *blockSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startTime" ascending:MOST_RECENT];
    [_momentBlockArray sortUsingDescriptors:@[blockSortDescriptor]];

    
    
    [self addBlocksToArrayController];
    [self arrangeImageController:NO scrolledDown:YES];
    // Sort Descriptors stays after adding objects. Filter predicates dont.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timestamp" ascending:NO];
    [_arrayController setSortDescriptors:@[sortDescriptor]];
    [_arrayController setAutomaticallyRearrangesObjects:YES];
    [_arrayController setClearsFilterPredicateOnInsertion:NO];
    [_imageArrayController setSortDescriptors:@[sortDescriptor]];
    [_imageArrayController setAutomaticallyRearrangesObjects:YES];
    [_imageArrayController setClearsFilterPredicateOnInsertion:NO];
    
    [self setupApplicationCountArray];
    [_momentTableView reloadData];
    [_imageTableView reloadData];
    
    // This selects and scrolls to the relevant block
    MomentBlock *selectedBlock = [BlockPredicateHelper getBlockFromMoment:moment storeArray:@[_currShowingStore]];
    [self selectPreviousBlock:selectedBlock];
    [self scrollToPreviousBlock:selectedBlock];
}

#pragma mark Menu Items

- (IBAction)exportImageTableView:(NSMenuItem *)sender {
    NSInteger clickedRow = [_imageTableView clickedRow];
    [self closeVideoWindow];
    [DragAndDropHelper writeImageToPanel:[NSIndexSet indexSetWithIndex:clickedRow] array:[_imageArrayController arrangedObjects]];
}

- (IBAction)exportMomentTableView:(NSMenuItem *)sender {
    NSIndexSet *selectedRows = [_momentTableView selectedRowIndexes];
    if (!selectedRows || [selectedRows count] < 2) {
        NSInteger selectedRow = [_momentTableView clickedRow];
        [self closeVideoWindow];
        [DragAndDropHelper writeImageToPanel:[NSIndexSet indexSetWithIndex:selectedRow] array:[_arrayController arrangedObjects]];
    } else {
        [self closeVideoWindow];
        [DragAndDropHelper writeVideoToPanel:_videoWriter indexSet:selectedRows array:[_arrayController arrangedObjects] progressIndicator:_progressIndicator exportLabel:_exportLabel];
    }
}

- (IBAction)zoomInImageTableView:(NSMenuItem *)sender {
    [self zoomWithSelection];
}

- (IBAction)zoomInMomentTableView:(NSMenuItem *)sender {
    [self zoomWithSelection];
}

- (IBAction)openApplicationMomentTableView:(NSMenuItem *)sender {
    [VideoWindowController openApp:_selectedMoment];
}

- (IBAction)openApplicationImageTableView:(NSMenuItem *)sender {
    [VideoWindowController openApp:_selectedMoment];
}

- (IBAction)switchFromSearch:(NSMenuItem *)sender {
    // If no moment is selected, just go with the first one.
    NSInteger selectedRow = [_momentTableView selectedRow];
    if (selectedRow == (int)NSNotFound)
        selectedRow = 0;
    _selectedMoment = [[_arrayController arrangedObjects] objectAtIndex:selectedRow];
    
    // I guess also clear the serach bar
    [_searchField setStringValue:@""];
    
    // Then just get the stuff immediately before and after those I guess...
    // Would this be both for blocks and for moments? Sure.
    if (_directoryView)
        [self blockFetchWithSelection:nil];
    else
        [self fetchWithSelection:nil];
    
    
}


- (void)setSelectedMomentFromClick:(NSTableView *)tableView controller:(NSArrayController *)controller {
    Moment *moment;
    NSInteger clickedRow = [tableView clickedRow];
    moment = [controller arrangedObjects][clickedRow];
    _selectedMoment = moment;
}

- (void)zoomWithoutSelection {
    
    [self zoomUIChanges];
    [self fetchWithoutSelection];
}

- (void)zoomWithSelection {
    
    // Following is superflous for now, since everytime you right click it's already called, and if you zoom in by pressing the button, there already is a selection.
//    if (tableView != nil) {
//        [self setSelectedMomentFromClick:tableView controller:controller];
//    }
    [self zoomUIChanges];
    
    // So these are actually "reversed", since the _directoryView thing already changed.
    if (_directoryView)
        [self blockFetchWithSelection:[_searchField stringValue]];
    else
        [self fetchWithSelection:[_searchField stringValue]];
}

- (void)zoomUIChanges {
    // All the zooms go through here, so good place for GA helper.
    [[GAHelper sharedInstance] toggleZoom];
    if (_directoryView) {
        _directoryView = NO;
        [_zoomButton setImage:[NSImage imageNamed:@"new_minus"]];
        [_momentTableView setRowHeight:MOMENT_CELL_HEIGHT];
    } else {
        _directoryView = YES;
        [_zoomButton setImage:[NSImage imageNamed:@"new_plus"]];
        [_momentTableView setRowHeight:BLOCK_CELL_HEIGHT];
    }
    
}





#pragma mark Application Count Array

- (void)setupApplicationCountArray {
    _applicationCountArray = [[NSMutableArray alloc] init];
    // 4 signifies more than 3, therefor need an "etc" one.
    NSArray *tempCountArray = [PredicateHelper fetchFilterApplications:[AppDelegate sharedInstance].filterString storeArray:@[_currShowingStore] limit:4];
    if (tempCountArray)
        [_applicationCountArray addObjectsFromArray:tempCountArray];
    
    NSInteger numberOfUniqueApps = [_applicationCountArray count];
    
    switch (numberOfUniqueApps) {
        case 4:
            [self setupBlueFilter];
            [self setupRedFilter];
            [self setupGreenFilter];
            [self setupYellowFilter];
            break;
            
        case 3:
            [self setupBlueFilter];
            [self setupRedFilter];
            [self setupGreenFilter];
            [self invalidateYellowFilter];
            break;
        case 2:
            [self setupBlueFilter];
            [self setupRedFilter];
            [self invalidateGreenFilter];
            [self invalidateYellowFilter];
            break;
        case 1:
            [self setupBlueFilter];
            [self invalidateRedFilter];
            [self invalidateGreenFilter];
            [self invalidateYellowFilter];
            break;
        default:
            [self invalidateBlueFilter];
            [self invalidateRedFilter];
            [self invalidateGreenFilter];
            [self invalidateYellowFilter];
            break;
    }
}



- (void)setupBlueFilter {
    [_blueButton setImage:((ApplicationCount *)_applicationCountArray[0]).icon];
    [_blueButton setAlternateImage:[((ApplicationCount *)_applicationCountArray[0]).icon imageTintedWithColor:[NSColor savantTintColor]]];
    [_blueButton setHidden:NO];
    [_blueButton setEnabled:YES];
    [_blueTag setHidden:NO];
    _BLUE_APP_NAME = ((ApplicationCount *)_applicationCountArray[0]).name;
}

- (void)setupRedFilter {
    [_redButton setImage:((ApplicationCount *)_applicationCountArray[1]).icon];
    [_redButton setAlternateImage:[((ApplicationCount *)_applicationCountArray[1]).icon imageTintedWithColor:[NSColor savantTintColor]]];
    [_redButton setHidden:NO];
    [_redButton setEnabled:YES];
    [_redTag setHidden:NO];
    _RED_APP_NAME = ((ApplicationCount *)_applicationCountArray[1]).name;
}

- (void)setupGreenFilter {
    [_greenButton setImage:((ApplicationCount *)_applicationCountArray[2]).icon];
    [_greenButton setAlternateImage:[((ApplicationCount *)_applicationCountArray[2]).icon imageTintedWithColor:[NSColor savantTintColor]]];
    [_greenButton setHidden:NO];
    [_greenButton setEnabled:YES];
    [_greenTag setHidden:NO];
    _GREEN_APP_NAME = ((ApplicationCount *)_applicationCountArray[2]).name;
}

- (void)setupYellowFilter {
    [_yellowButton setHidden:NO];
    [_yellowButton setEnabled:YES];
    [_yellowTag setHidden:NO];
}

- (void)invalidateBlueFilter {
    [_blueButton setEnabled:NO];
    [_blueButton setHidden:YES];
    [_blueTag setHidden:YES];
}

- (void)invalidateRedFilter {
    [_redButton setEnabled:NO];
    [_redButton setHidden:YES];
    [_redTag setHidden:YES];
}

- (void)invalidateGreenFilter {
    [_greenButton setEnabled:NO];
    [_greenButton setHidden:YES];
    [_greenTag setHidden:YES];
}


- (void)invalidateYellowFilter {
    [_yellowButton setEnabled:NO];
    [_yellowButton setHidden:YES];
    [_yellowTag setHidden:YES];
}

#pragma mark misc.

- (NSString *)getSearchText {
    NSMutableDictionary *dict = [SharedPredicateHelper parseSearchField:[_searchField stringValue]];
    if (!dict || ![dict objectForKey:TEXT_FILTER])
        return nil;
    else
        return [dict objectForKey:TEXT_FILTER];
    
}

- (void)clearData {
    [[[DataManager sharedInstance] imageCache] removeAllObjects];
    _directoryView = YES;
    [_zoomButton setImage:[NSImage imageNamed:@"new_plus"]];
    [_momentBlockArray removeAllObjects];
    [_arrayController removeAllObjects];
    [_imageArrayController removeAllObjects];
    [_applicationCountArray removeAllObjects];
}

- (BOOL)setupStoreSelection:(NSString *)string {
    // so actually, everything should go in here about whether the string is nil or not...
    NSArray *storeDbDictionary = [NSArray array];
    storeDbDictionary = [AlternateDatabaseHelper fetchSortedFromEveryDb:string];
    
    if ([storeDbDictionary count] == 0)
        return NO;
    _currDbArray = storeDbDictionary;
    // so storeDbDictionary is a presorted array of dict that has relevant moment and store and url and etc.
    [_dbChooser removeAllItems];
    for (NSDictionary *dict in storeDbDictionary) {
        [_dbChooser addItemWithTitle:[dict objectForKey:@"name"]];
    }
    
    _currShowingDict = storeDbDictionary[0];
    // so this chooses to show the most recent store.
    _currShowingStore = [_currShowingDict objectForKey:@"store"];
    // TODO: probably fix this so it doesn't just auto choose the first element.
    [_dbChooser selectItemWithTitle:[_currShowingDict objectForKey:@"name"]];
    return YES;
    // So this probably works. Let's test it, and then we probably have to fix it for grabbing with strings.
}

- (IBAction)chooseDb:(NSPopUpButton *)sender {
    NSString *selectedStore = [[sender selectedItem] title];
    // So what do we actually want it to do if we select a new DB? If we select the same one, I guess nothing. If we select a new one... the normal stuff, but with the new one I guess.
    
    for (NSDictionary *dict in _currDbArray) {
        if ([[dict objectForKey:@"name"] isEqualToString:selectedStore]) {
            if ([[dict objectForKey:@"store"] isEqualTo:_currShowingStore])
                return;
            else {
                _currShowingStore = [dict objectForKey:@"store"];
                _currShowingDict = dict;
                // Then have to refresh it?
                [self clearData];
                [self setupArrayController:nil];
            }
        }
    }
}


- (void)toggleVisibility:(NSString *)string {

#ifdef DEBUG
    NSLog(@"this is the debug version");
#endif
    
    //## TEST
    AppDelegate *ad = [AppDelegate sharedInstance];
    ad.videoController.currShowingMoment = nil;
    
    if ( ![self isActive] ) {
        #ifndef DEBUG
            BITSystemProfile *bsp = [BITSystemProfile sharedSystemProfile];
            [bsp startUsage];
        #endif
        _codeSearchString = string;
        // This is here so that if you can't find the string, nothing happens.
        // TODO: Think about how to exit this more gracefully.
        if (![self setupStoreSelection:string])
            return;
        
        // TODO: searchfield is not really relevant for this usecase, but change this if that should be the first thing.
        // [self.searchField becomeFirstResponder];
        [self.window makeFirstResponder:_momentTableView];
        
        [self setupArrayController:nil];
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [self showWindow:self];
        
        [[AppDelegate sharedInstance].visibilityItem setTitle:@"Hide history"];
        [[GAHelper sharedInstance] startBrowse];
        [[GAHelper sharedInstance] viewBrowsingWindow];
    } else {
        #ifndef DEBUG
            BITSystemProfile *bsp = [BITSystemProfile sharedSystemProfile];
            [bsp stopUsage];
        #endif
        if ([self isWindowLoaded]){
            [self.window orderOut:self];
            [self clearData];
            [[GAHelper sharedInstance] endBrowse];
        }
        [[AppDelegate sharedInstance].visibilityItem setTitle:@"Show history"];
        [self.videoController dismissPlayer];
    }
}

- (void)closeVideoWindow {
    [self.videoController dismissPlayer];
}

- (BOOL)isActive {
    return [self isWindowLoaded] && [self.window isVisible];
}


- (void)deleteSelection
{
    // TODO: should handle image table view somehow? I guess would need to translate selection
    // so that they can tell what they're deleting
    NSArray *srcArray = self.directoryView ? self.momentBlockArray : self.arrayController.arrangedObjects;
    NSArray *selectedObjects = [srcArray objectsAtIndexes:self.momentTableView.selectedRowIndexes];
    NSMutableSet *momentsToDelete;
    if(self.directoryView){
        momentsToDelete = [NSMutableSet set];
        for (MomentBlock *block in selectedObjects) {
            [momentsToDelete unionSet:block.moment];
        }
    } else {
        momentsToDelete = [NSMutableSet setWithArray:selectedObjects];
    }
    
    [[DataManager sharedInstance] deleteMoments:momentsToDelete];
    
    [self refresh];
}

- (void)refresh
{
    [self fetchWithoutSelection];
}

- (void)keyDown:(NSEvent *)theEvent {
    switch (theEvent.keyCode) {
        case KEY_LEFT_ARROW:
            if ([_videoController.playbackWindow isVisible])
                [_videoController goBackOneFrame];
            break;
            
        case KEY_RIGHT_ARROW:
            if ([_videoController.playbackWindow isVisible])
                [_videoController goForwardOneFrame];
            break;
            
        default:
            break;
    }
}



@end
