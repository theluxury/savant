//
//  SafariHelper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-07.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SafariHelper.h"

@implementation SafariHelper

+ (NSArray *)webAreaPath
{
    return @[NSAccessibilityGroupRole, NSAccessibilityGroupRole, NSAccessibilityGroupRole, NSAccessibilityScrollAreaRole, NSAccessibilityWebAreaRole];
}

@end
