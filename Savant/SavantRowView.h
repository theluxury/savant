//
//  SavantRowView.h
//  Savant
//
//  Created by Paul Musgrave on 2014-09-22.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSString * const kSavantRowViewIdentifier;


@interface SavantRowView : NSTableRowView

- (instancetype)initForTableView:(NSTableView *)tableView;

@end
