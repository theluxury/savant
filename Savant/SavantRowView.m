//
//  SavantRowView.m
//  Savant
//
//  Created by Paul Musgrave on 2014-09-22.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SavantRowView.h"
#import "NSColor+PrettyColors.h"

NSString * const kSavantRowViewIdentifier = @"SavantRowView";


@interface SavantRowView ()

@property (nonatomic, weak) NSTableView *tableView;

@end


@implementation SavantRowView

- (instancetype)initForTableView:(NSTableView *)tableView {
    self = [super initWithFrame:NSZeroRect];
    if (self) {
        _tableView = tableView;
        self.identifier = kSavantRowViewIdentifier;
    }
    return self;
}

- (void)drawSelectionInRect:(NSRect)dirtyRect
{
    // Is this really the only way to do this?
    if (self.tableView == [[self window] firstResponder] && [[self window] isMainWindow] && [[self window] isKeyWindow]){
        [[NSColor savantFocusedSelectionColor] setFill];
    } else {
        [[NSColor clearColor] setFill];
    }
    
    NSRectFillUsingOperation(dirtyRect, NSCompositeSourceOver);
}

@end
