//
//  SavantTableView.h
//  Savant
//
//  Created by Mark Wang on 10/16/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SavantTableView : NSTableView <NSDraggingSource>

@end
