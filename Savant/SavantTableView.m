//
//  SavantTableView.m
//  Savant
//
//  Created by Mark Wang on 10/16/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SavantTableView.h"
#import "AppDelegate.h"

@implementation SavantTableView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (NSDragOperation)draggingSession:(NSDraggingSession *)session sourceOperationMaskForDraggingContext:(NSDraggingContext)context {
    if (context == NSDraggingContextOutsideApplication) {
        // Close the video window so that you can drag stuff there
        AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
        [d.browsingController closeVideoWindow];
        return NSDragOperationCopy;
    }
    else
        return NSDragOperationNone;
}



@end
