//
//  SreenRecorder.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Moment.h"

//## not sure if this should be the delegate, or just pass through
@interface ScreenRecorder : NSObject

- (void)startCapture;
- (void)stopCapture;

- (void)resetState;

// This probably shouldn't need to be public, but for now its the designated place
// to operate on the recording context
@property dispatch_queue_t recordingQueue;

@property (nonatomic) BOOL isRecording;
@property (nonatomic, strong) Moment *lastSignificantMoment;

@end
