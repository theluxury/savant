//
//  SreenRecorder.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "ScreenRecorder.h"

#import "DataManager.h"
#import "MetadataSnapshot.h"
#import "SVAccessibilityScraper.h"
#import "AppDelegate.h"

#import "NSImage+SaveImageRef.h"
#import "NSString+StringUtil.h"
#import "NSString+Contains.h"
#import "ApplicationCount.h"

#import "NSURL+SavantLocations.h"



//##test (should be ~600)
// ### and must be even for now :P
static const int ROLLING_BUFFER_LENGTH = 300; // in frames

static const int MAX_DIFF_CHAIN_LENGTH = 5;

static const int kMetadataBufferLength = 4;

@interface StackFrame : NSObject
@property (nonatomic, strong) Moment *inst;
@property (nonatomic) void *data;
@property (nonatomic) int idx;
+(instancetype)frameWithBuffer:(IOSurfaceRef)buf instant:(Moment *)inst index:(int)idx;
@end


@implementation StackFrame {}
+(instancetype)frameWithBuffer:(IOSurfaceRef)buf instant:(Moment *)inst index:(int)idx {
    StackFrame *sf = [[StackFrame alloc] init];
    
    size_t slen = IOSurfaceGetAllocSize(buf);
    sf.data = malloc(slen);
    IOSurfaceLock(buf, kIOSurfaceLockReadOnly, 0);
    memcpy(sf.data, IOSurfaceGetBaseAddress(buf), slen);
    IOSurfaceUnlock(buf, kIOSurfaceLockReadOnly, 0);
    
    sf.inst = inst; sf.idx = idx;
    return sf;
}
- (void)dealloc { free(self.data); }

- (NSString *)description { return [NSString stringWithFormat:@"idx: %d; %@", self.idx, self.inst.timestamp]; }
@end



@interface ScreenRecorder ()

@property (nonatomic) CGDisplayStreamRef displayStream;

@property (nonatomic, strong) NSMutableArray *frameStack;
@property (nonatomic, strong) StackFrame *curKeyFrame;
@property size_t curFrameSize;
// Since we can't preserve the IOSurfaces, store the prototypical info for creating images
@property size_t curFrameWidth;
@property size_t curFrameHeight;
@property size_t curFrameBytesPerRow;

@property NSInteger subsampleCounter;

@property (nonatomic, strong) NSTimer *noChangeTimer;
@property (nonatomic, strong) NSDate *lastFrameDate;
@property BOOL lastFrameWasSkipped;

@property CGDirectDisplayID display;

@property (nonatomic, strong) NSManagedObjectContext *moc;

@property NSInteger frameCount;

@end

@implementation ScreenRecorder

- (instancetype) init
{
    if(self = [super init]){
        self.recordingQueue = dispatch_queue_create("com.savant.recordingQueue", DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(self.recordingQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0));
        
        self.moc = [DataManager sharedInstance].recordingMOC;
        
        self.frameStack = [NSMutableArray arrayWithCapacity:MAX_DIFF_CHAIN_LENGTH];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Moment"];
        [request setIncludesSubentities:NO];
        self.frameCount = [_moc countForFetchRequest:request error:nil];
        
        self.subsampleCounter = -1;
    }
    return self;
}

- (void)createDisplayStream
{
    if(!self.displayStream){
        
        self.display = CGMainDisplayID();
        
        // If the lag is still noticable, try the runloop way
        // Also, possibly try a different pixel format
        self.displayStream = CGDisplayStreamCreateWithDispatchQueue(self.display,
                                               CGDisplayPixelsWide(self.display),
                                               CGDisplayPixelsHigh(self.display),
                                               'BGRA',
                                               (__bridge CFDictionaryRef)(@{
                                                    // It seems like we end up with approximately up to 1 frame of latency,
                                                    // so we subsample (our real framerate is ~1/sec)
                                                                            
// In the stress test configuration, we capture every .1 secs and don't subsample
// This helps to debug e.g. timing issues
#ifdef STRESS_TEST
                                                    (__bridge NSString*)kCGDisplayStreamMinimumFrameTime : @0.1,
#else
                                                    (__bridge NSString*)kCGDisplayStreamMinimumFrameTime : @0.333,
#endif
                                                    // This prevents late frames and seems to slightly reduce latency in general, though not sure if it's just causing us to drop those frames
                                                    (__bridge NSString*)kCGDisplayStreamQueueDepth : @1,
                                               }),
                                               self.recordingQueue,
        ^(CGDisplayStreamFrameStatus status, uint64_t displayTime, IOSurfaceRef frameSurface, CGDisplayStreamUpdateRef updateRef) {
            
#ifndef STRESS_TEST
            self.subsampleCounter++;
            if(self.subsampleCounter %= 3) return;
#endif
 
            [[SVAccessibilityScraper sharedInstance] refreshFocusedWindow];
            if([self shouldSkipCurrentFrame]){
                self.lastFrameWasSkipped = YES;
                return;
            }
            self.lastFrameWasSkipped = NO;
            
            // Latency
//            static mach_timebase_info_data_t sTimebase;
//            if (sTimebase.denom == 0){
//                (void)mach_timebase_info(&sTimebase);
//            }
//            NSLog(@"delta t: %f", ((mach_absolute_time() - displayTime)*sTimebase.numer/sTimebase.denom)/1000000000.0 );
            
            
            NSDate *sampleDate = [NSDate date];
            self.lastFrameDate = sampleDate;
            
            
            Moment *moment = [Moment insertInManagedObjectContext:self.moc];
//            NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
//            NSPersistentStore *mainStore = [psc persistentStoreForURL:[NSURL SVPersistentStoreUrl]];
//            [self.moc assignObject:moment toPersistentStore:mainStore];
            moment.timestamp = sampleDate;
            
            NSRunningApplication *activeApp = [[NSWorkspace sharedWorkspace] frontmostApplication];
            moment.activeApplication = activeApp.localizedName;
            
            moment.timeGap = @([[DataManager sharedInstance] checkTimegapArray:moment]);
            [[DataManager sharedInstance] incrementCount:moment];
            // TODO gotta do appCount here so there's no lag.
            
            self.lastSignificantMoment = moment;
            
            
            // ## hopefully that's right?
            size_t bufLen = IOSurfaceGetAllocSize(frameSurface);
//            size_t bufLen = CVPixelBufferGetDataSize(imageBuffer);
            
            // If the buffer length changes (i.e. the screen resolution changes), we need to
            // reset our keyframe and stack. Once we handle multiple displays better this shouldn't come up
            if (bufLen != self.curFrameSize){
                [self resetKeyStack];
            }
            
            if (self.curKeyFrame == nil) {
                self.curKeyFrame = [StackFrame frameWithBuffer:frameSurface instant:moment index:0];
                self.curFrameSize = bufLen;
                
                self.curFrameBytesPerRow = IOSurfaceGetBytesPerRow(frameSurface);
                self.curFrameWidth = IOSurfaceGetWidth(frameSurface);
                self.curFrameHeight = IOSurfaceGetHeight(frameSurface);
                
                [moment setMetadata:[MetadataSnapshot takeSnapshotWithRefreshRect:[[NSScreen mainScreen] frame]]];
                
                
                CGImageRef im = [self createFrameImageWithData:IOSurfaceGetBaseAddress(frameSurface)];
                moment.imageURL = [NSImage saveImageRef:im idx:self.frameCount];
                CGImageRelease(im);
                
                moment.diffDepthValue = 0;
            } else {
                int popped = 0;
                // ## this is stupid
                while (_frameStack.count > 0 && [_frameStack.lastObject idx] == MAX_DIFF_CHAIN_LENGTH){
                    [_frameStack removeLastObject];
                    popped++;
                }
                
                if (_frameStack.count == 0) {
                    [_frameStack addObject:_curKeyFrame];
                }
                
                StackFrame *baseFrame = _frameStack.lastObject;
                
                // TODO: ideally don't make multiple copies of the stack frame, or at least don't copy the data,
                // though need separate indexes (unless just use chain length instead)
                [_frameStack removeLastObject];
                [_frameStack addObject:[StackFrame frameWithBuffer:frameSurface instant:moment index:baseFrame.idx+1]];
                
                while(popped-- > 0){
                    [_frameStack addObject:[StackFrame frameWithBuffer:frameSurface instant:moment index:baseFrame.idx+1]];
                }
                
                moment.baseMoment = baseFrame.inst;
                moment.diffDepthValue = baseFrame.inst.diffDepthValue + 1;
                if(moment.diffDepthValue > MAX_DIFF_CHAIN_LENGTH){
                    NSLog(@"!! Diff chain is too long !!");
                }
                
                IOSurfaceLock(frameSurface, kIOSurfaceLockReadOnly, 0);
                uint32_t *baseAddress = IOSurfaceGetBaseAddress(frameSurface);
                uint32_t *compBaseAddress = baseFrame.data;
                uint32_t *diffDat = malloc(bufLen);
                size_t plen = bufLen/4;
                
                //@@ what?
//                CGSize s = CVImageBufferGetEncodedSize(imageBuffer);
//                size_t w = s.width, h = s.height;
//                size_t xmax=0,ymax=0,xmin=w,ymin=h;
                
                size_t w = IOSurfaceGetWidth(frameSurface), h = IOSurfaceGetHeight(frameSurface);
                size_t xmax=0,ymax=0,xmin=w,ymin=h;
                
                
                for (size_t i=0; i<plen; i++) {
                    if(baseAddress[i] == compBaseAddress[i]){
                        diffDat[i] = 0x00000000; // In particular, alpha is zero so we can compose
                    } else {
                        diffDat[i] = baseAddress[i];
                        //## screw it
                        xmin = MIN(xmin, i % w);
                        xmax = MAX(xmax, i % w);
                        ymin = MIN(ymin, i / w);
                        ymax = MAX(ymax, i / w);
                    }
                }
                IOSurfaceUnlock(frameSurface, kIOSurfaceLockReadOnly, 0);
                
                // ## could make this all more efficient by only doing one iteration over the image data,
                // ## and by only creating an image of the needed part
                CGImageRef im = [self createFrameImageWithData:diffDat];
                free(diffDat);
                
                NSInteger dw = xmax >= xmin ? xmax-xmin+1 : 0;
                NSInteger dh = ymax >= ymin ? ymax-ymin+1 : 0;
                CGRect diffBox = CGRectMake(xmin, ymin, dw, dh);
                CGImageRef croppedIm = CGImageCreateWithImageInRect(im, diffBox);
                // NSLog(@"%@", NSStringFromRect(NSRectFromCGRect(diffBox)));
                
                [moment setMetadata:[MetadataSnapshot takeSnapshotWithRefreshRect:diffBox]];
                
                if(croppedIm != nil){
                    moment.diffURL = [NSImage saveImageRef:croppedIm idx:self.frameCount prefix:@"diff-"];
                    // @@ goddam flipped images. this is correct. ideally clean up
                    moment.diffBox = NSMakeRect(xmin, h - ymax -1, dw, dh);
                    CGImageRelease(croppedIm);
                } else {
                    moment.isNullDiff = @YES;
                }
                CGImageRelease(im);
            }
       
            [[DataManager sharedInstance] updateMomentBlock:moment];
            self.frameCount++;
   
            [self.moc save:nil];
        });
    }
}

- (BOOL)shouldSkipCurrentFrame
{
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    NSRunningApplication *activeApp = [[NSWorkspace sharedWorkspace] frontmostApplication];
    
    // skip if savant is visible or if it's turned off.
    // TODO: This is not a good longterm solution.
    if((d.browsingController.isWindowLoaded && [d.browsingController.window isVisible])
       || !d.screenRecorder.isRecording
       || [activeApp.bundleIdentifier isEqualToString:[[NSBundle mainBundle] bundleIdentifier]]){
        return YES;
    }
    
    
    // Then, determine if you're in blacklist or whitelist mode.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isWhitelist = NO;
    if ([defaults objectForKey:FILTER_BLACKLIST_SORT] && [[defaults objectForKey:FILTER_BLACKLIST_SORT] isEqualToString:FILTER_WHITELIST]) {
        isWhitelist = YES;
    }
    
    if (!isWhitelist)
        return [self checkForBlacklist];
    else
        return [self checkForWhitelist];
    
    

    
    
}

- (BOOL)checkForBlacklist {
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
     NSRunningApplication *activeApp = [[NSWorkspace sharedWorkspace] frontmostApplication];
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // skip if activeApp is in blacklist
    if ([defaults objectForKey:blacklist]) {
        for (NSString *string in [defaults objectForKey:blacklist]) {
            if ([activeApp.localizedName contains:string]){
                return YES;
            }
        }
    }
    // Skip if url is blacklisted
    if(d.shouldRecordAccessibilityText){
        SVAccessibilityScraper *sc = [SVAccessibilityScraper sharedInstance];
        NSString *url = [sc getUrl];
        
        if (url && [defaults objectForKey:URL_BLACKLIST]) {
            for (NSString *string in [defaults objectForKey:URL_BLACKLIST]) {
                if ([url contains:string]){
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (BOOL)checkForWhitelist {
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    NSRunningApplication *activeApp = [[NSWorkspace sharedWorkspace] frontmostApplication];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // For this, should check URL first, actually.
    // Skip if url is blacklisted
    if(d.shouldRecordAccessibilityText){
        SVAccessibilityScraper *sc = [SVAccessibilityScraper sharedInstance];
        NSString *url = [sc getUrl];
        
        if (url && ![url isEqualToString:@""] && [defaults objectForKey:URL_WHITELIST]) {
            for (NSString *string in [defaults objectForKey:URL_WHITELIST]) {
                if ([url contains:string]){
                    return NO;
                }
            }
        }
    }
    
    // Is app whitelisted?
    if ([defaults objectForKey:APP_WHITELIST]) {
        for (NSString *string in [defaults objectForKey:APP_WHITELIST]) {
            if ([activeApp.localizedName contains:string]){
                return NO;
            }
        }
    }
    
    // else skip that stuff.
    return YES;
}


- (CGImageRef)createFrameImageWithData:(void *)data
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(data, self.curFrameWidth, self.curFrameHeight, 8,
                                                 self.curFrameBytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    return quartzImage;
}


- (void)startCapture
{
    _isRecording = YES;
    if(!self.displayStream) {
        [self createDisplayStream];
    }
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //## check if this is also slow
    CGDisplayStreamStart(self.displayStream);
    
    [[DataManager sharedInstance] beginSaving];
    [[DataManager sharedInstance] beginEnforcingStorageConstraints];
    
    NSLog(@"started running");
//    });
    
    if(!self.noChangeTimer){
        self.noChangeTimer = [NSTimer timerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(checkForNoChange)
                                               userInfo:nil
                                                repeats:YES];
        [self.noChangeTimer setFireDate:[NSDate date]];
        [[NSRunLoop mainRunLoop] addTimer:self.noChangeTimer forMode:NSDefaultRunLoopMode];
    }
}

- (void)stopCapture
{
    _isRecording = NO;
    // Invalidate on the main thread so it's removed from the right run loop
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.noChangeTimer invalidate];
        self.noChangeTimer = nil;
    });
    
//    dispatch_async(self.recordingQueue, ^{
    CGDisplayStreamStop(self.displayStream);
    self.displayStream = nil;
    
    [[DataManager sharedInstance] stopSaving];
    [[DataManager sharedInstance] stopEnforcingStorageConstraints];

    NSLog(@"finished running");
//    });
}

// It seems like we should get callbacks from the display stream on null diffs
// (there's even a status parameter to cover this) but it seems like we don't, so we do this instead
- (void)checkForNoChange
{
    // Perform this on the recording queue where all the other recording is happening
    // to avoid concurrency issues
    dispatch_async(self.recordingQueue, ^{
        // This can potentially miss single null frames, but oh well
        // (the reason we do -2 at all is because otherwise jitter could cause us to insert extra ones)
        // Check if the last significant moment is nil in case we reset the state
        if(!self.lastFrameWasSkipped && [self.lastFrameDate timeIntervalSinceNow] < -2 && self.lastSignificantMoment){
            NSDate *nullDiffTime = [self.lastFrameDate dateByAddingTimeInterval:1];
            self.lastFrameDate = nullDiffTime;
            
            DataManager *dm = [DataManager sharedInstance];
            Moment *nullDiffMoment = [Moment insertInManagedObjectContext:self.moc];
//            NSPersistentStoreCoordinator *psc = [[DataManager sharedInstance] persistentStoreCoordinator];
//            NSPersistentStore *mainStore = [psc persistentStoreForURL:[NSURL SVPersistentStoreUrl]];
//            [self.moc assignObject:nullDiffMoment toPersistentStore:mainStore];
            [nullDiffMoment setAsNullDiffFromMoment:self.lastSignificantMoment];
            nullDiffMoment.timestamp = nullDiffTime;
            nullDiffMoment.timeGap = @([dm checkTimegapArray:nullDiffMoment]);
            [dm updateMomentBlock:nullDiffMoment];
            [self.moc save:nil];
        }
    });
}


- (void)resetKeyStack
{
    self.curKeyFrame = nil;
    self.frameStack = [NSMutableArray array];
}

- (void)resetState
{
    [self resetKeyStack];
    self.lastSignificantMoment = nil;
}




@end
