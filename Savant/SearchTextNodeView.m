//
//  SearchTextNodeView.m
//  Savant
//
//  Created by Mark Wang on 10/2/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SearchTextNodeView.h"

@implementation SearchTextNodeView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    NSRect newRect = NSMakeRect(dirtyRect.origin.x, dirtyRect.origin.y, dirtyRect.size.width, dirtyRect.size.height);
    
    NSBezierPath *textViewSurround = [NSBezierPath bezierPathWithRoundedRect:newRect xRadius:2 yRadius:2];
    [textViewSurround setLineWidth:1.5];
    [[NSColor redColor] set];
    [textViewSurround stroke];
    [self setAlphaValue:0.5];
}

@end
