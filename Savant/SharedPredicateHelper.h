//
//  SharedPredicateHelper.h
//  Savant
//
//  Created by Mark Wang on 11/4/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SharedPredicateHelper : NSObject

extern NSString * const APP_FILTER;
extern NSString * const TEXT_FILTER;
extern NSString * const URL_FILTER;
extern BOOL const EARLIEST;
extern BOOL const MOST_RECENT;

+ (NSPredicate *)setButtonPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow;

+ (NSMutableDictionary *)parseSearchField:(NSString *)string;

+ (NSPredicate *)searchTextNodes:(NSString *)string entity:(NSString *)entity;

+ (NSPredicate *)searchUrl:(NSString *)string entity:(NSString *)entity;

+ (NSPredicate *)setSearchPredicate:(NSString *)string entity:(NSString *)entity;

+ (NSArray *)fetch:(NSString *)entity storeArray:(NSArray *)storeArray predicate:(NSPredicate *)predicate withLimit:(NSInteger)limit ascending:(BOOL)ascending;

@end
