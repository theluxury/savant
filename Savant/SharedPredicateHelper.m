//
//  SharedPredicateHelper.m
//  Savant
//
//  Created by Mark Wang on 11/4/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SharedPredicateHelper.h"
#import "SVBrowsingWindowController.h"
#import "AppDelegate.h"
#import "DataManager.h"

@implementation SharedPredicateHelper

NSString * const APP_FILTER = @"appFilter";
NSString * const TEXT_FILTER = @"textFilter";
NSString * const URL_FILTER = @"urlFilter";
BOOL const EARLIEST = YES;
BOOL const MOST_RECENT = NO;

+ (NSPredicate *)setButtonPredicate:(BOOL)isBlue isRed:(BOOL)isRed isGreen:(BOOL)isGreen isYellow:(BOOL)isYellow {
    
    SVBrowsingWindowController *browsingController = [[AppDelegate sharedInstance] browsingController];
    
    NSPredicate *bluePredicate = nil;
    NSPredicate *redPredicate = nil;
    NSPredicate *greenPredicate = nil;
    NSPredicate *yellowPredicate = nil;
    
    // Need to wrap booleans in NSNumber to put them in array
    NSMutableArray *predicates = [[NSMutableArray alloc] init];
    
    // There might be an issue where COLOR_APP_NAME is not defined sometimes, but if it's not the isColor button should be disabled, so that should be okay...
    if (!isBlue) {
        bluePredicate = [NSPredicate predicateWithFormat: @"activeApplication !=[cd] %@", browsingController.BLUE_APP_NAME];
        [predicates addObject:bluePredicate];
    }
    
    
    if (!isRed) {
        redPredicate = [NSPredicate predicateWithFormat:@"activeApplication !=[cd] %@", browsingController.RED_APP_NAME];
        [predicates addObject:redPredicate];
    }
    
    
    if (!isGreen) {
        // This might not work?
        greenPredicate = [NSPredicate predicateWithFormat:@"activeApplication !=[cd] %@", browsingController.GREEN_APP_NAME];
        [predicates addObject:greenPredicate];
    }
    
    
    if (!isYellow) {
        // IN[cd] doesnt't work, so these need correct capitalization.
        NSSet *set = [NSSet setWithObjects:browsingController.BLUE_APP_NAME , browsingController.RED_APP_NAME,browsingController.GREEN_APP_NAME, nil];
        yellowPredicate = [NSPredicate predicateWithFormat:@"activeApplication IN %@", set];
        [predicates addObject:yellowPredicate];
    }
    
    // If 0, return nil. If 1, just return that one. If more than one, return a compound. Adding 0 to signify when it ends.
    
    if ([predicates count] == 0)
        return nil;
    else if ([predicates count] == 1)
        return [predicates objectAtIndex:0];
    else {
        return [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
}

+(NSMutableDictionary *)parseSearchField:(NSString *)string {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    // Remove the unucessary white spaces
    NSString* result = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL hasAt = NO;
    BOOL hasTilde = NO;
    
    // So first to see if it begins with an @
    if ([result hasPrefix:@"@"] && ![result isEqualToString:@"@"]) {
        hasAt = YES;
    } else {
        hasAt = NO;
    }
    
    // So ability to search for URL's. Let's go with tilde for now.
    if ([result hasPrefix:@"~"] && ![result isEqualToString:@"!"]) {
        hasTilde = YES;
    } else {
        hasTilde = NO;
    }
    
    if (hasAt) {
        return [self parseSearch:APP_FILTER result:result];
    } else if (hasTilde) {
        return [self parseSearch:URL_FILTER result:result];
    } else {
        if([result isEqualToString:@""] || [result isEqualToString:@"@"] || result == nil){
            return nil;
        } else {
            [dict setObject:result forKey:TEXT_FILTER];
            return dict;
        }
    }
}

+ (NSMutableDictionary *)parseSearch:(NSString *)type result:(NSString *)result {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    // This is the case that there is an at. So first, get the word after tge bang.
    NSString *appFilter = [[result componentsSeparatedByString:@" "] objectAtIndex:0];
    // Now remove the @
    appFilter = [appFilter substringFromIndex:1];
    
    
    // Then get the resulting string...
    // TODO: make it so if you search for multiple words it handles it intelligently.
    NSString *textFilter = [result substringFromIndex:[appFilter length] + 1];
    // remove extra whitespaces
    textFilter = [textFilter stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([textFilter isEqualToString:@""] || [textFilter isEqualToString:@" "] || textFilter == nil){
        [dict setObject:appFilter forKey:type];
        return dict;
    } else {
        [dict setObject:appFilter forKey:type];
        [dict setObject:textFilter forKey:TEXT_FILTER];
        return dict;
    }
}

+ (NSPredicate *)setSearchPredicate:(NSString *)string entity:(NSString *)entity{
    NSMutableDictionary *dict = [SharedPredicateHelper parseSearchField:string];
    if (dict == nil)
        return nil;
    
    // So, um, 5 cases? just text, or just app, or app + text, or just url, or url + text.
    // Application predicates should go first, then URL, then text, for performance.
    
    else if ([dict objectForKey:URL_FILTER] && [dict objectForKey:TEXT_FILTER]) {
        // So this has url and text. so need 3 predicates: 1 for the url, 1 to make sure its in a browser, and 1 for the text. Eh, actually, doesn't really seem to matter.
        NSPredicate *applicationPredicate = [NSPredicate predicateWithFormat:@"activeApplication contains[cd] %@ OR activeApplication contains[cd] %@", @"chrome", @"safari"];
        NSPredicate *urlPredicate = [self searchUrl:[dict objectForKey:URL_FILTER] entity:entity];
        NSPredicate *textPredicate = [self searchTextNodes:[dict objectForKey:TEXT_FILTER] entity:entity];
        
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:applicationPredicate, urlPredicate, textPredicate, nil]];
    } else if ([dict objectForKey:URL_FILTER] && ![dict objectForKey:TEXT_FILTER]) {
        NSPredicate *applicationPredicate = [NSPredicate predicateWithFormat:@"activeApplication contains[cd] %@ OR activeApplication contains[cd] %@", @"chrome", @"safari"];
        NSPredicate *urlPredicate = [self searchUrl:[dict objectForKey:URL_FILTER] entity:entity];
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:applicationPredicate, urlPredicate, nil]];
    } else if ([dict objectForKey:APP_FILTER] && [dict objectForKey:TEXT_FILTER]) {
        NSPredicate *applicationPredicate = [NSPredicate predicateWithFormat:@"activeApplication contains[cd] %@", [dict objectForKey:APP_FILTER]];
        NSPredicate *textPredicate = [self searchTextNodes:[dict objectForKey:TEXT_FILTER] entity:entity];
        return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:applicationPredicate, textPredicate, nil]];
    } else if ([dict objectForKey:APP_FILTER] && ![dict objectForKey:TEXT_FILTER]) {
        NSPredicate *applicationPredicate = [NSPredicate predicateWithFormat:@"activeApplication contains[cd] %@", [dict objectForKey:APP_FILTER]];
        return applicationPredicate;
    } else {
        return [self searchTextNodes:[dict objectForKey:TEXT_FILTER] entity:entity];
    }
}


+ (NSPredicate *)searchTextNodes:(NSString *)string entity:(NSString *)entity {
    if ([entity isEqualToString:@"moment"])
        return [NSPredicate predicateWithFormat:@"any textState.textNodes.text contains[cd] %@", string];
    else
        // need subquery here.
        return [NSPredicate predicateWithFormat:@"SUBQUERY(moment, $moment, $moment.textState.textNodes.text contains[cd] %@ ).@count > 0", string];
}

+ (NSPredicate *)searchUrl:(NSString *)string entity:(NSString *)entity {
    if ([entity isEqualToString:@"moment"])
        return [NSPredicate predicateWithFormat:@"url contains[cd] %@", string];
    else
        return [NSPredicate predicateWithFormat:@"any moment.url contains[cd] %@", string];
}

+ (NSArray *)fetch:(NSString *)entity storeArray:(NSArray *)storeArray predicate:(NSPredicate *)predicate withLimit:(NSInteger)limit ascending:(BOOL)ascending {
    NSManagedObjectContext *moc = [[DataManager sharedInstance] mainMOC];
    NSEntityDescription *entityDescription;
    NSSortDescriptor *sortDescriptor;
    
    if ([entity isEqualToString:@"moment"]) {
        entityDescription = [NSEntityDescription
                             entityForName:@"Moment" inManagedObjectContext:moc];
        sortDescriptor = [[NSSortDescriptor alloc]
                          initWithKey:@"timestamp" ascending:ascending];
    } else {
        entityDescription = [NSEntityDescription
                             entityForName:@"MomentBlock" inManagedObjectContext:moc];
        sortDescriptor = [[NSSortDescriptor alloc]
                          initWithKey:@"startTime" ascending:ascending];
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    
    // In case predicate is nil, just return the earliest/latest stuff.
    // So before this could be nil, but now it's never nil because there's alwaays isNullDiff == nil...
    if (predicate)
        [request setPredicate:predicate];
    
    [request setSortDescriptors:@[sortDescriptor]];
    [request setFetchLimit:limit];
    
    [request setAffectedStores:storeArray];

    NSError *error;
    
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (error) NSLog(@"Got an error trying to fetch core data %@", error);
    
    // This here returns the most recent if it couldnt find anything. But not sure I actually want to do that. 
//    if (!array || [array count] == 0) {
//        [request setPredicate:[NSPredicate predicateWithFormat:@"isNullDiff == nil"]];
//        if ([entity isEqualToString:@"moment"]) {
//            sortDescriptor = [[NSSortDescriptor alloc]
//                              initWithKey:@"timestamp" ascending:MOST_RECENT];
//        } else {
//            sortDescriptor = [[NSSortDescriptor alloc]
//                              initWithKey:@"startTime" ascending:MOST_RECENT];
//        }
//        
//        [request setSortDescriptors:@[sortDescriptor]];
//        array = [moc executeFetchRequest:request error:&error];
//    }
    
    return array;
    
}

@end
