//
//  SpecialApplicationHelper.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-07.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXWrapper.h"

@interface SpecialAppHelper : NSObject

// # not entirely sure whether these should take wrappers or whether they should be axwrapper subclasses,
// but that seems to be a bit awkward
+ (NSString *)getURLForWindow:(AXWrapper *)window;
+ (NSString *)getTitleForWindow:(AXWrapper *)window;

@end
