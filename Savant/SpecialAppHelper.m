//
//  SpecialApplicationHelper.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-07.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "SpecialAppHelper.h"

@implementation SpecialAppHelper

// Default implementations

+ (NSString *)getURLForWindow:(AXWrapper *)window
{
    return nil;
}

// Applications should only return a window title here if it is meaningful and distinct from the app name
+ (NSString *)getTitleForWindow:(AXWrapper *)window
{
    return nil;
}

@end
