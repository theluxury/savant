//
//  StatusItemPopover.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-10.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface StatusItemPopover : NSPopover

- (void)present;

@end
