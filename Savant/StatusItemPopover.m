//
//  StatusItemPopover.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-10.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "StatusItemPopover.h"
#import "AppDelegate.h"

@implementation StatusItemPopover

- (void)present
{
    NSStatusItem *statusItem = [AppDelegate sharedInstance].statusItem;
    [self showRelativeToRect:CGRectZero ofView:statusItem.view preferredEdge:NSMinYEdge];
}

@end
