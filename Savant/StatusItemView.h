//
//  StatusItemView.h
//  Savant
//
//  Created by Paul Musgrave on 2014-11-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface StatusItemView : NSView <NSMenuDelegate>


@property (nonatomic, weak) NSStatusItem *statusItem;

@property (nonatomic) BOOL isActive;
@property (nonatomic, strong) NSImage *activeImage;
@property (nonatomic, strong) NSImage *inactiveImage;
//@property (nonatomic, strong) NSImage *invertedImage;

@property (nonatomic) BOOL isShowingMenu;
@property (nonatomic, strong) NSMenu *menu;


@end
