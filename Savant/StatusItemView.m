//
//  StatusItemView.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-21.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "StatusItemView.h"


@interface StatusItemView ()

@property (nonatomic, strong) NSImage *curImage;

@end


@implementation StatusItemView

// I guess since we define a setter, and its not a primitive type (..?), we have to synthesize?
@synthesize menu = _menu;


- (void)drawRect:(NSRect)dirtyRect
{
    // For now never highlight
    [self.statusItem drawStatusBarBackgroundInRect:dirtyRect withHighlight:NO];
    
    // Not sure why exacly this is necessary, but it seems to be; not sure its exactly right
    NSSize iconSize = [self.curImage size];
    NSRect bounds = self.bounds;
    CGFloat iconX = roundf((NSWidth(bounds) - iconSize.width) / 2);
    CGFloat iconY = roundf((NSHeight(bounds) - iconSize.height) / 2);
    NSPoint iconPoint = NSMakePoint(iconX, iconY);
    
	[self.curImage drawAtPoint:iconPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
    return YES;
}


- (void)setActiveImage:(NSImage *)activeImage
{
    _activeImage = activeImage;
    [self updateImage];
}
- (void)setInactiveImage:(NSImage *)inactiveImage
{
    _inactiveImage = inactiveImage;
    [self updateImage];
}
- (void)updateImage
{
    self.curImage = self.isActive ? self.activeImage : self.inactiveImage;
    [self setNeedsDisplay:YES];
}

- (void)setIsActive:(BOOL)isActive
{
    _isActive = isActive;
    [self updateImage];
}

- (void)setMenu:(NSMenu *)menu
{
    _menu = menu;
    [menu setDelegate:self];
}

- (void)mouseDown:(NSEvent *)event {
    [self.statusItem popUpStatusItemMenu:self.menu];
    [self setNeedsDisplay:YES];
}
- (void)rightMouseDown:(NSEvent *)event {
    [self mouseDown:event];
}

- (void)menuWillOpen:(NSMenu *)menu {
    self.isShowingMenu = YES;
    [self setNeedsDisplay:YES];
}

- (void)menuDidClose:(NSMenu *)menu {
    self.isShowingMenu = NO;
    [self setNeedsDisplay:YES];
}


@end
