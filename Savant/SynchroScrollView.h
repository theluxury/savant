//
//  SynchroScrollView.h
//  Savant
//
//  Created by Mark Wang on 8/3/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SynchroScrollView : NSScrollView
 
@property (nonatomic, weak) NSScrollView* synchronizedScrollView; //unretained

- (void)stopSynchronizing;
- (void)synchronizedViewContentBoundsDidChange:(NSNotification *)notification;

@end
