//
//  TaggingPopdown.m
//  Savant
//
//  Created by Paul Musgrave on 2014-11-10.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "TaggingViewController.h"
#import "StatusItemPopover.h"
#import "AppDelegate.h"
#import "MTag.h"

#import "ScreenRecorder.h"
#import "DataManager.h"

@interface TaggingViewController ()

@property (nonatomic, strong) StatusItemPopover *popover;
@property (nonatomic, weak) IBOutlet NSTextField *tagTextField;

@property (nonatomic, strong) Moment *moment;

- (IBAction)handleEnter:(id)sender;

@end


// Somewhat hacky way of ensuring there's only one popdown
static BOOL isShowing = NO;


@implementation TaggingViewController

+ (void)presentTaggingViewForMoment:(Moment *)moment
{
    if(isShowing) return;
    
    TaggingViewController *vc = [[TaggingViewController alloc] initWithNibName:@"TaggingPopover" bundle:[NSBundle mainBundle]];
    vc.popover = [[StatusItemPopover alloc] init];
    vc.popover.contentViewController = vc;
    vc.moment = moment;
    [vc.popover present];
    
    isShowing = YES;
}

- (void)addTags:(NSArray *)tags
{
    // ## this is kinda lame :P (doing it for the core data concurrency)
    ScreenRecorder *sr = [AppDelegate sharedInstance].screenRecorder;
    dispatch_async(sr.recordingQueue, ^{
        NSManagedObjectContext *moc = [DataManager sharedInstance].recordingMOC;
        for (NSString *tagName in tags) {
            MTag *tag = [MTag findOrCreate:tagName context:moc];
            [tag addMomentsObject:self.moment];
        }
    });
}
- (void)dismiss{
    [self.popover close];
    self.popover = nil;
    isShowing = NO;
}

// Not sure exactly what the behaviour should be here; this is a bit weird since
// it will e.g. allow tags containing #, but close enough for now
// Not even sure if we should require including #, but might want to allow e.g. annotations later
- (NSArray *)parseTags:(NSString *)tagString
{
    NSArray *components = [tagString componentsSeparatedByString:@" "];
    NSMutableArray *tagNames = [NSMutableArray array];
    for (NSString *s in components) {
        NSString *t = [s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([t characterAtIndex:0] == '#'){
            [tagNames addObject:[t substringFromIndex:1]];
        }
    }
    return tagNames;
}

- (IBAction)handleEnter:(id)sender {
    NSString *tagString = self.tagTextField.stringValue;
    NSArray *tags = [self parseTags:tagString];
    if(tags.count > 0){
        [self addTags:tags];
        [self dismiss];
    } else {
        [self dismiss];
    }
}



@end
