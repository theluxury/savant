//
//  VideoWindow.h
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TextNode.h"
#import "Moment.h"

@interface VideoWindow : NSWindow

@property (weak) IBOutlet NSImageView *tmphackimageview;

-(NSRect)getVideoRectFromAXElement:(TextNode *)element;

-(NSRect)convertToVideoWindowCoordFromScreenCoord:(NSRect)rect ratio:(float)ratio;

-(float)getScreenToWindowRatio;

-(void)setupSearchTextNodes:(Moment *)moment string:(NSString *)string;

- (void)setController:(id)controller;

- (void)removeAllSubview;

- (void)removeSearchTextNodeView;

@end
