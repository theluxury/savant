//
//  VideoWindow.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "VideoWindow.h"
#import "MiniWindowController.h"
#import "Moment.h"
#import "TextState.h"
#import "NSString+StringUtil.h"
#import "AppDelegate.h"
#import "HighlightView.h"
#import "AXElementTextView.h"
#import "GAHelper.h"
#import "SearchTextNodeView.h"
#import "NSString+Contains.h"

@interface VideoWindow ()

@property (nonatomic, strong) AXElementTextView *textView;
@property (nonatomic, strong) HighlightView *highlightView;
@property (nonatomic, strong) NSMutableArray *searchTextNodesArray;
@property (nonatomic) NSPoint dragStartPoint;
@property (nonatomic) CGRect slightlyBiggerRect;
@property (nonatomic) BOOL isDragging;
@property (nonatomic) BOOL isMainWindow;


@end

@implementation VideoWindow

- (void)mouseEntered:(NSEvent *)theEvent
{
    // so [theEvent userData] gives me dictioanry with key = {{x,y},{width,height}};
    CGRect rect = [self getRectFromEvent:theEvent];
    
    
    // Should give it a little bit of slack...
    if (_highlightView)
        _slightlyBiggerRect = [self makeSlightyBigger:[_highlightView frame]];
    
    // So check if see if this is inside one already being shown. If it is, ignore?
    if (_highlightView && CGRectContainsRect(_slightlyBiggerRect, rect)) {
        return;
    }
    
    if (_isDragging)
        return;
    
    [self removeAllSubview];
    _highlightView = [[HighlightView alloc] initWithFrame:rect];
    [[self contentView] addSubview:_highlightView];
}

- (void)mouseExited:(NSEvent *)theEvent
{
    
    if (_isDragging)
        return;
    
    if (_highlightView) {
        CGRect rect = [self getRectFromEvent:theEvent];
        CGRect otherSightlyBiggerRect = [self makeSlightyBigger:rect];
    
        // So if you exited from a nested rectangle, return.
        if (CGRectContainsRect(_slightlyBiggerRect, rect) && !CGRectEqualToRect(_slightlyBiggerRect, otherSightlyBiggerRect))
            return;
    }
    [self removeAllSubview];
}

- (CGRect)getRectFromEvent: (NSEvent *)event {
    NSDictionary *dict = [event userData];
    NSRect rect = [[dict objectForKey:@"rect"] convertToRect];
    return [self flipUpsideDown:rect];
}

- (CGRect)makeSlightyBigger:(CGRect)rect {
    return NSMakeRect(rect.origin.x - 2, rect.origin.y - 2, rect.size.width + 4, rect.size.height + 4);
}

- (void)mouseDown:(NSEvent *)theEvent {
    
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    if (_isMainWindow)
        [d.videoController.popover close];
    else
        [d.miniWindowController.popover close];
    
    _dragStartPoint = [theEvent locationInWindow];
    _isDragging = YES;
    
    NSPoint windowClickedPoint =[theEvent locationInWindow];
    
    // Alright, so track to see what tracking areas they're in. Then, find the biggest one, and swallow then others. Then, run through TA again, and do the text thing we do for drag. Sure.
    
    NSMutableArray *clickedTrackingAreas = [NSMutableArray array];
    for (NSTrackingArea *ta in [[self contentView] trackingAreas]) {
        NSRect nsrect = [self flipUpsideDown:[[[ta userInfo] objectForKey:@"rect"] convertToRect]];
        if (NSPointInRect(windowClickedPoint, nsrect)) {
            [clickedTrackingAreas addObject:ta];
        }
    }
    
    // Then weed out the smaller/nested ones. Just going by size, should be fine?
    NSTrackingArea *biggestTrackingArea;
    if ([clickedTrackingAreas count] == 0)
        return;
    else if ([clickedTrackingAreas count] == 1)
        biggestTrackingArea = clickedTrackingAreas[0];
    else {
        biggestTrackingArea = clickedTrackingAreas[0];
        for (NSTrackingArea *ta in clickedTrackingAreas) {
            CGRect origRect = [biggestTrackingArea rect];
            CGRect rect = [ta rect];
            if ((rect.size.width * rect.size.height) > (origRect.size.width * origRect.size.height))
                biggestTrackingArea = ta;
        }
    }
    
    // Then go through trackingAreas again to see which ones are in there...
    CGRect biggestRect = [self flipUpsideDown:[[[biggestTrackingArea userInfo] objectForKey:@"rect"] convertToRect]];
    
    // Little bit of slack.
    biggestRect = [self makeSlightyBigger:biggestRect];
    
    NSMutableArray *finalTrackingAreas = [NSMutableArray array];
    for (NSTrackingArea *ta in [[self contentView] trackingAreas]) {
        NSRect nsrect = [self flipUpsideDown:[[[ta userInfo] objectForKey:@"rect"] convertToRect]];
        if (CGRectContainsRect(biggestRect, nsrect)) {
            [finalTrackingAreas addObject:ta];
        }
    }
    
    [self makePopupFromArray:finalTrackingAreas];
    
    [[GAHelper sharedInstance] selectTextNode];
}

- (void)mouseUp:(NSEvent *)theEvent {
    [self removeAllSubview];
    _isDragging = NO;
    
}

- (void)mouseDragged:(NSEvent *)theEvent {
    NSPoint dragPoint = [theEvent locationInWindow];
    
    CGFloat xmin = MIN(_dragStartPoint.x, dragPoint.x);
    CGFloat xmax = MAX(_dragStartPoint.x, dragPoint.x);
    CGFloat ymin = MIN(_dragStartPoint.y, dragPoint.y);
    CGFloat ymax = MAX(_dragStartPoint.y, dragPoint.y);
    
    NSRect dragrect = NSMakeRect(xmin, ymin, xmax - xmin, ymax - ymin);
    
    
    if (!_highlightView) {
        _highlightView = [[HighlightView alloc] init];
        [[self contentView] addSubview:_highlightView];
    }
    
    [_highlightView setFrame:dragrect];
    NSMutableArray *stringArray = [NSMutableArray array];
    
    for (NSTrackingArea *ta in [[self contentView] trackingAreas]) {
        NSRect rect = [[[ta userInfo] objectForKey:@"rect"] convertToRect];
        rect = [self flipUpsideDown:rect];
        
        if (CGRectIntersectsRect(rect, dragrect)) {
            [stringArray addObject:ta];
        }
    }
    
    [self makePopupFromArray:stringArray];
}

- (void)makePopupFromArray:(NSMutableArray *)array {
    // Now sort the strings by order they should be added in?
    [array sortUsingComparator: ^NSComparisonResult(NSTrackingArea *ta1, NSTrackingArea *ta2) {
        
        NSRect rect1 = [[[ta1 userInfo] objectForKey:@"rect"] convertToRect];
        NSRect rect2 = [[[ta2 userInfo] objectForKey:@"rect"] convertToRect];
        
        
        // Probably want to give the y a little bit of slack, since the bolded could be slightly bigger, messed up readning over.
        if ([self centerOfRect:rect1].y + 3< [self centerOfRect:rect2].y)
            return NSOrderedAscending;
        else if ([self centerOfRect:rect1].y > [self centerOfRect:rect2].y + 3)
            return NSOrderedDescending;
        else {
            if ([self centerOfRect:rect1].x < [self centerOfRect:rect2].x)
                return NSOrderedAscending;
            else return NSOrderedDescending;
        }
    }];
    
    
    NSString *combinedString = @"";
    if (array) {
        for (NSTrackingArea *ta in array) {
            // If the y values differ, want to add in a space instead? So how to check that? Hmmm... I guess see if the y values are more than, let's say 5 pixels apart?
            if ([array indexOfObject:ta] == 0)
                combinedString = [combinedString stringByAppendingString:[NSString stringWithFormat:@"%@",[[ta userInfo] objectForKey:@"text"]]];
            else {
                NSRect rect1 = [[[[array objectAtIndex:[array indexOfObject:ta] - 1] userInfo] objectForKey:@"rect"] convertToRect];
                rect1 = [self flipUpsideDown:rect1];
                NSRect rect2 = [[[ta userInfo] objectForKey:@"rect"] convertToRect];
                rect2 = [self flipUpsideDown:rect2];
                
                // After it's flipped, (0,0) is bottom left, so this is right. Want to check for y position of origin.
                // So this meansures from the bottom and top, since sometimes you have nested elements.
                
                if (rect1.origin.y  > rect2.origin.y + 5 &&
                    rect1.origin.y + rect1.size.height > rect2.origin.y + rect2.size.height + 5)
                    combinedString = [combinedString stringByAppendingString:[NSString stringWithFormat:@"\n\n%@",[[ta userInfo] objectForKey:@"text"]]];
                else
                    combinedString = [combinedString stringByAppendingString:[NSString stringWithFormat:@" %@",[[ta userInfo] objectForKey:@"text"]]];
            }
        }
    }
    
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    if (![combinedString isEqualTo:@""] && [array count] != 0) {
        if (_isMainWindow) {
            [d.videoController.popoverText setString:combinedString];
            // So have to convert the rectangle back to video view rectangle.
            NSRect taRect = [[[[array objectAtIndex:0] userInfo] objectForKey:@"rect"] convertToRect];
            taRect = [self flipUpsideDown:taRect];
            [d.videoController.popover showRelativeToRect:taRect ofView:[self contentView] preferredEdge:NSMaxYEdge];
            [d.videoController.popover becomeFirstResponder];
            [d.videoController.popover.contentViewController.view.window makeKeyAndOrderFront:self];
        } else {
            [d.miniWindowController.popoverText setString:combinedString];
            // So have to convert the rectangle back to video view rectangle.
            NSRect taRect = [[[[array objectAtIndex:0] userInfo] objectForKey:@"rect"] convertToRect];
            taRect = [self flipUpsideDown:taRect];
            [d.miniWindowController.popover showRelativeToRect:taRect ofView:[self contentView] preferredEdge:NSMaxYEdge];
            [d.miniWindowController.popover becomeFirstResponder];
            [d.miniWindowController.popover.contentViewController.view.window makeKeyAndOrderFront:self];

        }
    }
    
}

-(NSRect)getVideoRectFromAXElement:(TextNode *)element {
    CGRect rect = [element.rect convertToRect];
    NSRect nsrect = NSRectFromCGRect(rect);
    return [self convertToVideoWindowCoordFromScreenCoord:nsrect ratio:[self getScreenToWindowRatio]];
}

// TODO: This naming is shit and should probably be changed. 

-(NSPoint)convertToScreenCoordFromWinodowsCoord:(NSPoint)point ratio:(float)ratio {
    
    CGFloat clickedWidth = point.x;
    CGFloat clickedHeight = point.y;
    
    CGFloat windowClickedWidth = ratio * clickedWidth;
    CGFloat windowClickHeight = ratio * clickedHeight;
    
    CGPoint newPoint = {windowClickedWidth,windowClickHeight};
    return NSPointFromCGPoint(newPoint);
}

-(NSRect)convertToVideoWindowCoordFromScreenCoord:(NSRect)rect ratio:(float)ratio {
    // Think I can just divide everything by the ratio? Yeah, that seems fine.
    float newX = rect.origin.x / ratio;
    float newY = rect.origin.y / ratio;
    float newWidth = rect.size.width / ratio;
    float newHeight = rect.size.height / ratio;
    CGRect newRect = CGRectMake(newX, newY, newWidth, newHeight);
    return NSRectFromCGRect(newRect);
}

-(NSRect)flipUpsideDown:(NSRect)rect {
    float newY  = [[NSScreen mainScreen] frame].size.height - rect.origin.y - rect.size.height;
    return NSMakeRect(rect.origin.x, newY, rect.size.width, rect.size.height);
}

-(float)getScreenToWindowRatio {
    NSSize frameSize = [self frame].size;
    CGFloat frameWidth = frameSize.width;
    
    // So now get ratio of the screensize to framesize, and multiple clicked by that
    CGFloat windowWidth = [[NSScreen mainScreen] frame].size.width;
    return windowWidth / frameWidth;
}

-(void)setupSearchTextNodes:(Moment *)moment string:(NSString *)string {
    [self removeSearchTextNodeView];
    
    if (!string || [string isEqualToString:@""])
        return;
    @synchronized(moment.textState.textNodes) {
        for (TextNode *element in moment.textState.textNodes) {
            CGRect videoRect = [self getVideoRectFromAXElement:element];
            // Do an if check here that... makes a rectengle if the textNode contains the search text?
            if (string && [element.text contains:string]) {
                SearchTextNodeView *foundView = [[SearchTextNodeView alloc] initWithFrame:videoRect];
                [[self contentView] addSubview:foundView];
                [_searchTextNodesArray addObject:foundView];
            }
        }
    }
}

- (void)removeSearchTextNodeView {
    if (!_searchTextNodesArray) {
        _searchTextNodesArray = [NSMutableArray array];
    }
    if ([_searchTextNodesArray count] != 0) {
        for (SearchTextNodeView *view in _searchTextNodesArray) {
            [view removeFromSuperview];
        }
    }
}

- (CGPoint)centerOfRect:(NSRect)rect {
    return CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height / 2);
}


#pragma mark misc

// This is to make text popups clickeable
- (BOOL)canBecomeKeyWindow
{

    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    if (_isMainWindow) {
        VideoWindowController *controller = d.videoController;
        if (![controller.popover isShown])
            return NO;
    } else {
        // Think for miniWindowController just yes is fine. If you have to click twice to open then put this back.
//        MiniWindowController *controller = d.miniWindowController;
//        if (![controller.popover isShown])
//            return NO;
    }
    return YES;

}

#pragma mark Could Be useful later?
// Following would be ideal for mouse exit but crashes for some reason. Figure it out later.
// Think it's because of multithreading. Right, that's why it's messing up.

- (void)removeAllSubview {
    NSArray *viewsToRemove = [[self contentView] subviews];
    @synchronized(viewsToRemove) {
        for (NSView *v in viewsToRemove) {
            if ([v isKindOfClass:[HighlightView class]] || [v isKindOfClass:[NSTextView class]]) {
                [v removeFromSuperview];
            }
        }
    }
    _highlightView = nil;
}

- (void)setController:(id)controller {
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    if (controller == d.videoController)
        _isMainWindow = YES;
    else
        _isMainWindow = NO;
}




@end
