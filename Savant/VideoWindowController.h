//
//  VideoWindowController.h
//  Savant
//
//  Created by Mark Wang on 9/7/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "VideoWindow.h"
#import "Moment.h"
#import "GeneralPlaybackController.h"

@interface VideoWindowController : GeneralPlaybackController

@property (strong) IBOutlet NSWindow *playbackWindow;
@property (weak) IBOutlet NSSlider *momentSlider;
@property (weak) IBOutlet NSButton *backButton;
@property (weak) IBOutlet NSButton *forwardButton;
@property (strong) IBOutlet NSWindow *openWindow;
@property (weak) IBOutlet NSButton *openButton;

- (IBAction)sliderMoved:(NSSlider *)slider;
- (IBAction)skipBack:(NSButton *)sender;
- (IBAction)skipForward:(NSButton *)sender;
- (IBAction)openApp:(NSButton *)button;


@property (weak) IBOutlet NSPopover *popover;
@property (strong) IBOutlet NSTextView *popoverText;

- (void)tableViewMomentWasSelected:(Moment *)moment;

- (void)dismissPlayer;

+ (void)openApp:(Moment *)moment;

@property (nonatomic, strong) NSTimer *timer;

@end
