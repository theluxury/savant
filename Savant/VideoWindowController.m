//
//  VideoWindowController.m
//  Savant
//
//  Created by Mark Wang on 9/7/14.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import "VideoWindowController.h"
#import "LightboxOverlayWindow.h"
#import "SVBrowsingWindowController.h"
#import "AppDelegate.h"
#import "PredicateHelper.h"
#import "TextNode.h"
#import "TextState.h"
#import "NSString+StringUtil.h"
#import "GAHelper.h"
#import "SearchTextNodeView.h"
#import "SharedPredicateHelper.h"

@interface VideoWindowController ()

@property (readonly, nonatomic, strong) LightboxOverlayWindow *overlayWindow;
@property (readonly, nonatomic, strong) NSWindow *timeWindow;
@property (nonatomic, strong) NSTextView *timeView;

@end

@implementation VideoWindowController

static const Float32 PLAYBACK_SPEED = 0.4;
// TODO: amount to moments to grab before and after selected moment. eventually change this to be smarter.
static const NSInteger FORWARD_GRAB = 25;
static const NSInteger BACKWARD_GRAB = 25;

@synthesize overlayWindow = _overlayWindow;

- (instancetype)init {
    if(self = [super initWithWindowNibName:@"MainVideoWindow"]){
        self.momentsArray = [NSMutableArray array];
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];

    [(VideoWindow *)self.window setController:self];
    [self setupWindow];
    [self setupTimeDisplay];
    [self setupControlDisplay];
    [self setupOpenDisplay];
    _popover.animates = false;

    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)sliderMoved:(NSSlider *)sender {
    // Now should move the array
    self.currShowingMoment = self.momentsArray[[_momentSlider intValue]];
    [self refreshImage];
}

// Why is it not getting stuff right?

- (void)refreshImage {
    [_momentSlider setMaxValue:[self.momentsArray count] - 1];
    [_momentSlider setIntegerValue:[self.momentsArray indexOfObject:self.currShowingMoment]];
    [_popover close];
    [((VideoWindow *)self.window).tmphackimageview setImage:self.currShowingMoment.screenImage];
    [self.timeView setString:[self getDateString:self.currShowingMoment]];
    [(VideoWindow *)self.window removeAllSubview];
    [self setupTrackingAreas];
    // If this lags here move it into setupTrackingAreas
    AppDelegate *d = [AppDelegate sharedInstance];
    [(VideoWindow *)self.window setupSearchTextNodes:self.currShowingMoment string: [d.browsingController getSearchText]];
    
}

- (IBAction)skipBack:(NSButton *)sender {
    [self goBackOneFrame];
    return;
    
    // Following is if you want that button to go a lot back. It turned out to be real clunky.
    [self skipBack];
}

- (IBAction)skipForward:(NSButton *)sender {
    [self goForwardOneFrame];
    return;
    
    [self skipForward];
}

- (NSPredicate *)getButtonPredicate {
    SVBrowsingWindowController *bc = [(AppDelegate *)[NSApplication sharedApplication].delegate browsingController];
    return [SharedPredicateHelper setButtonPredicate:bc.isBlue isRed:bc.isRed isGreen:bc.isGreen isYellow:bc.isYellow];
}

- (NSPredicate *)getCombinedPredicate:(NSPredicate *)predicate {
    NSPredicate *buttonPredicate = [self getButtonPredicate];
    return [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate,  buttonPredicate, nil]];
}


- (IBAction)openApp:(NSButton *)button {
    [VideoWindowController openApp:self.currShowingMoment];
}

+ (void)openApp:(Moment *)moment {
    if (!moment)
        return;
    
    // so see what kind of moment it is.
    if (moment.url) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:moment.url]];
        [[GAHelper sharedInstance] openUrl];
    }
    else {
        [[NSWorkspace sharedWorkspace] launchApplication:moment.activeApplication];
        [[GAHelper sharedInstance] openApplication];
    }
    
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    [d.videoController dismissPlayer];
    
}

- (void)showPlayer
{
    AppDelegate *d = (AppDelegate *)[NSApplication sharedApplication].delegate;
    
    if(![self.window isVisible]) {
        
        NSRect appFrame = d.browsingController.window.frame;
        NSRect screenFrame = self.window.screen.frame;
        
        CGFloat aspectRatio = screenFrame.size.height / screenFrame.size.width;
        
        CGFloat vWidth = screenFrame.size.width - appFrame.size.width - 40;
        CGFloat vHeight = vWidth * aspectRatio;
        
        [self setupOverlayWindow];
        
        [self.window setFrame:NSMakeRect(appFrame.size.width+20, (screenFrame.size.height - vHeight)/1.6, vWidth, vHeight) display:YES];
        [self.window makeKeyAndOrderFront:self];
        
        [_playbackWindow setFrame:NSMakeRect(appFrame.size.width+20 + (vWidth - 218) / 2, 20, 218, 48) display:YES];
        [_playbackWindow makeKeyAndOrderFront:self];
        
        float timeDisplayX  = appFrame.size.width+20 + vWidth - 180;
        float timeDisplayY = (screenFrame.size.height - vHeight)/1.6 + vHeight + 15;
        [_timeWindow setFrame:NSMakeRect(timeDisplayX, timeDisplayY, 180, 20) display:YES];
        [_timeWindow makeKeyAndOrderFront:self];
        
        float buttonWidth = _openButton.frame.size.width;
        float buttonHeight = _openButton.frame.size.height;
        
        [_openWindow setFrame:NSMakeRect(appFrame.size.width+20 + (vWidth - buttonWidth) / 2, timeDisplayY, buttonWidth, buttonHeight) display:YES];
        [_openWindow makeKeyAndOrderFront:self];
        
        
    }
}
- (void)dismissPlayer
{
    [self.overlayWindow orderOut:self];
    [self.window orderOut:self];
    [_playbackWindow orderOut:self];
    [_timeWindow orderOut:self];
    [_openWindow orderOut:self];
}


// TODO: should be on the class
- (NSWindow *)overlayWindow
{
    if(!_overlayWindow){
        _overlayWindow = [[LightboxOverlayWindow alloc] initWithContentRect:NSZeroRect]; //##?
        
    }
    return _overlayWindow;
}

- (void)tableViewMomentWasSelected:(Moment *)moment {
    self.currShowingMoment = moment;
    [self.momentsArray removeAllObjects];
    [self.momentsArray addObjectsFromArray:[self fetchArray]];
    [self showPlayer];
    [self refreshImage];
}

- (NSString *)getDateString:(Moment *)moment {
    NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
    [dtFormatter setDateFormat:@"hh:mm:ss a MM/dd/yyyy"];
    return [dtFormatter stringFromDate:moment.timestamp];
}


#pragma mark Time and Button Display

- (void)setupTimeDisplay {
    
    _timeWindow = [[NSWindow alloc] init];
    [self setupWindowDisplay:_timeWindow backgroundColor:[NSColor clearColor]];
    _timeView = [[NSTextView alloc] initWithFrame:NSMakeRect(0, 0, 180, 20)];
    [_timeView setBackgroundColor:[NSColor clearColor]];
    [_timeView setTextColor:[NSColor whiteColor]];
    [_timeView setFont:[NSFont fontWithName:@"Helvetica" size:14]];
    [[_timeWindow contentView] addSubview:_timeView];
}

- (void)setupControlDisplay {
    [self setupWindowDisplay:_playbackWindow backgroundColor:[NSColor savantBackgroundGradientStart]];
}

- (void)setupOpenDisplay {
    [self setupWindowDisplay:_openWindow backgroundColor:[NSColor savantBackgroundGradientStart]];
}

- (void)setupWindowDisplay:(NSWindow *)window backgroundColor:(NSColor *)color {
    [window setStyleMask:NSBorderlessWindowMask];
    [window setOpaque:NO];
    [window setAlphaValue:0.99];
    [window setBackgroundColor:color];
    [window setLevel:CGShieldingWindowLevel()];
}

-(void)setupOverlayWindow {
    [self.overlayWindow setFrame:self.window.screen.frame display:YES];
    [self.overlayWindow makeKeyWindow];
    [self.overlayWindow orderBack:self];
}

-(void)setupWindow {
    [self.window setStyleMask:NSBorderlessWindowMask];
    [self.window setBackgroundColor:[NSColor darkGrayColor]];
    [self.window setLevel:CGShieldingWindowLevel()];
}


#pragma mark Horizontal Scroll Setup


- (NSArray *)fetchArray {
    SVBrowsingWindowController *bc = [(AppDelegate *)[NSApplication sharedApplication].delegate browsingController];
    self.currShowingStore = bc.currShowingStore;
    // So at playback, should be able to be smart enough to grab the next 30 seconds, and then... remake that into the array?
    // So first thing first, let's make a fetch predicate that... let's say filters by app? Yeah, I think for now filters by app is fine. Also needs to know what moment is being selected...
    NSPredicate *afterPredicate = [PredicateHelper getPlaybackPredicate:bc.isBlue isRed:bc.isRed isGreen:bc.isGreen isYellow:bc.isYellow moment:self.currShowingMoment after:YES];
    NSPredicate *beforePredicate =[PredicateHelper getPlaybackPredicate:bc.isBlue isRed:bc.isRed isGreen:bc.isGreen isYellow:bc.isYellow moment:self.currShowingMoment after:NO];
    
    // So this is the array you want to playback with...
    NSArray *afterArray = [PredicateHelper fetchScreenshots:afterPredicate storeArray:@[self.currShowingStore] withLimit:FORWARD_GRAB ascending:YES];
    NSArray *beforeArray = [PredicateHelper fetchScreenshots:beforePredicate storeArray:@[self.currShowingStore] withLimit:BACKWARD_GRAB ascending:NO];
    // so then combine the arrays into one
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:afterArray];
    [tempArray addObjectsFromArray:beforeArray];
    // Then sort the arrays
    NSSortDescriptor *sortDescriptor;
    // So, um, this might be descending. can look at after.
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    return [tempArray sortedArrayUsingDescriptors:sortDescriptors];
    
}

@end
