//
//  main.m
//  Savant
//
//  Created by Paul Musgrave on 2014-08-02.
//  Copyright (c) 2014 Paul Musgrave. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
